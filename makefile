OBJS = LoRaSp.o htree.o edge.o redNode.o blackNode.o superNode.o node.o dataLog.o dataProcessor.o dataLoader.o hparams.o params.o
EIGEN_DIR = "EIGEN"
SCOTCH_DIR = "SCOTCH"
CINCLUDES = -I $(EIGEN_DIR) -I $(SCOTCH_DIR)/include
LFLAGS = -L $(SCOTCH_DIR)/lib
CFLAGS = -Wall -O3 -std=c++0x
CPP = g++
LIBS = -lscotch -lscotcherr -lm

default: LoRaSp

LoRaSp: $(OBJS)
	$(CPP) $(CFLAGS) $(CINCLUDES) -o LoRaSp $(OBJS) $(LFLAGS) $(LIBS)

LoRaSp.o: LoRaSp.cpp ILU.h dataLog.h dataLog.cpp dataProcessor.h dataProcessor.cpp diagPC.h directSolver.h eyePC.h fixedPoint.h gmres.h hparams.h hparams.cpp htree.h htree.cpp outerSolver.h params.h params.cpp preconditioner.h
	$(CPP) $(CFLAGS) $(CINCLUDES) -c LoRaSp.cpp

htree.o: preconditioner.h blackNode.h blackNode.cpp edge.h edge.cpp hparams.h hparams.cpp redNode.h redNode.cpp superNode.h superNode.cpp htree.h htree.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c htree.cpp

edge.o: edge.cpp edge.h redNode.h redNode.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c edge.cpp

superNode.o: blackNode.h blackNode.cpp node.h node.cpp hparams.h hparams.cpp htree.h htree.cpp redNode.h redNode.cpp superNode.h superNode.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c superNode.cpp

blackNode.o: blackNode.h blackNode.cpp edge.h edge.cpp hparams.h hparams.cpp htree.h htree.cpp redNode.h redNode.cpp superNode.h superNode.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c blackNode.cpp

redNode.o: node.h node.cpp redNode.h redNode.cpp blackNode.h blackNode.cpp edge.h edge.cpp hparams.h hparams.cpp htree.h htree.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c redNode.cpp

node.o: node.h node.cpp edge.h edge.cpp hparams.h hparams.cpp htree.h htree.cpp redNode.h redNode.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c node.cpp

dataLog.o: dataLoader.h dataLoader.cpp dataLog.h dataLog.cpp dataProcessor.h dataProcessor.cpp outerSolver.h preconditioner.h
	$(CPP) $(CFLAGS) $(CINCLUDES) -c dataLog.cpp

dataProcessor.o: dataProcessor.h dataProcessor.cpp dataLoader.h dataLoader.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c dataProcessor.cpp

dataLoader.o: dataLoader.h dataLoader.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c dataLoader.cpp

hparams.o: hparams.h hparams.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c hparams.cpp

params.o: params.h params.cpp dataLoader.h dataLoader.cpp hparams.h hparams.cpp
	$(CPP) $(CFLAGS) $(CINCLUDES) -c params.cpp

clean:
	rm -rf *.o LoRaSp
