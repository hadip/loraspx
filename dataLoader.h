#ifndef data_loader_h
#define data_loader_h

#include <string>
#include "Eigen/Dense"
#include "Eigen/Sparse"

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! A dense vector class used for RHS and Solutions */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

//! TRIPLET Type
/*! Define a triplet type to store entries of a matrix */
typedef Eigen::Triplet<double> TRIPLET;

//! Class Data Loader
/*! This class provides inpurt matrix, right-hand-side vector(s), and
solution(s) vector in Eigen formats. For the main matrix we use sparse
storage, while right-hand-side and solution vectors are stored in dense
format. */
class DataLoader {
 public:
  //! Default constructor
  DataLoader() {}

  //! Constructor
  /*!
  With this constructor DataLoader load data from memory:
  main matrix(sparse matrix), rhs (dense vec), solution (dense vec)
  */
  DataLoader(spMat*, VectorXd*, VectorXd*);

  //! Constructor
  /*!
  With this constructor DataLoader load data from memory
  main matrix, rhs, and sol all given as sparse matrices
  */
  DataLoader(spMat*, densMat*, densMat*);

  //! Constructor
  /*!
  With this constructor DataLoader load data from given file paths or
  directions.

  The input files should have the following format:
  First line: n, n, nnz
  n is the matrix size and nnz is the number of non-zero entries.
  The first line is followed by nnz lines each with following format:
  i, j, v
  which means the (i,j)'th entry of the matrix has value v
  */
  DataLoader(std::string, std::string, std::string);

  //! Destructor
  ~DataLoader();

  //! Access function to the matrix
  spMat* matrix() { return matrix_; }

  //! Access function to the the i'th right hand side vector
  //! The defult value is 0 (the first right-hand-side)
  VectorXd* rhs(int = 0);

  //! Access function to the the i'th solution vector
  //! The defult value is 0 (the first solution)
  VectorXd* sol(int = 0);

  //! Return input matrix size (non-square matrices are not accepted)
  int size() { return size_; }

  //! Return number of input rhs/sol vectors
  int numRHS() { return numRHS_; }

  //! Returns true if exact solution is available
  bool solutionAvailable() { return solutionAvailable_; }

  //! Log data loading info given reference to the log file
  void log(std::ofstream&);

 private:
  //! Pointer to the main matrix
  spMat* matrix_ = NULL;

  //! Pointer to the rhs matrix
  densMat* rhsMatrix_ = NULL;

  //! Pointer to the sol matrix
  densMat* solMatrix_ = NULL;

  //! Pointer to rhs vector (for single rhs)
  VectorXd* rhs_ = NULL;

  //! Pointer to solution vector (for single sol)
  VectorXd* sol_ = NULL;

  //! Loaded matrix size;
  int size_;

  //! Number of provided rhs (solution) vectors
  int numRHS_;

  //! Keep track of extra memory used
  bool extraVectorMemory_;
  bool extraMatrixMemory_;

  //! Helper function to read RHS and Solution matrices from file
  /*! Input argument is: file path
  returns dense matrix pointer
  */
  densMat* readDenseMatrix(char*);

  //! Helper function to read the main matrix from file
  /*! Input argument is: file path
  returns sparse matrix pointer
  */
  spMat* readSparseMatrix(char*);

  //! Time to load the data
  double time_ = 0;

  //! True if solution vector is available
  bool solutionAvailable_;
};

#endif
