#ifndef diagPC_h
#define diagPC_h

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "preconditioner.h"
#include "time.h"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/***************************************************************/
/*                     Diagonal Preconditioner                 */
/***************************************************************/
//! Class ILU
/*!
This is the implementation of a diagonal preconditioner inherited from
preconditioner class.
 */
class DiagPC : public Preconditioner {
  //! Size of the matrix
  int n_;

  //! Inverse of diagonal entries
  VectorXd* invDiag_;

  //! Pointer to the solution vector
  VectorXd* x_;

  //! Time for preconditioner factorization time
  double setupAndFactorizationTime_;

 public:
  //! Default constructor
  DiagPC(){};

  //! constructor
  /*!
    Takes pointer to the sparse matrix
   */
  DiagPC(spMat* A) {
    clock_t start = clock();
    n_ = A->rows();
    invDiag_ = new VectorXd(n_);
    x_ = new VectorXd(n_);
    (*invDiag_) = A->diagonal();
    for (int i = 0; i < n_; i++) {
      (*invDiag_)(i) = 1. / (*invDiag_)(i);
    }
    setupAndFactorizationTime_ = double(clock() - start) / CLOCKS_PER_SEC;
  }

  //! Destructor
  ~DiagPC() {
    delete invDiag_;
    delete x_;
  }

  //! Solve function
  VectorXd& solve(VectorXd& b) {
    for (int i = 0; i < n_; i++) {
      (*x_)(i) = (*invDiag_)(i)*b(i);
    }
    return *x_;
  }

  //! Log info
  void log(std::ofstream& out) {
    out << "Diagonal preconditioner setup and factorization time = "
        << setupAndFactorizationTime_ << " seconds\n";
  }
};

#endif
