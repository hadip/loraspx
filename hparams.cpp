#include "hparams.h"

Hparams::Hparams() {
  treeDepth_ = 0;
  wellSepDistance_ = 1;
  solveLevel_ = 1;
  symmetric_ = 2;
  combinedElimination_ = false;
  unCompressedSolve_ = false;
  secondOrder_ = 0;
  eliminationOrder_ = 0;
  lowRankMethod_ = 0;
  rSVDFactor_ = 1.5;
  cutOffMethod_ = 1;
  epsilon_ = 0.1;
  aPrioriRank_ = 0;
  rankCapFactor_ = 0;
  smoothing_ = 0;
  basisEnhancement_ = 0;
  verbose_ = false;
}

Hparams::Hparams(unsigned int depth, int wellSepDist, unsigned int sLevel,
                 unsigned int sym, int secondOrder, bool combElim,
                 bool unCompSolve, unsigned int elimErder,
                 unsigned int lowRankMeth, double rSVDFact,
                 unsigned int cutOffMeth, double eps, unsigned int rank,
                 double cap, int smooth, int enhance) {
  treeDepth_ = depth;
  wellSepDistance_ = wellSepDist;
  solveLevel_ = sLevel;
  symmetric_ = sym;
  secondOrder_ = secondOrder;
  combinedElimination_ = combElim;
  unCompressedSolve_ = unCompSolve;
  eliminationOrder_ = elimErder;
  lowRankMethod_ = lowRankMeth;
  rSVDFactor_ = rSVDFact;
  cutOffMethod_ = cutOffMeth;
  epsilon_ = eps;
  aPrioriRank_ = rank;
  rankCapFactor_ = cap;
  smoothing_ = smooth;
  basisEnhancement_ = enhance;
  verbose_ = true;
}