#ifndef data_processor_h
#define data_processor_h

#include <fstream>
#include <string>
#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "dataLoader.h"

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! A dense vector class used for RHS and Solutions */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! A dense vector of booleans (for pre-elimination) */
typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> VectorXb;

/*! A dense vector class used for permutation */
typedef Eigen::Matrix<int, Eigen::Dynamic, 1> VectorXi;

/*! The permutation matrix type */
typedef Eigen::PermutationMatrix<Eigen::Dynamic, Eigen::Dynamic, int> permMat;

//! Class Data Processor
/*! This class let us to apply some pre-process operations on the input linear
 * system. Basically, this class gets a linear system A * x = b (given in the
 * form of a dataLoader object), and convert it to A' * x' = b' linear system,
 * which is supposed to be an easier linear system compraed to the original one
 * to solve. Pre-process operations consist of scaling, pre-elimination, etc.
 * The matrix, righ-hand-side(s), and exact solution(s) vectors can be accessed
 * from this class in exact similar ways as from the DataLoader class. After the
 * trasformed linear system is solved (using the main precondtioner/iteration
 * scheme) the original solution (x) can be retrieved (applying the inverse of
 * the pre-proessing operations) using this class. */
class DataProcessor {
 public:
  //! Default constructor
  DataProcessor() {}

  //! Constructor
  /*!
  Data Processor can be initiated using an instance of the data laoder class
  (which is assumed to be already initiated), and parameters for scaling and
  pre-elimination. The default parameters are no scaling and no pre-elimination.

  For the scaling parameter:
  "0": No re-scaling
  "1": Make all rows norm one
  "2": Make all columns norm one
  "3": Make all diagonal entries 1 (row manipulation)
  "4": Make all diagonal entries 1 (column manipulation)

  For the pre-elimination parameter:
  "0": No pre-elimination
  "1": 1-selection (if the graph is 2-colorable, this will be automatically a
  2-coloring)
  "[d]": For any integer [d] > 1 it performs a d-selection.
  */
  DataProcessor(DataLoader*, unsigned = 0, unsigned = 0);

  //! Destructor
  ~DataProcessor();

  //! Access function to the matrix
  spMat* matrix() { return matrixPreEliminated_; }

  //! Access function to the the i'th trasnformed right hand side vector
  //! The defult value is 0 (the first right-hand-side)
  VectorXd* rhs(int = 0);

  //! Access function to the the i'th trasnformed solution vector
  //! The defult value is 0 (the first solution)
  VectorXd* sol(int = 0);

  //! Return input matrix size (non-square matrices are not accepted)
  int size() { return size_; }

  //! Return number of input rhs/sol vectors
  int numRHS() { return numRHS_; }

  //! Retrieve the original solution, x, given a solution to the transformed
  //! linear system
  VectorXd* retrieve(VectorXd*);

  //! Compute relative error and relative residual of the original (before
  //! transformation) and add to the sum variables
  /*!
  The input arguments are:
  1) Refrence to solution vector (of the transformed linear system)
  2) RHS index
  */
  void analyze(VectorXd&, int);

  //! Returns true if exact solution is available
  bool solutionAvailable() { return dl_->solutionAvailable(); }

  //! Log pre-processing and solution errors given reference to the log file
  void log(std::ofstream&);

 private:
  //! Time to complete data processing
  double time_ = 0;

  //! Pointer to the data loader class
  DataLoader* dl_ = NULL;

  //! Pointer to the main matrix
  spMat* matrix_ = NULL;

  //! Pointer to the scaled matrix
  spMat* matrixScaled_ = NULL;

  //! Pointer to the pre-eliminated matrix
  spMat* matrixPreEliminated_ = NULL;

  //! Diagonal entries of the left scaling matrix, D1
  VectorXd* d1_ = NULL;

  //! Diagonal entries of the right scaling matrix, D2
  VectorXd* d2_ = NULL;

  //! Inverse of the diagonal entries of the right scaling matrix, D2
  VectorXd* invD2_ = NULL;

  //! Scaled rhs
  VectorXd* rhsScaled_ = NULL;

  //! Pre-eliminated rhs
  VectorXd* rhsPreEliminated_ = NULL;

  //! Scaled exact solution
  VectorXd* solScaled_ = NULL;

  //! Pre-eliminated exact solution
  VectorXd* solPreEliminated_ = NULL;

  //! Result of undoScaling
  VectorXd* xUnScaled_ = NULL;

  //! Result of undoPreElimination
  VectorXd* xUnPreEliminate_ = NULL;

  //! Indices of selected rows/cols to be pre-eliminated
  VectorXb* selected_ = NULL;

  //! Permutation vector (used for pre-elimination)
  VectorXi* permutationVector_ = NULL;

  //! Permutation matrix (used for pre-elimination)
  permMat* permutationMatrix_ = NULL;

  //! Pre-elimination extra variables
  Eigen::SparseLU<spMat> solver_;
  spMat* B21_ = NULL;
  spMat* B12_ = NULL;
  VectorXd* invB11_ = NULL;

  //! pre-processed matrix size;
  int size_;

  //! Number of provided rhs (solution) vectors
  int numRHS_;

  //! Scaling parameter
  unsigned int scaling_param_;

  //! Pre-elimination parameter
  int elim_param_;

  //! Total number of solution analysis (to compute the averages)
  int num_analysis_ = 0;

  //! Sum of all relative residuals
  double sum_residuals_ = 0.0;

  //! Sum of all relative errors
  double sum_errors_ = 0.0;

  //! Apply scaling.
  /*! In the general case this transformation can be written as follows:
  A x = b ---> D1 A D2 inv(D2) x = D1 b,
  where D1 and D2 are diagonal matrices. Therefore, the the trasnformed matrix
  is D1 A D2, the trasnformed solution is inv(D2) x, and the transformed
  right-hand side is D1 b.
  */
  void applyScaling();

  //! Undo scaling on the solution vector
  /*! This function gets undo the effect of scaling on the solution.
  It gets a solution to the scaled system D1 A D2 inv(D2) x = D1 b, and return
  the corresponding solution to the unscaled system. Essentially, it gets z and
  returns D2 * z.
  */
  VectorXd* undoScaling(VectorXd*);

  //! Apply pre-elimination
  /*! In the general case this transformation can be writen as follows:
  1) select DOFs to be pre-eliminated:
  A x = b ---> P A P^T P x = P b,
  where P is a permutation matrix.
  2) Eliminate the selected DOFs:
                B_11  B_12            y_1            c_1
  P A P^T = B = B_21  B_22, P x = y = y_2, P b = c = c_2
  where B_11 is a block diagonal matrix (easy to invert)
  The trasnformed linear system is obtained through Schur-Complement:
  ( B_22 - B_21 inv(B_11) B_12 ) y_2 = c_2 - B_21 inv(B_11) c_1
  */
  void applyPreElimination();

  //! Undo pre-elimination on the solution vector
  /*!
  This function gets a solution to the pre-eliminated system and returns the
  corresponding solution to the original system. Note that the input and out
  vector size can is different when pre-elimination is applied.
  */
  VectorXd* undoPreElimination(VectorXd*);

  //! Select DOF's to be preliminated
  /*! In this implementation we select rows/columns randomly
  An efficient implementation is to select rows/cols such that the largest
  connected component in the induced graph is minimized.
  */
  void select();

  //! Select DOFs for pre-eliminatoin
  /*!
  This function using a greedy algorithm select DOFs such that all connected
  components in the induced graph have only one vertex.
  */
  void oneSelection();
};

#endif
