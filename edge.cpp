#include "edge.h"
#include <iostream>
#include "redNode.h"

// Function to determine if the edge is between two well-separated nodes
bool Edge::isWellSeparated() {
  return !(
      source_->redParent()->generalAdjSet()->count(destination_->redParent()));
}

// Constructor
Edge::Edge(Node* s, Node* d, densMat* m) {
  // load source/des. pointers
  source_ = s;
  destination_ = d;
  matrix_ = m;

  // Initially the edge is not compressed
  compressed_ = false;
}

// Destructor
Edge::~Edge() {
  if (matrix_ != NULL) delete matrix_;
}

// Returns [true] if any of the source or dest. nodes are eliminated
bool Edge::isEliminated() {
  if ((source_->isEliminated()) || (destination_->isEliminated())) {
    return true;
  } else {
    return false;
  }
}

// Compress the edge. i.e.:
// Set the compressed flag to [true]
// Deallocate the matrix memory
void Edge::compress() {
  // Set the flag to true
  compressed_ = true;

  // Free the matrix memory
  if (matrix_ != NULL) delete matrix_;
  matrix_ = NULL;
}

void Edge::addMatrix(Node* view, densMat& m) {
  if (view == source_) {
    *matrix_ += m;
  } else if (view == destination_) {
    *matrix_ += m.transpose();
  } else {
    std::cout << "Invalid access to matrix of an edge!\n";
    exit(1);
  }
}

void Edge::setMatrix(Node* view, densMat& m) {
  if (view == source_) {
    *matrix_ = m;
  } else if (view == destination_) {
    *matrix_ = m.transpose();
  } else {
    std::cout << "Invalid access to matrix of an edge!\n";
    exit(1);
  }
}

void Edge::createEdge(Node* source, Node* destination, densMat& mat) {
  // check if edge already exist
  if (source->outEdges.find(destination) == source->outEdges.end()) {
    Edge* newEdge = new Edge(source, destination, new densMat(mat));
    source->outEdges.insert(EdgePair(destination, newEdge));
    destination->inEdges.insert(EdgePair(source, newEdge));
  }
}

void Edge::createEdgeSymmetric(Node* source, Node* destination, densMat& mat) {
  // check if edge already exist
  if (source->outEdges.find(destination) == source->outEdges.end()) {
    Edge* newEdge = new Edge(source, destination, new densMat(mat));
    source->outEdges.insert(EdgePair(destination, newEdge));
    destination->outEdges.insert(EdgePair(source, newEdge));
  }
}

void Edge::createUpdateEdge(Node* source, Node* destination, densMat& mat) {
  // check if edge already exist
  if (source->outEdges.find(destination) != source->outEdges.end()) {
    source->outEdges[destination]->addMatrix(mat);
  } else {
    createEdge(source, destination, mat);
  }
}

void Edge::createUpdateEdgeSymmetric(Node* source, Node* destination,
                                     densMat& mat) {
  // check if edge already exist
  if (source->outEdges.find(destination) != source->outEdges.end()) {
    source->outEdges[destination]->addMatrix(source, mat);
  } else {
    createEdgeSymmetric(source, destination, mat);
  }
}

bool Edge::isTransposed(Node* view) {
  if (view == source_) {
    return false;
  } else if (view == destination_) {
    return true;
  } else {
    std::cout << "WRONG EDGE VIEW!\n";
    exit(1);
  }
}