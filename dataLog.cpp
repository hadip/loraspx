#include "dataLog.h"
#include <fstream>
#include "dataLoader.h"
#include "dataProcessor.h"
#include "outerSolver.h"
#include "preconditioner.h"
#include "time.h"

DataLog::DataLog() {
  out_.open("log.out", std::fstream::out | std::fstream::app);
}

DataLog::~DataLog() {
  out_ << "::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
       << std::endl;
  out_ << std::endl;
  out_.close();
}

void DataLog::logDataLoader(DataLoader* dl) {
  out_ << "::::::: Data-loader Info :::::::\n";
  dl->log(out_);
  out_ << std::endl;
}

void DataLog::logDataProcessor(DataProcessor* dp) {
  out_ << "::::::: Pre/Post processing Info :::::::\n";
  dp->log(out_);
  out_ << std::endl;
}

void DataLog::logPreconditioner(Preconditioner* pc) {
  out_ << "::::::: Pre-conditioner Info :::::::\n";
  pc->log(out_);
  out_ << std::endl;
}

void DataLog::logOuterSolver(OuterSolver* outerSolver) {
  out_ << "::::::: Outer Solver Info :::::::\n";
  outerSolver->log(out_);
  out_ << std::endl;
}