#ifndef direct_solver_h
#define direct_solver_h

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "outerSolver.h"
#include "preconditioner.h"
#include "time.h"

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

//! Class DirectSolver
/*!
This class implements a direct solve. I.e., the outer solver only performs one
iteration.
 */
class DirectSolver : public OuterSolver {
  //! Pointer to the preconditioner
  /*!
    Preconditioner supposed to have a function:
    x* solve( *rhs ): This function solve A x = rhs (approximately)
    and returns pointer to solution.
   */
  Preconditioner* precond_;

  //! Size of the matrix
  int n_;

  //! Total iterations applied
  int totalIters_;

  //! Total time
  double totalTime_;

  //! Pointer to the solution vector
  VectorXd* x_;

  //! Pointer to the matrix
  spMat* A_;

 public:
  //! Default constructor
  DirectSolver(){};

  //! Destructor
  ~DirectSolver();

  //! Constructor with input parameters
  /*!
    Pointer to the sparse matrix.
    Pointer to the preconditioner.
   */
  DirectSolver(spMat*, Preconditioner*);

  //! solve: start iteration until:
  /*!
    1) Reach maximum number of iteration
    or
    2) Reach desired accuracy
    Input parameters:
    1) Reference to right hand side vector
    2) Reference to the initial guess vector (not used here)
   */
  VectorXd& solve(VectorXd&, VectorXd&);

  //! Overloaded solve function with only RHS vector required.
  VectorXd& solve(VectorXd&);

  //! Returns solution
  VectorXd* retVal();

  //! Returns total number of itertions
  int totalIters() const { return totalIters_; }

  //! Returns total time to solve
  double totalTime() const { return totalTime_; }

  //! Log direct solver results in the given file
  void log(std::ofstream&);
};

DirectSolver::DirectSolver(spMat* A, Preconditioner* P) {
  A_ = A;
  precond_ = P;
  n_ = A->cols();  // = A->rows()
                   //
  //! Allocate memory to matrices and vectors
  x_ = new VectorXd(n_);
}

DirectSolver::~DirectSolver() { delete x_; }

VectorXd& DirectSolver::solve(VectorXd& b_, VectorXd& x0_) { return solve(b_); }

VectorXd& DirectSolver::solve(VectorXd& b_) {
  clock_t start;
  start = clock();
  (*x_) = precond_->solve(b_);
  totalTime_ = double(clock() - start) / CLOCKS_PER_SEC;
  totalIters_ = 1;
  return *x_;
}

VectorXd* DirectSolver::retVal() { return x_; }

void DirectSolver::log(std::ofstream& out) {
  out << "DirectSolver total time = " << totalTime_ << " seconds\n";
}

#endif
