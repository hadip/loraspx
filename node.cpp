#include "node.h"
#include <iostream>
#include "edge.h"
#include "hparams.h"
#include "htree.h"
#include "redNode.h"

// constructor for node
Node::Node(Node* P, HTree* T, int first, int last) {
  index_first_ = first;
  index_last_ = last;
  tree_ptr_ = T;
  parent_ = P;
  eliminated_ = false;
  VAR_ = NULL;
  RHS_ = NULL;
  hparams_ = T->parameters();
}

Node::~Node() {
  if (VAR_ != NULL) {
    delete VAR_;
  }

  if (RHS_ != NULL) {
    delete RHS_;
  }
}

// Erase compressed edges from the list of incoming/outgoing edges
void Node::eraseCompressedEdges() {
  if (!hparams_->symmetric()) {
    // Erase compressed edges from incoming edges list
    for (auto it = inEdges.begin(); it != inEdges.end();) {
      if (it->second->isCompressed()) {
        it = inEdges.erase(it);
      } else {
        ++it;
      }
    }
  }

  // Erase compressed edges from outgoing edges list
  for (auto it = outEdges.begin(); it != outEdges.end();) {
    if (it->second->isCompressed()) {
      it = outEdges.erase(it);
    } else {
      ++it;
    }
  }
}

// Set the elimination flag to [true]
void Node::eliminate() {
  eliminated_ = true;
  order = tree_ptr_->count;
  tree_ptr_->count++;
}

// Set the elimination flag to [false]
void Node::deEliminate() { eliminated_ = false; }

// Solve U VAR = RHS
void Node::solveU() {
  // Check if this node has any unknowns
  if (n() == 0) {
    return;
  }

  // A vecotr to store the effect of all other incoming edges
  VectorXd potential = VectorXd::Zero(n());

  if (!hparams_->symmetric()) {
    // Go through all incoming edges
    for (auto it = inEdges.begin(); it != inEdges.end(); ++it) {
      // Check if source of the edge is already solved
      if (it->first->order > order) {
        potential += it->second->matrix() * (*(it->first->VAR()));
      }
    }
  } else {
    for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
      // Check if source of the edge is already solved
      if (it->first->order > order) {
        if (it->second->isTransposed(it->first)) {
          potential += it->second->matrix().transpose() * (*(it->first->VAR()));
        } else {
          potential += it->second->matrix() * (*(it->first->VAR()));
        }
      }
    }
  }

  // solve unknowns
  *VAR() = (*invPivot) * (*RHS() - potential);

  // Add effect from far-field
  if (hparams_->combinedElimination() && redParent()->n() > 0) {
    *VAR() += (*outInvPivot_) * (*(redParent()->VAR()));
  }
}

// Solve L * z = b (the result is stored in RHS)
void Node::solveL() {
  // Check if this node has any unknowns
  if (n() == 0) {
    return;
  }

  // For RHS update: invPivot * RHS
  VectorXd factor_RHS = (*invPivot) * (*(RHS()));
  // Loop over all outgoing edges ( exclude the selfEdge ):Y
  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (it->first->order > order) {
      // Update RHS_Y_Dest.
      if (it->second->isTransposed(this)) {
        *(it->first->RHS()) -= it->second->matrix().transpose() * factor_RHS;
      } else {
        *(it->first->RHS()) -= it->second->matrix() * factor_RHS;
      }
    }
  }

  // Add effect to far-field
  if (hparams_->combinedElimination() && redParent()->n() > 0) {
    if (!hparams_->symmetric()) {
      *(redParent()->RHS()) = (*inInvPivot_) * (*RHS());
    } else {
      *(redParent()->RHS()) = outInvPivot_->transpose() * (*RHS());
    }
  }
}

void Node::gaussElimination() {
  if (!hparams_->symmetric()) {
    // Loop over all incoming edges ( exclude the selfEdge ): X
    for (auto it_i = inEdges.begin(); it_i != inEdges.end(); ++it_i) {
      if ((it_i->first != this) && !(it_i->second->isEliminated())) {
        // At this point we can compute invPivot * X
        densMat factor = (*invPivot) * it_i->second->matrix();
        // Loop over all outgoing edges ( exclude the selfEdge ):Y
        for (auto it_j = outEdges.begin(); it_j != outEdges.end(); ++it_j) {
          if ((it_j->first != this) && !(it_j->second->isEliminated())) {
            // Here we have ei -> selfEdge -> ej
            // After elimination we have: i->j  with matrix = -X * invPivot * Y
            densMat sc = -it_j->second->matrix() * factor;
            it_i->first->createUpdateEdge(it_j->first, sc);
          }
        }
      }
    }
  } else {
    // Loop over all edges ( exclude the selfEdge ): X
    for (auto it_i = outEdges.begin(); it_i != outEdges.end(); ++it_i) {
      if ((it_i->first != this) && !(it_i->second->isEliminated())) {
        // At this point we can compute invPivot * X
        densMat factor;
        if (it_i->second->isTransposed(it_i->first)) {
          factor = (*invPivot) * it_i->second->matrix().transpose();
        } else {
          factor = (*invPivot) * it_i->second->matrix();
        }
        // Loop over all edges ( exclude the selfEdge ):Y
        for (auto it_j = it_i; it_j != outEdges.end(); ++it_j) {
          if ((it_j->first != this) && !(it_j->second->isEliminated())) {
            // Here we have ei -> selfEdge -> ej
            // After elimination we have: i->j  with matrix = -X * invPivot * Y
            densMat sc;
            if (it_j->second->isTransposed(this)) {
              sc = -it_j->second->matrix().transpose() * factor;
            } else {
              sc = -it_j->second->matrix() * factor;
            }
            it_i->first->createUpdateEdge(it_j->first, sc);
          }
        }
      }
    }
  }
}

void Node::createUpdateEdge(Node* destination, densMat& mat) {
  if (!hparams_->symmetric()) {
    Edge::createUpdateEdge(this, destination, mat);
  } else {
    Edge::createUpdateEdgeSymmetric(this, destination, mat);
  }
}

void Node::createEdge(Node* destination, densMat& mat) {
  if (!hparams_->symmetric()) {
    Edge::createEdge(this, destination, mat);
  } else {
    Edge::createEdgeSymmetric(this, destination, mat);
  }
}
