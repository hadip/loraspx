#include "htree.h"
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include "Eigen/Sparse"
#include "blackNode.h"
#include "edge.h"
#include "hparams.h"
#include "redNode.h"
#include "superNode.h"
#include "time.h"

// Constructor
HTree::HTree(spMat* M, Hparams* par) {
  clock_t start = clock();
  param_ = (par == NULL) ? new Hparams() : par;
  default_param_created_ = (par == NULL);

  matrix_ = M;

  n_ = M->rows();

  // Check if depth and aPriori rank should be set automatically
  autoParameterSet();

  // Form the symbolic matrix
  formSymbolicMatrix();

  // Allocate memory for permutation matrices
  permuted_symb_matrix_ = new spMatBool(n_, n_);

  // Init. RHS to all zeros
  RHS_ = new VectorXd(n_);
  RHS_->setZero();

  // Allocate memory to the solution vector
  VAR_ = new VectorXd(n_);

  if (param_->cutOffMethod() == 9) {
    computeMaxSig();
  }

  // Reserve memory for redNode, superNode, frobNorm, and totalSize lists:
  redNodeList_.reserve(param_->treeDepth() + 1);
  superNodeList_.reserve(param_->treeDepth() + 1);
  frobNorms.reserve(param_->treeDepth() + 1);
  totalSizes.reserve(param_->treeDepth() + 1);
  for (unsigned int l = 0; l <= param_->treeDepth(); l++) {
    redNodeStrList newRedLevel;
    redNodeList_.push_back(newRedLevel);
    redNodeList_.back().reserve(1 << l);

    superNodeStrList newSuperLevel;
    superNodeList_.push_back(newSuperLevel);
    superNodeList_.back().reserve(1 << l);

    frobNorms.push_back(0);
    totalSizes.push_back(0);
  }

  // Allocate memory to the pemuration vecotr
  permutationVector_ = new VectorXi(n_);

  // Allocate memory to the permutation matrix
  permutationMatrix_ = new permMat(n_);

  // Allocate and initiate full permutation matrix to identity
  fullPermutationMatrix_ = new permMat(n_);
  fullPermutationMatrix_->setIdentity();

  // Define the root of tree
  // We arbitrarily assume it is a left child (which doesn not matter)
  root_ = new RedNode(0, this, 0, 0, n_ - 1);

  // Since the 0'th level does not have any actual superNode, we just create an
  // empty list
  superNodeStrList NewLevel;
  superNodeList_.push_back(NewLevel);

  // loop over levels, create them, and perform permutations
  for (unsigned int l = 0; l < param_->treeDepth(); l++) {
    // loop over redNodes of level l, and create their black list.
    // this will be automatically followed by creating the redNode children.
    for (unsigned int i = 0; i < redNodeList_[l].size(); i++) {
      redNodeList_[l][i]->createBlackNode();
    }
    // At this point new redNodes are added to the tree.
    // Hence, we need to permute the matrix accordingly
    permuteMatrix();
  }

  // Set the global frob norm = 0 initially
  globFrobNorm = 0;
  globSize = 0;

  //! Initially no node is eliminated
  count = 0;

  //! Apply full permutation to the matrix
  applyFullPermutation();

  //! Create the columns to leaves map
  createCol2LeafMap();

  //! Create the Adjacency set for redNodes
  createAdjSet();

  //! Create the edges between leaves of the tree
  createLeafEdges();

  //! Dedicate memory, and set the RHS (and VARs) for the leaf nodes
  setLeafRHSVAR();

  setupTime_ = double(clock() - start) / CLOCKS_PER_SEC;

  //! Factorization
  factorize();

  //! Apply full permutation to the matrix
  applyInverseFullPermutation();

  //! Free permutation-related extra memory
  delete permutationVector_;
  delete permutationMatrix_;
  delete permuted_symb_matrix_;
  delete symb_matrix_;
}

HTree::~HTree() {
  if (default_param_created_) delete param_;
  if (RHS_ != NULL) delete RHS_;
  if (VAR_ != NULL) delete VAR_;
  if (fullPermutationMatrix_ != NULL) delete fullPermutationMatrix_;
  if (root_ != NULL) delete root_;
}

// Adding a new red node to the list of red nodes of the tree
void HTree::addRedNode(unsigned int l, RedNode* ptr) {
  // Push the ptr to redNode in the corresponding level
  redNodeList_[l].push_back(ptr);
}

// Adding a new red node to the list of red nodes of the tree
void HTree::addSuperNode(unsigned int l, SuperNode* ptr) {
  // Push the ptr to redNode in the corresponding level
  superNodeList_[l].push_back(ptr);
}

// Use permuteationVector and permute the matrix
// Note that the current version of Eigen does not support in-place permutation
void HTree::permuteMatrix() {
  (*permutationMatrix_) = permMat(*permutationVector_);

  *permuted_symb_matrix_ = symb_matrix_->transpose();
  *symb_matrix_ = (*permutationMatrix_) * (*permuted_symb_matrix_);
  *permuted_symb_matrix_ = symb_matrix_->transpose();
  *symb_matrix_ = (*permutationMatrix_) * (*permuted_symb_matrix_);

  *fullPermutationMatrix_ = (*permutationMatrix_) * (*fullPermutationMatrix_);
}

void HTree::createCol2LeafMap() {
  // Index of the first and last columns corresponding to each leaf node
  int first, last;

  // Pointer to the node of interest
  RedNode* Leaf;
  // loop over all leaf redNodes
  for (unsigned int i = 0; i < redNodeList_[parameters()->treeDepth()].size();
       i++) {
    Leaf = redNodeList_[parameters()->treeDepth()][i];
    first = Leaf->indexFirst();
    last = Leaf->indexLast();
    if (!Leaf->isEmpty())  // i.e., if the leaf is not empty
    {
      col2Leaf_.insert(std::pair<int, RedNode*>(first, Leaf));
    }
  }
}

void HTree::createAdjSet() {
  // Index of the first and last colum corresponding to each leaf node
  int first, last;

  // Pointer to the node of interest, and its adjacent node
  RedNode* Leaf;
  RedNode* AdjLeaf;

  // Iteration variable to work with col2Leaf map
  std::map<int, RedNode*>::iterator it;

  // First create the adjacency list for the leaf nodes
  for (unsigned int i = 0; i < redNodeList_[parameters()->treeDepth()].size();
       i++) {
    Leaf = redNodeList_[parameters()->treeDepth()][i];
    first = Leaf->indexFirst();
    last = Leaf->indexLast();

    Leaf->adjSet()->insert(Leaf);  // Make sure to consider self interaction

    if (!Leaf->isEmpty())  // i.e., if the leaf is not empty
    {
      // Extract the list of rows that have interaction with a column in this
      // leaf node
      for (int j = symbMatrix()->outerIndexPtr()[first];
           j < symbMatrix()->outerIndexPtr()[last + 1]; j++) {
        it = col2Leaf_.upper_bound(
            symbMatrix()->innerIndexPtr()[j]);  // This give us the iterator for
                                                // the leaf after the adjacent
                                                // leaf in the map
        it--;  // This is the iterator for the adjacent node
        AdjLeaf = it->second;
        Leaf->adjSet()->insert(AdjLeaf);  // Add newly found adjacent list to
                                          // the list of adjacent lists
      }
    }
  }

  // Pointer to the redNode of interest, and the left and right children of its
  // black child
  RedNode* myRedNode;
  RedNode* leftRedChild;
  RedNode* rightRedChild;

  // The adjacency set of my (grand child)
  std::set<RedNode*>* childAdjSet;

  // Now, from bottom to top we create the adjacency list of all levels redNodes
  for (int i = parameters()->treeDepth() - 1; i >= 0; i--) {
    for (unsigned int j = 0; j < redNodeList_[i].size(); j++) {
      myRedNode = redNodeList_[i][j];
      leftRedChild = myRedNode->child()->leftChild();
      rightRedChild = myRedNode->child()->rightChild();

      // Go through (grand) parent of adjacent nodes of my (grand) children, and
      // add them to my adjacent list

      // Left (grand) child
      childAdjSet = leftRedChild->adjSet();
      for (auto it = childAdjSet->begin(); it != childAdjSet->end(); ++it) {
        myRedNode->adjSet()->insert(((*it)->parent())->parent());
      }

      // Right (grand) child
      childAdjSet = rightRedChild->adjSet();
      for (auto it = childAdjSet->begin(); it != childAdjSet->end(); ++it) {
        myRedNode->adjSet()->insert(((*it)->parent())->parent());
      }
    }
  }

  // Create general adjacent set
  createGeneralAdjSet();
}

void HTree::createGeneralAdjSet() {
  for (int l = parameters()->treeDepth(); l >= 0; l--) {
    for (unsigned int i = 0; i < redNodeList_[l].size(); i++) {
      redNodeList_[l][i]->formGeneralAdjSet();
    }
  }
}

// Create the edges between leaves of the tree
void HTree::createLeafEdges() {
  // The set of adjacent nodes to a leaf
  std::set<RedNode*>* leafAdjSet;

  // go through all leaves
  for (unsigned int i = 0; i < redNodeList_.back().size(); i++) {
    // pointer to a leaf
    RedNode* leaf = redNodeList_.back()[i];
    // pointer to its adjacency list
    leafAdjSet = leaf->adjSet();
    // location of columns corresponding to this leaf
    int colFirst = leaf->indexFirst();
    int colWidth = leaf->indexLast() - colFirst + 1;

    // Go through adjacency list of each node
    for (auto it = leafAdjSet->begin(); it != leafAdjSet->end(); ++it) {
      // location of rows corresponding to the adjacent leaf
      int rowFirst = (*it)->indexFirst();
      int rowWidth = (*it)->indexLast() - rowFirst + 1;

      densMat thisBlock =
          matrix_->block(rowFirst, colFirst, rowWidth, colWidth);
      // Creating the edge and add it to in/out edges list of two leaves
      leaf->createEdge(*it, thisBlock);
    }
  }
}

// Create superNodes at level [l] of the tree
double HTree::createSuperNodes(unsigned int l) {
  // Go through all redNodes at level [l-1], then call the mergeChildren
  // function for their black child
  double time_elapsed = 0;
  for (unsigned int i = 0; i < redNodeList_[l - 1].size(); i++) {
    time_elapsed += redNodeList_[l - 1][i]->child()->mergeChildren();
  }
  return time_elapsed;
}

// Eliminate nodes at level [l] of the tree
timeTuple4 HTree::eliminate(unsigned int l) {
  timeTuple4 times;
  times[0] = 0;
  times[1] = 0;
  times[2] = 0;
  times[3] = 0;

  timeTuple2 comp_time;
  timeTuple2 sc_time;

  // Go through all superNodes at level [l], compress and apply schurComp.
  for (unsigned int i = 0; i < superNodeList_[l].size(); i++) {
    comp_time = superNodeList_[l][i]->compress();
    times[0] += comp_time[0];
    times[1] += comp_time[1];

    if (param_->combinedElimination()) {
      sc_time = superNodeList_[l][i]->combinedSchurComp();
    } else {
      sc_time = superNodeList_[l][i]->schurComp();
    }
    times[2] += sc_time[0];
    times[3] += sc_time[1];
  }
  return times;
}

// solveU blackNodes and superNodes at level[l], (superNodes solution split to
// redNodes solution)
void HTree::solveU(unsigned int l) {
  for (unsigned int i = superNodeList_[l].size(); i > 0; i--) {
    if (!(param_->combinedElimination())) {
      // Solve for the blackNode first
      superNodeList_[l][i - 1]->parent()->solveU();
    }

    // Then solve for the superNode
    superNodeList_[l][i - 1]->solveU();
  }

  for (unsigned int i = superNodeList_[l].size(); i > 0; i--) {
    // Then split solution to the redNodes
    superNodeList_[l][i - 1]->splitVAR();
  }
}

// solveL blackNodes and superNodes at level[l], (first RHS of redNodes merge to
// create RHS of superNode)
void HTree::solveL(unsigned int l) {
  // Merge RHS of redNodes to RHS of the superNode
  for (unsigned int i = 0; i < superNodeList_[l].size(); i++) {
    superNodeList_[l][i]->parent()->mergeRHS();
  }
  for (unsigned int i = 0; i < superNodeList_[l].size(); i++) {
    superNodeList_[l][i]->solveL();
    if (!(param_->combinedElimination())) {
      superNodeList_[l][i]->parent()->solveL();
    }
  }
}

// Set leaf level RHS
void HTree::setRHS(VectorXd& rhs_) {
  // first set the RHS of the leaf redNodes
  setLeafRHS(rhs_);

  // now set the RHS of all other red and black nodes zero
  for (unsigned int l = 0; l < redNodeList_.size() - 1; l++) {
    for (unsigned int i = 0; i < redNodeList_[l].size(); i++) {
      // pointer to a red node
      RedNode* rNode = redNodeList_[l][i];

      if (rNode->n() > 0) {
        // make RHS of the redNode 0
        rNode->RHS()->setZero();
      }
      if (!(param_->combinedElimination())) {
        if (rNode->child()->n() > 0) {
          // make RHS of its black child zero
          rNode->child()->RHS()->setZero();
        }
      }
    }
  }
}

// Set leaf level RHS
void HTree::setLeafRHS(VectorXd& rhs_) {
  // go through all leaves
  for (unsigned int i = 0; i < redNodeList_.back().size(); i++) {
    // pointer to a leaf
    RedNode* leaf = redNodeList_.back()[i];

    // location of columns/rows corresponding to this leaf
    int colFirst = leaf->indexFirst();
    int colWidth = leaf->indexLast() - colFirst + 1;

    // Assign the RHS
    *(leaf->RHS()) = rhs_.segment(colFirst, colWidth);
  }
}

// Allocate memory for RHS and VAR
void HTree::setLeafRHSVAR() {
  // go through all leaves
  for (unsigned int i = 0; i < redNodeList_.back().size(); i++) {
    // pointer to a leaf
    RedNode* leaf = redNodeList_.back()[i];

    // location of columns/rows corresponding to this leaf
    int colFirst = leaf->indexFirst();
    int colWidth = leaf->indexLast() - colFirst + 1;

    // Allocate memory for VAR
    leaf->VAR(new VectorXd(colWidth));

    // Allocate memory RHS
    leaf->RHS(new VectorXd(colWidth));
  }
}

// collect the solution of all leaves
VectorXd& HTree::computeSolution() {
  // number of filled
  int numFilled = 0;

  // Go through all leaves, and concatenate their solutions
  for (unsigned int i = 0; i < redNodeList_.back().size(); i++) {
    Node* Leaf = redNodeList_.back()[i];
    if (Leaf->n() > 0)  // i.e., if this leaf has any unknown
    {
      VAR_->segment(numFilled, Leaf->n()) = *(Leaf->VAR());
      numFilled += Leaf->n();
    }
  }

  *VAR_ = fullPermutationMatrix_->transpose() * (*VAR_);
  return *VAR_;
}

// Compute mean, min, and max size of redNodes at each level
void HTree::setRanks() {
  // Go through every level
  double sum_tot = 0;
  double count_tot = 0;
  for (unsigned int l = 0; l < redNodeList_.size(); l++) {
    double mean = 0;
    int minimum = 1e6;
    int maximum = -1e6;
    // Go through every redNode
    for (unsigned int i = 0; i < redNodeList_[l].size(); i++) {
      int rank = redNodeList_[l][i]->n();
      if (rank > 0) {
        count_tot++;
        sum_tot += rank;
      }
      mean += rank;
      minimum = std::min(minimum, rank);
      maximum = std::max(maximum, rank);
    }
    meanRanks_.push_back(mean / redNodeList_[l].size());
    minRanks_.push_back(minimum);
    maxRanks_.push_back(maximum);
  }
}

//! Log info
void HTree::log(std::ofstream& out) {
  out << "H-Tree setup time = " << setupTime_ << " seconds\n";
  out << "H-Tree factorizaton time = " << factorizationTime_ << " seconds\n";

  out << "H-tree total low-rank factorization time = " << total_SVD << "\n";
  out << "H-tree total GEMM time = " << total_GEMM << "\n";
  out << "H-tree total inversion time = " << total_INV << "\n";
  out << "H-tree the rest  time = " << total_REST << "\n";
  out << "H-tree depth = " << param_->treeDepth() << "\n";
  setRanks();
  out << "H-tree average red-node size =\n";
  for (int l = redNodeList_.size() - 1; l >= 0; l--) {
    out << "  level " << l << " : " << meanRanks_[l] << std::endl;
  }
}

void HTree::computeFrobNorm(unsigned int l) {
  for (unsigned int i = 0; i < superNodeList_[l].size(); i++) {
    for (auto it = superNodeList_[l][i]->outEdges.begin();
         it != superNodeList_[l][i]->outEdges.end(); ++it) {
      frobNorms[l] += std::pow(it->second->matrix().norm(), 2);
      totalSizes[l] +=
          (it->second->matrix().rows() * it->second->matrix().cols());
    }
  }
  globFrobNorm = std::sqrt(std::pow(globFrobNorm, 2) + frobNorms[l]);
  globSize += totalSizes[l];
  frobNorms[l] = std::sqrt(frobNorms[l]);
  if (parameters()->verbose()) {
    std::cout << "frobNorm at level " << l << " = " << frobNorms[l]
              << " , total size = " << totalSizes[l] << "\n";
  }
}

// First set the RHS of all super/black nodes, then call solve.
VectorXd& HTree::solve(VectorXd& RHS) {
  RHS = (*fullPermutationMatrix_) * RHS;
  // CALL SET RHS FOR THE GIVEN RHS
  setRHS(RHS);

  clock_t start, finish;
  start = clock();

  //! bottom to top traverse to solve L z = b
  for (unsigned int l = parameters()->treeDepth(); l >= 1; l--) {
    solveL(l);
  }
  //! top to bottom traverse to solve U x = z
  for (unsigned int l = 1; l <= parameters()->treeDepth(); l++) {
    solveU(l);
  }
  finish = clock();

  RHS = fullPermutationMatrix_->transpose() * RHS;

  return computeSolution();
}

//! Bottom to top traverse
/*!
  At each level, first create the superNodes (i.e., combine two redNodes), and
  then eliminate (and compress) all superNodes, as well as their black node
  parents.
*/
void HTree::factorize() {
  clock_t start = clock();
  timeTuple4 eliminate_times;
  double merge_time;
  total_SVD = 0;
  total_GEMM = 0;
  total_INV = 0;
  total_REST = 0;
  for (unsigned int l = parameters()->treeDepth(); l >= 1; l--) {
    merge_time = createSuperNodes(l);
    computeFrobNorm(l);
    eliminate_times = eliminate(l);
    if (parameters()->verbose()) {
      std::cout
          << "*************************************************************"
             "**************************************\n";
      std::cout << "   Merge: " << merge_time
                << "   Cmp(SVD): " << eliminate_times[0]
                << "   Comp(etc.): " << eliminate_times[1]
                << "   S.C.(inv): " << eliminate_times[2]
                << "   S.C.(etc.): " << eliminate_times[3] << std::endl;
      std::cout
          << "*************************************************************"
             "**************************************\n";
    }
    total_SVD += eliminate_times[0];
    total_GEMM += eliminate_times[1];
    total_GEMM += eliminate_times[3];
    total_INV += eliminate_times[2];
  }
  factorizationTime_ = double(clock() - start) / CLOCKS_PER_SEC;
}

VectorXd* HTree::retVal() {
  computeSolution();
  return VAR_;
}

void HTree::computeMaxSig() {
  VectorXd randx = VectorXd::Random(n());
  VectorXd Arandx = (*matrix_) * randx;
  maxSig_ = Arandx.norm() / randx.norm();
  int num_mult = n() / 1000;
  for (int i = 0; i < num_mult; i++) {
    Arandx = (*matrix_) * randx;
    double maxSig_temp = Arandx.norm() / randx.norm();
    if (maxSig_temp > maxSig_) {
      maxSig_ = maxSig_temp;
    }
    randx = Arandx / Arandx.norm();
  }
  if (parameters()->verbose()) {
    std::cout << "  Maximum singular value ~ " << maxSig_ << std::endl;
  }
}

void HTree::formSymbolicMatrix() {
  symb_matrix_ = new spMatBool(n_, n_);
  std::vector<TRIPLETBOOL> tripletList;
  tripletList.reserve(matrix_->outerIndexPtr()[n_]);
  for (int i = 0; i < matrix_->outerSize(); i++) {
    for (int k = matrix_->outerIndexPtr()[i];
         k < matrix_->outerIndexPtr()[i + 1]; k++) {
      int j = matrix_->innerIndexPtr()[k];
      if (i != j) {
        tripletList.push_back(TRIPLETBOOL(i, j, true));
        tripletList.push_back(TRIPLETBOOL(j, i, true));
      }
    }
  }
  symb_matrix_->setFromTriplets(tripletList.begin(), tripletList.end());
  symb_matrix_->makeCompressed();
}

void HTree::autoParameterSet() {
  if (param_->treeDepth() == 0) {
    unsigned int d =
        (unsigned int)std::max(((int)(std::log2(n_) + 0.5)) - 4, 1);
    param_->setTreeDepth(d);
  }
  if (param_->aPrioriRank() == 0) {
    unsigned int r = (unsigned int)(n_ / (1 << param_->treeDepth()));
    param_->setAPrioriRank(r);
  }
}

void HTree::applyFullPermutation() {
  spMat permuted_matrix_ = matrix_->transpose();
  *matrix_ = (*fullPermutationMatrix_) * permuted_matrix_;
  permuted_matrix_ = matrix_->transpose();
  *matrix_ = (*fullPermutationMatrix_) * (permuted_matrix_);
}

void HTree::applyInverseFullPermutation() {
  spMat permuted_matrix_ = matrix_->transpose();
  *matrix_ = fullPermutationMatrix_->transpose() * permuted_matrix_;
  permuted_matrix_ = matrix_->transpose();
  *matrix_ = fullPermutationMatrix_->transpose() * (permuted_matrix_);
}
