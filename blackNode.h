#ifndef blackNode_h
#define blackNode_h

#include <array>
#include "Eigen/Dense"
#include "node.h"

class HTree;
class RedNode;
class BlackNode;
class SuperNode;
class Hparams;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! Define a type pointer to dynamic size dense matrix stored by edge */
typedef densMat* densMatStr;

//! List of four dense matrices
/*!
  This type is used to  merge two sibling redNodes.
  Order of matrices: Ls-Ld, Rs-Ld, Ls-Rd, Rs-Rd
  Here we assumed (L)eft and (R)ight children of (S)ource and (D)estination are
  merged.
 */
typedef std::array<densMatStr, 4> densMatStr4;

//! Flag if matrices in densMatStr4 need to be transposed
/*!
This is only used in symmetric calculations.
 */
typedef std::array<bool, 4> Bool4;

/*! Tuple of times for (time1, time2) */
typedef std::array<double, 2> timeTuple2;

/**************************************************************************/
/*                            CLASS Black NODE                            */
/**************************************************************************/

//! Class blackNode
/*!
  Inherited from the general class node. It is a node correponding to the local
  variables,
  and multipole equations.
*/

class BlackNode : public Node {
  //! Pointer to the parent (which is a redNode)
  RedNode* parent_;

  //! Pointer to its left redNode child
  RedNode* leftChild_ = NULL;

  //! Pointer to its right redNode child
  RedNode* rightChild_ = NULL;

  //! Pointer to its superNode child
  SuperNode* superChild_ = NULL;

  //! Access to Htree parameters
  Hparams* hparams_;

 public:
  //! Default constructor
  BlackNode(){};

  //! Constructor:
  /*!
    input arguments:
    1) Pointer to the redNode parent
    2) Pointer to the tree
    3) Range of belonging rows/cols
  */
  BlackNode(RedNode*, HTree*, int, int);

  //! Destructor
  ~BlackNode();

  //! Returns parent_
  RedNode* parent() const { return parent_; }

  //! Returns pointer to the left child
  RedNode* leftChild() const { return leftChild_; }

  //! Returns pointer to the right child
  RedNode* rightChild() const { return rightChild_; }

  //! Returns pointer to the super child
  SuperNode* superChild() const { return superChild_; }

  //! Returns pointer to its parent
  RedNode* redParent() { return parent(); }

  //! Merge left and right redNodes to a superNode
  /*!
    Output is the time elapsed.
   */
  double mergeChildren();

  //! Merge RHS of children
  void mergeRHS();

  //! Apply schur-complement
  /*!
    This function is called immediately after its superNode child is eliminated.
   */
  timeTuple2 schurComp();
};

#endif
