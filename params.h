#ifndef params_h
#define params_h
#include <math.h>
#include <string>
#include "dataLoader.h"
#include "hparams.h"

//! Parameters class
/*!
  This class loads an input parameter file, and stores different parameters.
  All other classes can access to the parameters using this class.
  See params.in for more information.
 */

class Params {
 private:
  //! Input data: matrix, solution, RHS
  DataLoader *data_;

  //! Solve method
  unsigned int solveMethod_;

  //! How Scale the Matrix
  unsigned int scaleMatrix_;

  //! what kind of pre-elimination
  int preElimination_;

  //! Preconditioner to be used with iterative method
  unsigned int preconditioner_;

  //! Maximum number of iterations for iterative methods
  double maxIters_;

  //! Residual threshold
  double iterationEpsilon_;

  //! Iteration verbose
  bool iterationVerbose_;

  //! ILU drop tol.
  double ILUDropTol_;

  //! ILU Fill
  int ILUFill_;

  //! H-solver parameters
  Hparams *hparams_;

 public:
  //! Default constructor
  Params() {}

  //! Constructor
  /*!
    With this constructor we directly provide parameters from command line in
    one of the two forms below:
    1) path to an input parameter file (e.g., params.in)
    2) list of all parameters seperated by space (to see the required order
    check the ctor implementation )
  */
  Params(int, char **);

  //! Destructor
  ~Params() {
    delete data_;
    delete hparams_;
  }

  //! Load data from file
  /*! Should construct a parameter object with the char* of \
    the path to the input parameter file */
  /*! In the input parameter file, empty line and lines starting  \
    with # will be ignored */
  void loadFromFile(char *);

  //! Access function to input data
  DataLoader *data() const { return data_; }

  //! Access function to solve method
  unsigned int solveMethod() const { return solveMethod_; }

  //! Access function to pre-process scaling method
  unsigned int scaleMatrix() const { return scaleMatrix_; }

  //! Access function to pre-elimination method
  int preElimination() const { return preElimination_; }

  //! Access function to choice of preconditioner
  unsigned int preconditioner() const { return preconditioner_; }

  //! Access function to maximum number of iterations
  unsigned int maxIters() const { return maxIters_; }

  //! Access function to iteration convergence epsilon
  double iterationEpsilon() const { return iterationEpsilon_; }

  //! Access function to verbosity of the iterative method
  bool iterationVerbose() const { return iterationVerbose_; }

  //! Access function to ILU drop tolerance
  double ILUDropTol() const { return ILUDropTol_; }

  //! Access function to ILU fill parameter
  unsigned int ILUFill() const { return ILUFill_; }

  //! Access function to h-solver parameters
  Hparams *hparams() const { return hparams_; }
};

#endif
