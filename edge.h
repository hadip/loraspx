#ifndef edge_h
#define edge_h

#include "Eigen/Dense"

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

class Node;

//! Class edge
/*!
  This class implements a directed edge data structure used in the htree. An
  edge can connect any two nodes (red, black, superNode). It has the data of the
  corresponding interaction block.
  Note that when symmetric calculation (for symmetric matrices) is perfomred
  edges can be considered as un-directed. This case is handled using the
  function isTransposed() described below.
*/

class Edge {
  //! Pointer to the source node
  Node* source_;

  //! Pointer to the destination node
  Node* destination_;

  //! True if the edge is compressed
  bool compressed_;

  //! Pointer to the interaction matrix
  /*!
    A pointer to a dense m by n matrix
    note: n is the number of columns (variables at source node),
    and m is the number of rows (equations at destination node).
  */
  densMat* matrix_ = NULL;

 public:
  //! Default constructor
  Edge(){};

  //! Construcotr
  /*!
    Constructor inputs are:
    pointer to the source node,
    pointer to the destination node,
    pointer to the matrix associated to this node
   */
  Edge(Node*, Node*, densMat*);

  // Destructor
  ~Edge();

  //! Returns pointer to the source node
  Node* source() const { return source_; }

  //! Returns pointer to the destination node
  Node* destination() const { return destination_; }

  //! Check if this edge is between two spearated nodes
  /*!
    This function check if the edges source and separation are well separated.
    The adjacency list of nodes are used to determine that. Note that source and
    destination can be different node types at different levels.
   */
  bool isWellSeparated();

  //! Compress the edge
  /*! This function should be called after an edge is compressed, and moved to
    the parent level.
    As a result of calling this function the following steps happen:
    - Set the compressed_ flag to [true]
    - Free the matrix memory (if not NULL)
  */
  void compress();

  //! Check if the edge is eliminated
  /*!
    An edge is eliminated during the elimination process iff either its source
    or destination node is eliminated
   */
  bool isEliminated();

  //! Returns the compressed flag
  bool isCompressed() const { return compressed_; }

  //! Returns matrix
  densMat& matrix() { return *matrix_; }

  //! Returns if you need to transpose the output of matrix()
  /*
  If you view an edge from destination point of veiw you need to tranpose matrix
  the get the correct interaction.
  This option is used when a symmetric calculation is performed. In that case
  the edges are essentially not directed anymore.
  */
  bool isTransposed(Node* view);

  //! Add a matrix to the matrix associated with this edge
  /*!
  First argument: determines the view point: source_ or destination_
  Second argument: the matrix value
  */
  void addMatrix(Node*, densMat&);

  //! Overloaded version of the function addMatrix()
  /*!
  Assume source_ as the view point
  */
  void addMatrix(densMat& m) { *matrix_ += m; }

  //! Set the matrix associated with this edge assuming it already exists
  /*!
  First argument: determines the view point: source_ or destination_
  Second argument: the matrix value
  */
  void setMatrix(Node*, densMat&);

  //! Overloaded version of the function setMatrix()
  /*!
  Assume source_ as the view point
  */
  void setMatrix(densMat& m) { *matrix_ = m; }

  //! Delete matrix and make it null
  void deleteMatrix() {
    if (matrix_ != NULL) delete matrix_;
    matrix_ = NULL;
  }

  //! Create a new edge to the graph only if it does not exist
  /*!
  first argument: pointer to edge's source node
  first argument: pointer to edge's destination node
  second argument: reference to the matrix value of the edge

  If the edge already exists this function does nothing
  */
  static void createEdge(Node* source, Node* destination, densMat& mat);

  // Symmetric version of the createEdge() (only use outEdges map)
  static void createEdgeSymmetric(Node* source, Node* destination,
                                  densMat& mat);

  //! A helper function to create/update a new edge to the graph
  /*!
  first argument: pointer to edge's source node
  first argument: pointer to edge's destination node
  second argument: reference to the matrix value of the edge

  If the edge already exists this function adds the matrix value to the existing
  matrix value.
  */
  static void createUpdateEdge(Node* source, Node* destination, densMat& mat);

  // Symmetric version of the createUpdateEdge() (only use outEdges map)
  static void createUpdateEdgeSymmetric(Node* source, Node* destination,
                                        densMat& mat);
};

#endif
