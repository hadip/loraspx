#include "dataLoader.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "time.h"

DataLoader::DataLoader(spMat* A, VectorXd* b, VectorXd* x) {
  clock_t start = clock();
  matrix_ = A;
  size_ = A->rows();

  rhs_ = b;
  sol_ = x;

  numRHS_ = 1;

  extraVectorMemory_ = false;
  extraMatrixMemory_ = false;
  solutionAvailable_ = true;
  time_ = double(clock() - start) / CLOCKS_PER_SEC;
}

DataLoader::DataLoader(spMat* A, densMat* B, densMat* X) {
  clock_t start = clock();
  matrix_ = A;
  rhsMatrix_ = B;
  solMatrix_ = X;

  size_ = A->rows();
  numRHS_ = B->cols();

  rhs_ = new VectorXd(size_);
  sol_ = new VectorXd(size_);
  extraVectorMemory_ = true;
  extraMatrixMemory_ = false;
  solutionAvailable_ = true;
  time_ = double(clock() - start) / CLOCKS_PER_SEC;
}

DataLoader::DataLoader(std::string inputMatrixFile, std::string inputRHSFile,
                       std::string inputSolFile) {
  clock_t start = clock();
  matrix_ = readSparseMatrix((char*)inputMatrixFile.c_str());

  size_ = matrix_->rows();

  rhs_ = new VectorXd(size_);
  sol_ = new VectorXd(size_);
  extraVectorMemory_ = true;
  extraMatrixMemory_ = true;

  if (inputSolFile == "NA") {  // solution not available
    if (inputRHSFile == "USE_RANDOM") {
      (*rhs_) = VectorXd::Random(size_);
      numRHS_ = 1;
    } else {
      rhsMatrix_ = readDenseMatrix((char*)inputRHSFile.c_str());
      numRHS_ = rhsMatrix_->cols();
    }
    solutionAvailable_ = false;
  } else {
    if (inputSolFile == "USE_RANDOM") {
      (*sol_) = VectorXd::Random(size_);
      (*rhs_) = (*matrix_) * (*sol_);
      numRHS_ = 1;
    } else {  // solution is provided
      solMatrix_ = readDenseMatrix((char*)inputSolFile.c_str());
      numRHS_ = solMatrix_->cols();
      rhsMatrix_ = new densMat(size_, numRHS_);
      for (int i = 0; i < numRHS_; i++) {
        rhsMatrix_->col(i) = (*matrix_) * solMatrix_->col(i);
      }
    }
    solutionAvailable_ = true;
  }
  time_ = double(clock() - start) / CLOCKS_PER_SEC;
}
DataLoader::~DataLoader() {
  if (extraMatrixMemory_) {
    if (matrix_ != NULL) {
      delete matrix_;
    }
    if (rhsMatrix_ != NULL) {
      delete rhsMatrix_;
    }
    if (solMatrix_ != NULL) {
      delete solMatrix_;
    }
  }
  if (extraVectorMemory_) {
    if (rhs_ != NULL) {
      delete rhs_;
    }
    if (sol_ != NULL) {
      delete sol_;
    }
  }
}

VectorXd* DataLoader::rhs(int j) {
  if (j > numRHS_) {
    std::cout << "Requested RHS is out-of-bound!" << std::endl;
    exit(1);
  }
  if (extraVectorMemory_ == true && rhsMatrix_ != NULL) {
    *rhs_ = rhsMatrix_->col(j);
    return rhs_;

  } else {
    return rhs_;
  }
}

VectorXd* DataLoader::sol(int j) {
  if (solutionAvailable_ == false) {
    std::cout << "Solution vector is not available" << std::endl;
    exit(1);
  }
  if (j > numRHS_) {
    std::cout << "Requested solution is out-of-bound!" << std::endl;
    exit(1);
  }
  if (extraVectorMemory_ == true && solMatrix_ != NULL) {
    *sol_ = solMatrix_->col(j);
    return sol_;

  } else {
    return sol_;
  }
}

densMat* DataLoader::readDenseMatrix(char* path) {
  std::ifstream matrixFile(path);

  int nrows, ncols, nnz;
  matrixFile >> nrows >> ncols >> nnz;

  densMat* M = new densMat(nrows, ncols);
  M->setZero();

  int i, j;
  double v;
  for (int line = 0; line < nnz; line++) {
    matrixFile >> i >> j >> v;
    //  Changing index from 1,2,.. to 0,1,..
    (*M)(i - 1, j - 1) = v;
  }
  matrixFile.close();
  return M;
}

spMat* DataLoader::readSparseMatrix(char* path) {
  std::ifstream matrixFile(path);

  std::string firstLine;
  std::getline(matrixFile, firstLine);

  bool symmetric = false;
  if (firstLine[0] == '%')  // i.e., file has a direction header
  {
    std::size_t found = firstLine.find("real symmetric");
    if (found != std::string::npos)  // i.e., it's symmetric
    {
      symmetric = true;
    }
  } else {
    std::istringstream firstLineIn(firstLine);
  }

  int nrows, ncols, nnz;
  std::istringstream firstLineIn(firstLine);
  firstLineIn >> nrows >> ncols >> nnz;

  if (nrows != ncols) {
    std::cout << " This code does not suppot non-square matrices!" << std::endl;
    exit(1);
  }

  int i, j;
  double v;

  // Reserve memory for the triplet list vector
  std::vector<TRIPLET> tripletList;

  tripletList.reserve((nnz - nrows) * (1 + symmetric));

  for (int line = 0; line < nnz; line++) {
    matrixFile >> i >> j >> v;
    //  Changing index from 1,2,.. to 0,1,..
    if (i != j) {
      tripletList.push_back(TRIPLET(i - 1, j - 1, v));
      // FOR SYMMETRIC MATRIX
      if (symmetric) {
        tripletList.push_back(TRIPLET(j - 1, i - 1, v));
      }
    } else  // make the matrix indefinite
    {
      tripletList.push_back(TRIPLET(i - 1, j - 1, v));
    }
  }
  matrixFile.close();

  spMat* M = new spMat(nrows, ncols);
  M->setFromTriplets(tripletList.begin(), tripletList.end());
  M->makeCompressed();
  return M;
}

void DataLoader::log(std::ofstream& out) {
  out << "Matrix size = " << size_ << std::endl;
  out << "Matrix norm = " << matrix_->norm() << std::endl;
  out << "Number of RHS = " << numRHS_ << std::endl;
  out << "Data loading time = " << time_ << " seconds" << std::endl;
}
