#include "tree.h"
#include "redNode.h"
#include "blackNode.h"
#include "superNode.h"
#include "params.h"
#include "edge.h"
#include "Poisson.h"
#include "Eigen/Sparse"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include "time.h"

#define MTX_RHS 0


// Constructor
tree::tree(params* par)
{

  // param_ get value
  param_ = par;
  double start,finish;
  /*******************************************************/
  /* READ MATRIX AND RHS FROM FILE AND STORE AS TRIPLETS */
  /*******************************************************/
  
  // Read the input matrix data
  /* We will store input data as triplets */
  start=clock();
  std::ifstream matrix_file( (char*) (param_->Input_Matrix_File().c_str()) );
  std::ifstream rhs_file( (char*) (param_->Input_Rhs_File().c_str()) );

  //number of rows, columns, and non-zero elements in the matrix
  int nrows , ncols;
  
  std::string first_line;
  std::getline(matrix_file,first_line);
  bool symmetric = false;
  if ( first_line[0]=='%' ) // i.e., file has a direction header
    {
      std::size_t found = first_line.find("real symmetric");
      if ( found != std::string::npos ) //i.e., it's symmetric
	{
	  symmetric = true;
	  std::cout<<"SYMMETRIC!\n";
	}
      matrix_file >> nrows >> ncols >> nnz;
    }
  else
    {
      std::istringstream first_line_in(first_line);
      first_line_in >> nrows >> ncols >> nnz;
    }
  
  n_ = nrows; 

  additional_vec = 0;
  std::cout<<"numRows = "<<nrows<<" , numCols = "<<ncols<<" , NNZ = "<<nnz<<"\n";

  if ( nrows != ncols )
    {
      std::cout<<" This code does not suppot non-square matrices!"<<std::endl;
      exit(1);
    }
  
  // The i,j indecies of an entry
  int index_i, index_j;

  // The value of an entry
  double value_ij;

  // Reserve memory for the triplet list vector
  TripletList_.reserve(n_);
  TripletBoolList_.reserve(n_);

  int percentage = 10;
  std::cout<<"Matrix loading: ";
  for ( int i = 0; i < nnz; i++ )
    {
      if(i*1.0/nnz*100>percentage){
	//std::cout<<percentage<<"% "<<std::endl;
	percentage+=10;
      }
      matrix_file>>index_i>>index_j>>value_ij;
      //  Changing index from 1,2,.. to 0,1,.. 
      TripletList_.push_back( TRIPLET( index_i-1, index_j-1, value_ij ) );
      if ( index_i != index_j )
	{
	  TripletBoolList_.push_back( TRIPLETBOOL( index_i-1 , index_j-1 , true));
	  // FOR SYMMETRIC MATRIX
	  if ( symmetric )
	    {
	      TripletList_.push_back( TRIPLET( index_j-1, index_i-1, value_ij ) );
	      TripletBoolList_.push_back( TRIPLETBOOL( index_j-1 , index_i-1 , true));
	    }
	}
    }
  std::cout<<std::endl;
  
  matrix_file.close();

  finish = clock();
  std::cout<<"Matrix loading takes "<< double(finish-start)/CLOCKS_PER_SEC<<std::endl;
  // Init. RHS to all zeros
  RHS_ = new VectorXd( nrows );
  *RHS_ = VectorXd::Zero( nrows );

  RHS_original_ = new VectorXd( nrows );
  *RHS_original_ = VectorXd::Zero( nrows );

  // used for iterative methods
  rhs_  = new VectorXd(nrows);
  *rhs_ = VectorXd::Zero(nrows);

  x_n  = new VectorXd(nrows);
  *x_n = VectorXd::Zero(nrows);

#if MTX_RHS
  rhs_file>> nrows>>ncols>>nnz;
  std::cout<<"rhs size:"<< nrows <<' ' <<ncols<<' '<<nnz<<std::endl;
  if(ncols!=1){
    std::cout<<"Rhs has incorrect format!"<<std::endl;
    exit(0);
  }
#else
  rhs_file>>ncols;
  std::cout<<"rhs size"<<ncols<<std::endl;
#endif


  //  std::cout<< "current precision is "<< rhs_file.precision()<<std::endl;
  rhs_file.precision(15);

#if MTX_RHS
  for(int i=0;i<nnz;i++)
    {
      rhs_file>>index_i>>index_j>>value_ij;
      (*RHS_)(index_i-1)=value_ij;
      (*RHS_original_)(index_i-1) = value_ij;
    }
#else 
  for(int i=0;i<ncols;i++){
    rhs_file>>value_ij;
    (*RHS_)(i)=value_ij;
    (*RHS_original_)(i) = value_ij;
  }
#endif
  std::cout<< "precision after reading rhs is "<<rhs_file.precision()<<std::endl;
  // Allocate memory to the solution vector
  VAR_ = new VectorXd( n_ );

  
  // Assigne memory for the full matrix
  matrix_ = new spMat(n_,n_);
  symb_matrix_ = new spMatBool(n_,n_);
  permuted_matrix_ = new spMat(n_,n_);
  permuted_symb_matrix_ = new spMatBool(n_,n_);

  // Make a SparseMatrix from triplets
  matrix_->setFromTriplets( TripletList_.begin(),TripletList_.end() );
  symb_matrix_->setFromTriplets( TripletBoolList_.begin(),TripletBoolList_.end() );

  // Make the matrix in regular compressed sparse form
  //normalize_rows( matrix_ );
  matrix_ -> makeCompressed();
  symb_matrix_ -> makeCompressed();

  // Max max per column = 1
  if ( param_ -> normCols() )
    {
      normalize_cols( matrix_ );
    }

  // Reserve memory for redNode, superNode, frobNorm, and totalSize lists:
  redNodeList_.reserve( param_->treeLevelThreshold() + 1 );
  superNodeList_.reserve( param_->treeLevelThreshold() + 1 );
  frobNorms.reserve( param_->treeLevelThreshold() + 1 );
  totalSizes.reserve( param_->treeLevelThreshold() + 1 );
  for ( unsigned int l = 0; l <= param_->treeLevelThreshold(); l++ )
    {
      redNodeStrList newRedLevel;
      redNodeList_.push_back( newRedLevel );
      redNodeList_.back().reserve( 1<<l );
      
      superNodeStrList newSuperLevel;
      superNodeList_.push_back( newSuperLevel );
      superNodeList_.back().reserve( 1<<l );
      
      frobNorms.push_back(0);
      totalSizes.push_back(0);      
    }

  start = clock();  
  /*******************************************************/
  /*                  Create the tree                    */
  /*******************************************************/
  
  //! Constructing the root
  /*! 
    The root of the tree is a redNode.
    We need to pass the range of columns that belong to each node.
    The tree will be created in a BFS order.
    The order of things need to be done:
    - create the root
    - When a redNode born it will be added to the redNodelist
    - for the last created level do the permuation
    - for the last created level decide if further subdividing is required (based on threshold_level)
    - if yes for all redNodes in the last level call createBlacknode()
  */

  // Allocate memory to the pemuration vecotr
  permutationVector = new VectorXi(n_);

  permutationVector_history = new VectorXi(n_);

  for(int i=0; i<n_; i++)
    (*permutationVector_history)(i) = i;

  //Allocate memory to the permutation matrix
  permutationMatrix = new permMat(n_);

  // Define the root of tree
  // We arbitrarily assume it is a left child (which doesn not matter)
  root_ = new redNode( 0, this, 0, 0, n_-1 );

  // Since the 0'th level does not have any actual superNode, we just create an empty list
  superNodeStrList NewLevel;
  superNodeList_.push_back(NewLevel);
  
  // std::cout<<"The original Symb Matrix = "<<*symb_matrix_<<std::endl;

  // loop over levels, create them, and perform permutations
  for ( unsigned int l = 0 ; l < param_->treeLevelThreshold(); l++ )
    {
      std::cout<<"Creating level "<<l<<" of the tree..."<<std::endl;
      // loop over redNodes of level l, and create their black list.
      // this will be automatically followed by creating the redNode children.
      for ( unsigned int i = 0; i < redNodeList_[l].size(); i++ )
	{
	  redNodeList_[l][i]->createBlackNode();
	}
      // At this point new redNodes are added to the tree.
      // Hence, we need to permute the matrix accordingly
      permuteMatrix();
      // std::cout<<"The Symb Matrix after creating level "<<l+1<<" = "<<*symb_matrix_<<std::endl;
    }
  finish=clock();
  std::cout<<"Tree creating takes "<< double(finish-start)/CLOCKS_PER_SEC<<std::endl;
  //! set the global frob norm = 0 initially
  globFrobNorm = 0;
  globSize = 0;

  //! Initially no node is eliminated
  count = 0;

  //! Initially no padding and no large pivot is assumed
  padding = false;
  largePivot = false;

}





// Constructor using predefined Poisson equation
tree::tree(params* par, Poisson *problem)
{

  // param_ get value
  param_ = par;
  problem_ = problem;
  
  double start,finish;
  /*******************************************************/
  /* READ MATRIX AND RHS FROM FILE AND STORE AS TRIPLETS */
  /*******************************************************/
  
  // Read the input matrix data
  /* We will store input data as triplets */
  start=clock();
  /*  
  std::ifstream matrix_file( (char*) (param_->Input_Matrix_File().c_str()) );
  std::ifstream rhs_file( (char*) (param_->Input_Rhs_File().c_str()) );

  //number of rows, columns, and non-zero elements in the matrix
  int nrows , ncols;
  
  std::string first_line;
  std::getline(matrix_file,first_line);
  bool symmetric = false;
  if ( first_line[0]=='%' ) // i.e., file has a direction header
    {
      std::size_t found = first_line.find("real symmetric");
      if ( found != std::string::npos ) //i.e., it's symmetric
	{
	  symmetric = true;
	  std::cout<<"SYMMETRIC!\n";
	}
      matrix_file >> nrows >> ncols >> nnz;
    }
  else
    {
      std::istringstream first_line_in(first_line);
      first_line_in >> nrows >> ncols >> nnz;
    }
  
  n_ = nrows; 

  additional_vec = 0;
  std::cout<<"numRows = "<<nrows<<" , numCols = "<<ncols<<" , NNZ = "<<nnz<<"\n";

  if ( nrows != ncols )
    {
      std::cout<<" This code does not suppot non-square matrices!"<<std::endl;
      exit(1);
    }
  
  // The i,j indecies of an entry
  int index_i, index_j;

  // The value of an entry
  double value_ij;

  // Reserve memory for the triplet list vector
  TripletList_.reserve(n_);
  TripletBoolList_.reserve(n_);

  int percentage = 10;
  std::cout<<"Matrix loading: ";
  for ( int i = 0; i < nnz; i++ )
    {
      if(i*1.0/nnz*100>percentage){
	//std::cout<<percentage<<"% "<<std::endl;
	percentage+=10;
      }
      matrix_file>>index_i>>index_j>>value_ij;
      //  Changing index from 1,2,.. to 0,1,.. 
      TripletList_.push_back( TRIPLET( index_i-1, index_j-1, value_ij ) );
      if ( index_i != index_j )
	{
	  TripletBoolList_.push_back( TRIPLETBOOL( index_i-1 , index_j-1 , true));
	  // FOR SYMMETRIC MATRIX
	  if ( symmetric )
	    {
	      TripletList_.push_back( TRIPLET( index_j-1, index_i-1, value_ij ) );
	      TripletBoolList_.push_back( TRIPLETBOOL( index_j-1 , index_i-1 , true));
	    }
	}
    }
  std::cout<<std::endl;
  
  matrix_file.close();

  finish = clock();
  std::cout<<"Matrix loading takes "<< double(finish-start)/CLOCKS_PER_SEC<<std::endl;
  // Init. RHS to all zeros
  RHS_ = new VectorXd( nrows );
  *RHS_ = VectorXd::Zero( nrows );

  RHS_original_ = new VectorXd( nrows );
  *RHS_original_ = VectorXd::Zero( nrows );

  // used for iterative methods
  rhs_  = new VectorXd(nrows);
  *rhs_ = VectorXd::Zero(nrows);

  x_n  = new VectorXd(nrows);
  *x_n = VectorXd::Zero(nrows);

#if MTX_RHS
  rhs_file>> nrows>>ncols>>nnz;
  std::cout<<"rhs size:"<< nrows <<' ' <<ncols<<' '<<nnz<<std::endl;
  if(ncols!=1){
    std::cout<<"Rhs has incorrect format!"<<std::endl;
    exit(0);
  }
#else
  rhs_file>>ncols;
  std::cout<<"rhs size"<<ncols<<std::endl;
#endif


  //  std::cout<< "current precision is "<< rhs_file.precision()<<std::endl;
  rhs_file.precision(15);

#if MTX_RHS
  for(int i=0;i<nnz;i++)
    {
      rhs_file>>index_i>>index_j>>value_ij;
      (*RHS_)(index_i-1)=value_ij;
      (*RHS_original_)(index_i-1) = value_ij;
    }
#else 
  for(int i=0;i<ncols;i++){
    rhs_file>>value_ij;
    (*RHS_)(i)=value_ij;
    (*RHS_original_)(i) = value_ij;
  }
#endif
  std::cout<< "precision after reading rhs is "<<rhs_file.precision()<<std::endl;
  // Allocate memory to the solution vector
  VAR_ = new VectorXd( n_ );

  
  // Assigne memory for the full matrix
  matrix_ = new spMat(n_,n_);
  symb_matrix_ = new spMatBool(n_,n_);
  permuted_matrix_ = new spMat(n_,n_);
  permuted_symb_matrix_ = new spMatBool(n_,n_);

  // Make a SparseMatrix from triplets
  matrix_->setFromTriplets( TripletList_.begin(),TripletList_.end() );
  symb_matrix_->setFromTriplets( TripletBoolList_.begin(),TripletBoolList_.end() );

  // Make the matrix in regular compressed sparse form
  //normalize_rows( matrix_ );
  matrix_ -> makeCompressed();
  symb_matrix_ -> makeCompressed();

  */
  n_ = problem->rows();
  int nrows = n_;
  additional_vec = 0;
  matrix_ = new spMat(n_,n_);
  symb_matrix_ = new spMatBool(n_,n_);
  permuted_matrix_ = new spMat(n_,n_);
  permuted_symb_matrix_ = new spMatBool(n_,n_);

  *matrix_ = *(problem->Matrix());
  *symb_matrix_ = *(problem->symbMatrix());


      // Init. RHS to all zeros
  RHS_ = new VectorXd( nrows );
  *RHS_ = VectorXd::Zero( nrows );

  RHS_original_ = new VectorXd( nrows );
  *RHS_original_ = VectorXd::Zero( nrows );

  // used for iterative methods
  rhs_  = new VectorXd(nrows);
  *rhs_ = VectorXd::Zero(nrows);

  x_n  = new VectorXd(nrows);
  *x_n = VectorXd::Zero(nrows);

  *RHS_ = *(problem->RHS());
  *RHS_original_ = *(problem->RHS());

    VAR_ = new VectorXd( n_ );




  
  // Max max per column = 1
  if ( param_ -> normCols() )
    {
      normalize_cols( matrix_ );
    }

  // Reserve memory for redNode, superNode, frobNorm, and totalSize lists:
  redNodeList_.reserve( param_->treeLevelThreshold() + 1 );
  superNodeList_.reserve( param_->treeLevelThreshold() + 1 );
  frobNorms.reserve( param_->treeLevelThreshold() + 1 );
  totalSizes.reserve( param_->treeLevelThreshold() + 1 );
  for ( unsigned int l = 0; l <= param_->treeLevelThreshold(); l++ )
    {
      redNodeStrList newRedLevel;
      redNodeList_.push_back( newRedLevel );
      redNodeList_.back().reserve( 1<<l );
      
      superNodeStrList newSuperLevel;
      superNodeList_.push_back( newSuperLevel );
      superNodeList_.back().reserve( 1<<l );
      
      frobNorms.push_back(0);
      totalSizes.push_back(0);      
    }

  start = clock();  
  /*******************************************************/
  /*                  Create the tree                    */
  /*******************************************************/
  
  //! Constructing the root
  /*! 
    The root of the tree is a redNode.
    We need to pass the range of columns that belong to each node.
    The tree will be created in a BFS order.
    The order of things need to be done:
    - create the root
    - When a redNode born it will be added to the redNodelist
    - for the last created level do the permuation
    - for the last created level decide if further subdividing is required (based on threshold_level)
    - if yes for all redNodes in the last level call createBlacknode()
  */

  // Allocate memory to the pemuration vecotr
  permutationVector = new VectorXi(n_);

  permutationVector_history = new VectorXi(n_);

  for(int i=0; i<n_; i++)
    (*permutationVector_history)(i) = i;

  //Allocate memory to the permutation matrix
  permutationMatrix = new permMat(n_);

  // Define the root of tree
  // We arbitrarily assume it is a left child (which doesn not matter)
  root_ = new redNode( 0, this, 0, 0, n_-1 );

  // Since the 0'th level does not have any actual superNode, we just create an empty list
  superNodeStrList NewLevel;
  superNodeList_.push_back(NewLevel);
  
  // std::cout<<"The original Symb Matrix = "<<*symb_matrix_<<std::endl;

  // loop over levels, create them, and perform permutations
  for ( unsigned int l = 0 ; l < param_->treeLevelThreshold(); l++ )
    {
      std::cout<<"Creating level "<<l<<" of the tree..."<<std::endl;
      // loop over redNodes of level l, and create their black list.
      // this will be automatically followed by creating the redNode children.
      for ( unsigned int i = 0; i < redNodeList_[l].size(); i++ )
	{
	  redNodeList_[l][i]->createBlackNode();
	}
      // At this point new redNodes are added to the tree.
      // Hence, we need to permute the matrix accordingly
      permuteMatrix();
      // std::cout<<"The Symb Matrix after creating level "<<l+1<<" = "<<*symb_matrix_<<std::endl;
    }
  finish=clock();
  std::cout<<"Tree creating takes "<< double(finish-start)/CLOCKS_PER_SEC<<std::endl;
  //! set the global frob norm = 0 initially
  globFrobNorm = 0;
  globSize = 0;

  //! Initially no node is eliminated
  count = 0;

  //! Initially no padding and no large pivot is assumed
  padding = false;
  largePivot = false;

}


// Adding a new red node to the list of red nodes of the tree
void tree::addRedNode( unsigned int l, redNode* ptr)
{

  // Check if this is the first redNode of a new level
  /*
  if ( l >= redNodeList_.size() )
    {
      redNodeStrList NewLevel;
      redNodeList_.push_back(NewLevel);
    }
  */

  // Push the ptr to redNode in the corresponding level
  redNodeList_[l].push_back(ptr);
}

// Adding a new red node to the list of red nodes of the tree
void tree::addSuperNode( unsigned int l, superNode* ptr)
{
  // Check if this is the first redNode of a new level
  /*
  if ( l >= superNodeList_.size() )
    {
      superNodeStrList NewLevel;
      superNodeList_.push_back(NewLevel);
      // std::cout<<" this happend with l = "<<l<<"\n";

      // Also push back 0 to FrobNorm list of Frob. norms
      frobNorms.push_back(0);
      totalSizes.push_back(0);
    }
  */

  // Push the ptr to redNode in the corresponding level
  superNodeList_[l].push_back(ptr);
}


// Use permuteationVector and permute the matrix
// Note that the current version of Eigen does not support in-place permutation
void tree::permuteMatrix()
{
  (*permutationMatrix) = permMat(*permutationVector);
    
  *permuted_matrix_ = matrix_->transpose();
  *matrix_ = (*permutationMatrix) * (*permuted_matrix_);
  *permuted_matrix_ = matrix_->transpose();
  *matrix_ = (*permutationMatrix) * (*permuted_matrix_);

  *permuted_symb_matrix_ = symb_matrix_->transpose();
  *symb_matrix_ = (*permutationMatrix) * (*permuted_symb_matrix_);
  *permuted_symb_matrix_ = symb_matrix_->transpose();
  *symb_matrix_ = (*permutationMatrix) * (*permuted_symb_matrix_);

  (*RHS_) = (*permutationMatrix) * (*RHS_);
  (*permutationVector_history) = (*permutationMatrix) * (*permutationVector_history);
}


// permute the solution of the original order of the input data to the order currently used in IFMM code
void tree::permuteSol(VectorXd *Sol)
{
  (*permutationMatrix) = permMat(*permutationVector_history);

  *Sol = (permutationMatrix->transpose())* (*Sol);
}

void tree::dumpPermuteVector()
{
  std::ofstream permute_file;
  permute_file.open("permute_vec.txt",std::ofstream::out);
  permute_file<<*permutationVector_history;
}




// permute the solution obtained in this code to the original order of the input data
void tree::permuteSolBack(VectorXd *Sol)
{
  (*permutationMatrix) = permMat(*permutationVector_history\
				 );

  *Sol = (*permutationMatrix)* (*Sol);
}


void tree::createCol2LeafMap()
{

  // Index of the first and last columns corresponding to each leaf node
  int first, last;

  // Pointer to the node of interest
  redNode* Leaf;
  // loop over all leaf redNodes
  for ( unsigned int i = 0 ; i < redNodeList_[maxLevel()].size(); i++ )
    {
      Leaf = redNodeList_[maxLevel()][i];
      first = Leaf->IndexFirst();
      last = Leaf->IndexLast();
      //Debug
      /*      
      std::cout<<"Node "<<i<<" [";
      for(int i=first;i<=last;i++){
	std::cout<< (*permutationVector_history)(i)<<'\t';
      }
      std::cout<<"]"<<std::endl;
      //*/
      if ( !Leaf->isEmpty() ) //i.e., if the leaf is not empty
	{
	  col2Leaf_.insert( std::pair<int,redNode*> (first, Leaf ) );
	}
    }

}

void tree::createAdjList()
{

  // Index of the first and last colum corresponding to each leaf node
  int first, last;

  // Pointer to the node of interest, and its adjacent node
  redNode* Leaf;
  redNode* AdjLeaf;

  // Iteration variable to work with col2Leaf map
  std::map<int,redNode*>::iterator it;

  // First create the adjacency list for the leaf nodes
    for ( unsigned int i = 0 ; i < redNodeList_[maxLevel()].size(); i++ )
    {
      Leaf = redNodeList_[maxLevel()][i];
      Leaf->label = i;
      first = Leaf->IndexFirst();
      last = Leaf->IndexLast();

      Leaf -> AdjList() -> insert(Leaf); // Make sure to consider self interaction

      if ( !Leaf -> isEmpty() ) //i.e., if the leaf is not empty
	{
	  // Extract the list of rows that have interaction with a column in this leaf node
	  for ( int j = SymbMatrix()->outerIndexPtr()[first]; j < SymbMatrix()->outerIndexPtr()[last+1]; j++ )
	    {
	      it = col2Leaf_.upper_bound(SymbMatrix()->innerIndexPtr()[j]); //This give us the iterator for the leaf after the adjacent leaf in the map
	      it--; // This is the iterator for the adjacent node
	      AdjLeaf = it->second;
	      Leaf -> AdjList() -> insert(AdjLeaf); // Add newly found adjacent list to the list of adjacent lists
	    }
	}
    }

    //Find 2D corner neighbors: although not directly connect, each of them is connected to at least 2 immediate neighbors of the current red node.
    std::map<redNode*, int> visited;

    for(unsigned int i = 0; i < redNodeList_[maxLevel()].size(); i++){
      Leaf = redNodeList_[maxLevel()][i];
      //go over the neighbors of the current leaf node
      for(std::set<redNode*>::iterator it=Leaf->AdjList()->begin(); it!=Leaf->AdjList()->end();++it){
	//go over the neighbors of the current neighbor
	for(std::set<redNode*>::iterator iit=(*it)->AdjList()->begin();iit!=(*it)->AdjList()->end();++iit){
	  //if not a neighbor of current leaf
	  if(Leaf->AdjList()->find(*iit)==Leaf->AdjList()->end()){
	    if(visited.find(*iit)==visited.end()){
	      visited[*iit] = 1;
	    }
	    else{
	      visited[*iit] += 1;
	    }
	  }

	}//end for iit
      }//end for it

      // go over visited nodes and pick the 2D corner neighbors
      for(std::map<redNode*, int>::iterator it=visited.begin(); it!=visited.end(); ++it){
	if(it->second>1){
	  //add a corner neighbor
	  Leaf->SecAdjList()->insert(it->first);
	}
      }
      visited.clear();
      
    }


    //Find 3D corner neighbors: although not directly connected, each of them is connected to at least 3 secondary neighbors of the current red node.


    for(unsigned int i = 0; i < redNodeList_[maxLevel()].size(); i++){
      Leaf = redNodeList_[maxLevel()][i];
      //go over the secondary neighbors of the current leaf node
      for(std::set<redNode*>::iterator it=Leaf->SecAdjList()->begin(); it!=Leaf->SecAdjList()->end();++it){
	//go over the neighbors of the secondary neighbors
	for(std::set<redNode*>::iterator iit=(*it)->AdjList()->begin();iit!=(*it)->AdjList()->end();++iit){
	  //if not a neighbor or secondary neighbor of current leaf
	  if((Leaf->AdjList()->find(*iit)==Leaf->AdjList()->end()) && (Leaf->SecAdjList()->find(*iit)==Leaf->SecAdjList()->end())){
	    if(visited.find(*iit)==visited.end()){
	      visited[*iit] = 1;
	    }
	    else{
	      visited[*iit] += 1;
	    }
	  }

	}//end for iit
      }//end for it

      // go over visited nodes and pick the 3D corner neighbors
      for(std::map<redNode*, int>::iterator it=visited.begin(); it!=visited.end(); ++it){
	if(it->second>2){
	  //add a corner neighbor
	  Leaf->ThiAdjList()->insert(it->first);
	}
      }
      visited.clear();
      
    }
    // check the numbers--debug
  for(unsigned int i = 0; i < redNodeList_[maxLevel()].size();i++){
      Leaf = redNodeList_[maxLevel()][i];
      //if(Leaf->n()!=8){
      //	std::cout<<"Leaf: "<<Leaf->n()<<std::endl;
      //	assert(0);
      //}
      //assert(Leaf->n()==8);
      //      std::cout<<"Node "<<i<<", neb: "<<Leaf->AdjList()->size()<<", 2nd neb: "<<Leaf->SecAdjList()->size()<<", 3rd neb: "<<Leaf->ThiAdjList()->size()<<std::endl;

    }



  
    //Check symmetry--debug
  int usymm1 = 0, usymm2=0, usymm3=0, totalEdge=0;
    for(unsigned int i = 0; i < redNodeList_[maxLevel()].size();i++){
      Leaf = redNodeList_[maxLevel()][i];
      for(std::set<redNode*>::iterator it = Leaf->AdjList()->begin();it!=Leaf->AdjList()->end();++it){
	totalEdge+=1;
	if((*it)->AdjList()->find(Leaf)==(*it)->AdjList()->end()){
	  //std::cout<<"Unsymmetry found!"<<std::endl;
	  usymm1+=1;
	}
      }
      for(std::set<redNode*>::iterator it = Leaf->SecAdjList()->begin();it!=Leaf->SecAdjList()->end();++it){
	if((*it)->SecAdjList()->find(Leaf)==(*it)->SecAdjList()->end()){
	  //std::cout<<"Unsymmetry found!"<<std::endl;
	  usymm2+=1;
	}
      }
      for(std::set<redNode*>::iterator it = Leaf->ThiAdjList()->begin();it!=Leaf->ThiAdjList()->end();++it){
	if((*it)->ThiAdjList()->find(Leaf)==(*it)->ThiAdjList()->end()){
	  //std::cout<<"Unsymmetry found!"<<std::endl;
	  usymm3+=1;
	}
      }      

    }
    //    std::cout<<"Total nodes: "<<redNodeList_[maxLevel()].size()<<", total edges: "<<totalEdge<<", unsym: "<<usymm<<std::endl;

    std::cout<<"Usymm1= "<<usymm1<<", usymm2= "<<usymm2<<", usymm3= "<<usymm3<<std::endl;
    // merge the AdjList, SecAdjList and ThiAdjList

    for(unsigned int i=0; i< redNodeList_[maxLevel()].size();i++){
      Leaf = redNodeList_[maxLevel()][i];
      //Debug
      // std::cout<<"Node "<<i<<"'s initial neighbor: ";
      //print original AdjList
      //for(redNode* neb : *(Leaf->AdjList())){
      //	std::cout<< neb->label<<" ";
      //}
      //std::cout<<std::endl;
      // Add secondary AdjList
      for(std::set<redNode*>::iterator it=Leaf->SecAdjList()->begin(); it!=Leaf->SecAdjList()->end();++it){
	Leaf->AdjList()->insert(*it);
      }
      Leaf->SecAdjList()->clear();

      //std::cout<<"Node "<<i<<"'s with secondary neighbor: ";
      //print new AdjList--debug
    
  //    for(redNode* neb : *(Leaf->AdjList())){
//	std::cout<< neb->label<<" ";
      //}
      //std::cout<<std::endl;
      
      // Add Third AdjList
      for(std::set<redNode*>::iterator it=Leaf->ThiAdjList()->begin(); it!=Leaf->ThiAdjList()->end();++it){
	Leaf->AdjList()->insert(*it);
      }
      Leaf->ThiAdjList()->clear();

      //      std::cout<<"Node "<<i<<"'s third neighbor: ";

      //print new AdjList--Debug
      
    //  for(redNode* neb : *(Leaf->AdjList())){
//	std::cout<< neb->label<<" ";
  //    }
    //  std::cout<<std::endl;
      
    }


    
 
    // Pointer to the redNode of interest, and the left and right children of its black child
    redNode* myRedNode;
    redNode* leftRedChild;
    redNode* rightRedChild;

    // The adjacency set of my (grand child)
    std::set<redNode*> *childAdjSet;

    // Now, from bottom to top we create the adjacency list of all levels redNodes
    for ( int i = maxLevel()-1; i >= 0; i-- )
      {
	for ( unsigned int j = 0; j < redNodeList_[i].size(); j++)
	  {
	    myRedNode = redNodeList_[i][j];
	    leftRedChild = myRedNode -> child() -> leftChild();
	    rightRedChild = myRedNode -> child() -> rightChild();
	    
	    // Go through (grand) parent of adjacent nodes of my (grand) children, and add them to my adjacent list

	    // Left (grand) child
	    childAdjSet = leftRedChild -> AdjList();
	    for ( std::set<redNode*>::iterator it = childAdjSet -> begin(); it != childAdjSet -> end(); ++it )
	      {
		myRedNode -> AdjList() -> insert( ( (*it) -> parent() ) -> parent() );
	      }

	    // Right (grand) child
	    childAdjSet = rightRedChild -> AdjList();
	    for ( std::set<redNode*>::iterator it = childAdjSet->begin(); it != childAdjSet->end(); ++it )
	      {
		myRedNode -> AdjList() -> insert( ( (*it) -> parent() ) -> parent() );
	      }
	  }
      }
}

// Create the edges between leaves of the tree
void tree::createLeafEdges()
{
  // The set of adjacent nodes to a leaf
  std::set<redNode*> *leafAdjSet;
  int total_edge=0;
  // go through all leaves
  for ( unsigned int i = 0; i < redNodeList_.back().size(); i++ )
    {
      // pointer to a leaf
      redNode* leaf = redNodeList_.back()[i];
      // pointer to its adjacency list
      leafAdjSet = leaf -> AdjList();
      // location of columns corresponding to this leaf
      int colFirst = leaf->IndexFirst();
      int colWidth = leaf->IndexLast() - colFirst + 1;
      total_edge+=leafAdjSet->size();
      // Go through adjacency list of each node
      for ( std::set<redNode*>::iterator it = leafAdjSet->begin(); it != leafAdjSet->end(); ++it )
	{
	  // location of rows corresponding to the adjacent leaf
	  int rowFirst = (*it)->IndexFirst();
	  int rowWidth = (*it)->IndexLast() - rowFirst + 1;

	  // Creating the edge and add it to in/out edges list of two leaves
	  leaf->outEdges.push_back(new edge(leaf, *it));
	  (*it)->inEdges.push_back(leaf->outEdges.back());
	  
	  // Create the interaction matrix and put that on the edge
	  leaf -> outEdges.back() -> matrix = new densMat( matrix_ -> block(rowFirst, colFirst, rowWidth, colWidth) ); 	 
	}
	
    }
  std::cout<<"Total # of edges on leaf level: "<<total_edge<<std::endl;
  
}

// Create superNodes at level [l] of the tree
double tree::createSuperNodes( unsigned int l )
{
  //std:: cout << " list of level " << l << " superNodes:" << std::endl;
  // Go through all redNodes at level [l-1], then call the mergeChildren function for their black child
  double time_elapsed = 0;
  for ( unsigned int i = 0; i < redNodeList_[l-1].size(); i++ )
    {
      time_elapsed += redNodeList_[l-1][i] -> child() -> mergeChildren();
      //std::cout << " superNode = " << redNodeList_[l-1][i] -> child() -> superChild() << "  , blackParent = " << redNodeList_[l-1][i] -> child() << "  , redParent = " << redNodeList_[l-1][i] << std::endl; 
    }
  return time_elapsed;
  //std::cout<<std::endl;
}

// Eliminate nodes at level [l] of the tree
timeTuple4 tree::eliminate( unsigned int l )
{
  timeTuple4 times;
  times[0] = 0;
  times[1] = 0;
  times[2] = 0;
  times[3] = 0;

  timeTuple2 comp_time;
  timeTuple2 sc_time;


  int percentage=10;
  // std::cout << "Start eliminating level " << l << std::endl;
  // Go through all superNodes at level [l], compress and apply schurComp.
  //std::cout<<"deal with level "<< l<<" with "<<superNodeList_[l].size()<<" supernodes"<<std::endl;
  std::cout<<"Factorization level "<<l<<" :"<<std::endl;

  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if(i*1.0/superNodeList_[l].size()*100>percentage){
	//	std::cout<<percentage<<"% "<<std::endl;
	percentage+=10;
      }

      //std::cout << "  Start eliminating node " << i <<"/"<<superNodeList_[l].size()<< std::endl;
      
      if(param_->constant()==2){
	//std::cout<<"addvec ";
	comp_time = superNodeList_[l][i] -> compress_addvec_only(1);
      }
      else if(param_->constant()==1){
	//	std::cout<<"addvec+SVD ";
	comp_time = superNodeList_[l][i] -> compress_addvec(1);
      }
      else{
	//		std::cout<<"SVD ";
	comp_time = superNodeList_[l][i] -> compress(1);	
      }
      //           comp_time = superNodeList_[l][i] -> compress((i==0));
      times[0] += comp_time[0];
      times[1] += comp_time[1];

      //std::cout << "    Compression done! "<< std::endl;
      sc_time = superNodeList_[l][i] -> schurComp();
      times[2] += sc_time[0];
      times[3] += sc_time[1];
      //std::cout << "    Schur Comp. done! "<< std::endl;
    }
   std::cout<<std::endl;
  return times;
}
                                                                                                                                                                                                                                                           
// solveU blackNodes and superNodes at level[l], (superNodes solution split to redNodes solution)
void tree::solveU( unsigned int l )
{
  for ( unsigned int i = superNodeList_[l].size() ; i > 0; i-- )
    {
      // Solve for the blackNode first
      superNodeList_[l][i-1] -> parent() -> solveU();
      superNodeList_[l][i-1] -> solveU();
    }
}

// split VAR from superNode to redNodes
void tree::splitVAR( unsigned int l )
{
  for ( unsigned int i = superNodeList_[l].size(); i > 0; i-- )
    {
      superNodeList_[l][i-1] -> splitVAR();
    }
}

// solveL blackNodes and superNodes at level[l], (first RHS of redNodes merge to create RHS of superNode)
void tree::solveL( unsigned int l )
{
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNodeList_[l][i] -> solveL();
      superNodeList_[l][i] -> parent() -> solveL();
     }
}


// merge RHS from redNodes to superNodes at level l
void tree::mergeRHS( unsigned int l )
{
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNodeList_[l][i] -> parent() -> mergeRHS();
    }
}

// Set leaf level RHS
void tree::setRHS( VectorXd& rhs_ )
{
  
  // first set the RHS of the leaf redNodes
  setLeafRHS( rhs_ );
  
  // now set the RHS of all other red and black nodes zero
  for ( unsigned int l = 0; l < redNodeList_.size()-1; l++)
    {
      for ( unsigned int i = 0; i < redNodeList_[l].size(); i++ )
	{
	  // pointer to a red node
	  redNode* rNode = redNodeList_[l][i];
	  
	  if ( rNode->m() > 0 )
	    {
	      // make RHS of the redNode 0
	      //*( rNode -> RHS() ) = VectorXd::Zero( rNode->m() );
	      rNode -> RHS() -> setZero();
	    }
	  
	  if ( rNode->child()->m() > 0 )
	    {
	      // make RHS of its black child zero
	      //( rNode -> child() -> RHS() ) = VectorXd::Zero( rNode -> child() -> m() );	  
	      rNode -> child() -> RHS() -> setZero();
	    }
	  
	}
    }
}


// Set leaf level RHS
void tree::setLeafRHS( VectorXd& rhs_ )
{
  
  // go through all leaves
  for ( unsigned int i = 0; i < redNodeList_.back().size(); i++ )
    {
      // pointer to a leaf
      redNode* leaf = redNodeList_.back()[i];

      // location of columns/rows corresponding to this leaf
      int colFirst = leaf->IndexFirst();
      int colWidth = leaf->IndexLast() - colFirst + 1;
      
      // Assign the RHS
      *( leaf -> RHS() ) = rhs_.segment( colFirst, colWidth );

    }
}

// set leaf level VAR
void tree::setLeafRHSVAR()
{
  
  // go through all leaves
  for ( unsigned int i = 0; i < redNodeList_.back().size(); i++ )
    {
      // pointer to a leaf
      redNode* leaf = redNodeList_.back()[i];

      // location of columns/rows corresponding to this leaf
      int colFirst = leaf->IndexFirst();
      int colWidth = leaf->IndexLast() - colFirst + 1;

      // Allocate memory for VAR
      leaf -> VAR( new VectorXd( colWidth ) );

      // Allocate memory RHS
      leaf -> RHS( new VectorXd( colWidth ) );

      *( leaf -> RHS() ) = RHS_ -> segment( colFirst, colWidth );
    }
}

// Set leaf level additional vector
void tree::setLeafADDVEC( VectorXd& vec_ )
{
  // go through all leaves
  for ( unsigned int i = 0; i < redNodeList_.back().size(); i++ )
    {
      // pointer to a leaf
      redNode* leaf = redNodeList_.back()[i];

      // location of columns/rows corresponding to this leaf
      int colFirst = leaf->IndexFirst();
      int colWidth = leaf->IndexLast() - colFirst + 1;
      if(leaf ->NumADDVEC_==0){
	leaf ->ADDVEC = new densMat(leaf->n(),1);
	//leaf -> ADDVEC.push_back(new VectorXd( leaf->n() ));      
      // Assign the ADDVEC
      //*( leaf -> ADDVEC.back() ) = vec_.segment( colFirst, colWidth );
      }
      else{
	leaf->ADDVEC->conservativeResize(leaf->n(),leaf ->NumADDVEC_+1);
      }
      leaf -> ADDVEC->col(leaf->NumADDVEC_) = vec_.segment( colFirst, colWidth );
      leaf -> NumADDVEC_+=1;
    }
  additional_vec +=1;
}





// collect the solution of all leaves
VectorXd& tree::computeSolution()
{
  
  // number of filled
  int numFilled = 0;
  
  // Go through all leaves, and concatenate their solutions
  for ( unsigned int i = 0; i < redNodeList_.back().size(); i++ )
    {
      node* Leaf = redNodeList_.back()[i];
      if ( Leaf->n() > 0 ) // i.e., if this leaf has any unknown
	{
	  VAR_ -> segment( numFilled, Leaf->n() ) = *( Leaf->VAR() );
	  numFilled += Leaf->n();
	}
    }
  return *VAR_;
}

// Compute mean, min, and max size of redNodes at each level
void tree::setRanks()
{
  // Go through every level
  double sum_tot = 0;
  double count_tot = 0;
  for ( unsigned int l = 0; l < redNodeList_.size(); l++ )
    {
      double mean = 0;
      int minimum = 1e6;
      int maximum = -1e6;
      // Go through every redNode
      for ( unsigned int i = 0; i < redNodeList_[l].size(); i++ )
	{
	  int rank = redNodeList_[l][i]->n();
	  if (rank > 0)
	    {
	      count_tot ++;
	      sum_tot += rank;
	    }
	  mean += rank;
	  minimum = std::min( minimum, rank );
	  maximum = std::max( maximum, rank );
	}
      meanRanks_.push_back( mean / redNodeList_[l].size() );
      minRanks_.push_back( minimum  );
      maxRanks_.push_back( maximum );
      //std::cout<<" At level = "<<l<<" mean Rank = "<<mean / redNodeList_[l].size()<<"\n";
      //std::cout<<" At level = "<<l<<" max Rank = "<<maximum<<"\n\n";
      //std::cout<<"Average rank = "<<sum_tot/count_tot<<"\n\n";
    }
}

// log informations:
// matrix_size nnz nnz_RHS epsilon epsilonR method methodR AssembleTime H2_solve_time SparseLU_sole_time accuracy depth meanRanks[] minRanks[] maxRanks[]
void tree::log( std::string fileName )
{
  std::ofstream output;
  output.open( fileName.c_str(), std::fstream::out|std::fstream::app );

  std::ofstream output2;
  output2.open( "rlog.txt", std::fstream::out|std::fstream::app );

  setRanks();

  output<<"matrixSize= "<<n_<<"\n";
  output<<"nnzMatrix= "<<nnz<<"\n";
  output<<"Padding happened? "<<padding<<"\n";
  output<<"LargePivot happened? "<<largePivot<<"\n";
  output<<"low-rank method= "<<param_->lowRankMethod()<<"\n";
  output<<"epsilon= "<<param_->epsilon()<<"\n";
  output<<"cut-off method= "<<param_->cutOffMethod()<<"\n";
  output<<"a priori rank= "<<param_->aPrioriRank()<<"\n";
  output<<"Rank cap factor= "<<param_->rankCapFactor()<<"\n";
  output<<"rSVD deploy factor= "<<param_->deployFactor()<<"\n";
  output<<"assembleTime= "<<assembleTime<<"\n";
  output<<"PrecondFactTime= "<<precondFactTime<<"\n";
  output<<"GMRES totalTime= "<<gmresTotalTime<<"\n";
  output<<"treeDepth= "<<param_->treeLevelThreshold()<<"\n";  
  output<<"GMRES epsilon = "<<param_->gmresEpsilon()<<"\n";
  output<<"GMRES Preconditinoer = "<<param_->gmresPC()<<"\n";
  output<<"ILU DropTol = "<<param_->ILUDropTol()<<"\n";
  output<<"ILU Fill = "<<param_->ILUFill()<<"\n";
  output<<"Smoothing = "<<param_->Ns()<<"\n";
  output<<"GMRES final (actual) residual = "<<residual<<"\n";
  output<<"GMRES final accuracy = "<<accuracy<<"\n";
  output<<"GMRES maxIters = "<<param_->gmresMaxIters()<<"\n";
  output<<"GMRES totalIters= "<<gmresTotalIters<<"\n";
  output<<"\n\n";

  output.close();

  // Store the data-only version
  output2<<n_<<" ";
  output2<<nnz<<" ";
  output2<<padding<<" ";
  output2<<largePivot<<" ";
  output2<<param_->lowRankMethod()<<" ";
  output2<<param_->epsilon()<<" ";
  output2<<param_->cutOffMethod()<<" ";
  output2<<param_->aPrioriRank()<<" ";
  output2<<param_->rankCapFactor()<<" ";
  output2<<param_->deployFactor()<<" ";
  output2<<assembleTime<<" ";
  output2<<precondFactTime<<" ";
  output2<<gmresTotalTime<<" ";
  output2<<param_->treeLevelThreshold()<<" ";
  output2<<param_->gmresEpsilon()<<" ";
  output2<<param_->gmresPC()<<" ";
  output2<<param_->ILUDropTol()<<" ";
  output2<<param_->ILUFill()<<" ";
  output2<<param_->Ns()<<" ";
  output2<<residual<<" ";
  output2<<accuracy<<" ";
  output2<<param_->gmresMaxIters()<<" ";
  output2<<gmresTotalIters<<" ";
  for ( int i = 0; i < gmresTotalIters; i++ )
    {
      output2<<(*residuals)(i)<<" ";
    }
  output2<<"\n";
  output2.close();
    
}

void tree::computeFrobNorm( unsigned int l )
{
  int num_dof=0;
  std::ofstream nodesize_file;
  if(l==maxLevel()){
    nodesize_file.open("nodesize.txt",std::ofstream::out);
    nodesize_file<<superNodeList_[l].size()<<std::endl;

  }

  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if(l==maxLevel()){
	nodesize_file<<superNodeList_[l][i]->n()<<std::endl;
	  }
      num_dof+=superNodeList_[l][i]->n();
      for ( unsigned int j = 0; j < superNodeList_[l][i]->outEdges.size(); j++)
	{
	  //	  std::cout<< superNodeList_[l][i]->outEdges[j]->matrix->norm()<<" ";
	  if(superNodeList_[l][i]->outEdges[j]->matrix==NULL){
	    std::cout<<"Empty matrix encountered!"<<std::endl;
	  }
	  frobNorms[l] += std::pow( superNodeList_[l][i]->outEdges[j]->matrix->norm(), 2);
	  totalSizes[l] +=  ( superNodeList_[l][i]->outEdges[j]->matrix->rows() * superNodeList_[l][i]->outEdges[j]->matrix->cols() );
	}
    }

  if(l==maxLevel()){
    nodesize_file.close();
  }
  globFrobNorm = std::sqrt( std::pow( globFrobNorm, 2) + frobNorms[l] );
  globSize += totalSizes[l];
  frobNorms[l] = std::sqrt( frobNorms[l] );
  std::cout<<"frobNorm at level "<<l<<" = "<<frobNorms[l]<<" , total size = "<<totalSizes[l]<<"num of dof="<<num_dof<<", average # of unknowns per red node= "<<num_dof*1.0/superNodeList_[l].size()/2<<"\n";
}

//! Top to bottom traverse
/*!
  Start from the top, solve the first set of equations directly (e.g., direct LU),
  Then back-propagate toword leaves, and solve all unknowns.
  This version uses the RHS provided in the input params.
*/
VectorXd& tree::solve()
{
  // solve
  return solve( *RHS_ );
}


// first set the RHS of all super/black nodes, then call solve()
VectorXd& tree::solve( VectorXd &RHS )
{
  
  clock_t start, finish;
  start = clock();
  int GS_smoothing = param_->Ns();

  if(GS_smoothing){
    *x_n = VectorXd::Zero(x_n->size()); 
    for(int i=0;i<GS_smoothing;i++){
      *rhs_ = RHS - (*matrix_)*(*x_n);
      *x_n = *x_n + (*matrix_).transpose().triangularView<Eigen::Lower>().solve(*rhs_);
    }  
    *rhs_ = RHS - (*matrix_)*(*x_n);
  }
  else{
    *rhs_ = RHS;
  }
  

  // CALL SET RHS FOR THE GIVEN RHS 
  setRHS( *rhs_ );
  // std::cout<<"start bottom to top solve\n";
  //! bottom to top traverse to solve L z = b

  for ( unsigned int l = maxLevel(); l > 0; l-- )
    {
      mergeRHS( l );
      //      if (param_->Ns() > 0)
      //	{
      //	    forwardSmoothing( l );
      //	}
      solveL( l );
    }
  // std::cout<<"start top to bottom solve\n";
  //! top to bottom traverse to solve U x = z
  for ( unsigned int l = 1; l <= maxLevel(); l++ )
    {
      solveU( l );
      //if (param_->Ns() > 0)
      //	{
      //	   backwardSmoothing( l );
      //	}
      splitVAR( l );
    }
  finish = clock(); 

  if (GS_smoothing){
    (*x_n) = (*x_n) + computeSolution();
    for(int i=0;i<GS_smoothing;i++){
      *rhs_ = RHS - (*matrix_)*(*x_n);
      *x_n = *x_n + (*matrix_).transpose().triangularView<Eigen::Upper>().solve(*rhs_);    
    }
    return (*x_n);
  }
  else{
    return computeSolution();
  }
  
}


//! Bottom to top traverse
/*!
  At each level, first create the superNodes (i.e., combine two redNodes), and then eliminate (and compress) all superNodes, as well as their black node parents.
*/
void tree::factorize()
{
  timeTuple4 eliminate_times;
  double merge_time;
  for ( unsigned int l = maxLevel(); l > 0; l-- )
    {
      merge_time = createSuperNodes( l );
      computeFrobNorm( l );
      //copyEdges( l );
      setSmoothingVAR( l );
      eliminate_times = eliminate( l );
            std::cout<<"***************************************************************************************************\n";
       std::cout<<"    Merge: "<<merge_time<<"   Cmp(SVD): "<<eliminate_times[0]<<"   Comp(etc.): "<<eliminate_times[1]<<"   S.C.(inv): "<<eliminate_times[2]<<"   S.C.(etc.): "<<eliminate_times[3]<<std::endl;
       std::cout<<"***************************************************************************************************\n\n";
    }
}

VectorXd* tree::retVal()
{
  computeSolution();
  return VAR_;
}

void tree::normalize_cols( spMat* A)
{
  // each colmn
  for ( int i = 0; i < A -> outerSize(); i++ )
    {
      
      // find max of the column
      double max_entry = -1e30;
      for ( int j = A->outerIndexPtr()[i]; j < A->outerIndexPtr()[i+1]; j++ )
	{
	  if ( A -> valuePtr()[j] > max_entry )
	    {
	      max_entry = A -> valuePtr()[j];
	    }
	}
      max_entry = 1./max_entry;
      for ( int j = A->outerIndexPtr()[i]; j < A->outerIndexPtr()[i+1]; j++ )
	{
	  A -> valuePtr()[j] *= max_entry;
	}
    }  
}

void tree::copyEdges( unsigned int l )
{
  // Go through all superNodes and copy their edges to intact lists
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNodeList_[l][i] -> copyEdges();
      //superNodeList_[l][i] -> copyILUEdges();
    }
}


void tree::setSmoothingVAR( unsigned int l )
{
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNode *S = superNodeList_[l][i];
      S -> VAR_s_ = new VectorXd( S -> n() ) ;
      S -> RHS_s_ = new VectorXd( S -> n() ) ;
    }
}



void tree::solveU_GS( unsigned int l )
{

  for ( unsigned int i = superNodeList_[l].size() ; i > 0; i-- )
    {
      superNodeList_[l][i-1] -> solveU_GS();
    }
}

void tree::solveL_GS( unsigned int l )
{

  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNodeList_[l][i] -> solveL_GS();
    }
}


// Smoothing before solveL
void tree::forwardSmoothing( unsigned int l )
{
  
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if ( superNodeList_[l][i] -> n() > 0 )
	{
	  superNodeList_[l][i] -> VAR_s_ -> setZero();
	}
    }
  
  // Apply ILU smoother
  //ILUsmoother( l );
  //  GSforward_smoother(l);
  
  //rhs_s = A_l * var_s
  multiply(l);
  
  // b -= rhs_s
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
       if ( superNodeList_[l][i] -> n() > 0 )
	 {
	   *( superNodeList_[l][i] -> RHS() ) -= *( superNodeList_[l][i] -> RHS_s_ );
	 }
    } 
  
}

// Smoothing before solveL
void tree::backwardSmoothing( unsigned int l )
{
  //std::cout<<"level = "<<l<<"  size="<<superNodeList_[l].size()<<"\n";
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if ( superNodeList_[l][i] -> n() > 0 )
	{
	  // VAR += VAR_s
	  *( superNodeList_[l][i] -> VAR() ) += *( superNodeList_[l][i] -> VAR_s_ );
	  
	  // b += rhs_s
	  *( superNodeList_[l][i] -> RHS() ) += *( superNodeList_[l][i] -> RHS_s_ );
	  
	  // VAR_s = VAR
	  *( superNodeList_[l][i] -> VAR_s_ ) = *( superNodeList_[l][i] -> VAR() );
	}
    }

  // Apply ILU smoother
  //ILUsmoother( l );
  // GSforward_smoother(l);

  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if ( superNodeList_[l][i] -> n() > 0 )
	{
	  // VAR = VAR_s
	  *( superNodeList_[l][i] -> VAR() ) = *( superNodeList_[l][i] -> VAR_s_ );
	}
    }

  // print residual:
  /*
  //rhs_s = A_l * var_s
  multiply(l);
  double total_level_residual = 0;
  double total_level_var = 0;
  // b -= rhs_s
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
       if ( superNodeList_[l][i] -> n() > 0 )
	 {
	   *( superNodeList_[l][i] -> RHS() ) -= *( superNodeList_[l][i] -> RHS_s_ );
	   total_level_residual += std::pow( superNodeList_[l][i] -> RHS() -> norm(), 2 );
	   total_level_var += std::pow( superNodeList_[l][i] -> VAR() -> norm(), 2 );
	 }
    } 
  std::cout<<"norm of residual at level "<<l<<" after ILU = "<<std::sqrt( total_level_residual )<<" and norm of var = "<<std::sqrt( total_level_var )<< "\n";
  */
}




void tree::GSforward_smoother( unsigned int l )
{
  // Number of smoothing (applying ILU)                                                                                                          
  int Ns = param_->Ns();
  /*                                                                                                                                             
  if ( l < param_->treeLevelThreshold() )                                                                                                        
    {                                                                                                                                            
      Ns = 0;                                                                                                                                    
    }                                                                                                                                            
  */

  for ( int is = 0; is < Ns; is++ )
    {
      //rhs_s = A_l * var_s                                                                                                                      
      multiply(l);

      for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
        {
          if ( superNodeList_[l][i] -> n() > 0 )
            {
              // rhs_s = b - rhs_s                                                                                                               
              *( superNodeList_[l][i] -> RHS_s_ ) = *( superNodeList_[l][i] -> RHS() ) -  *( superNodeList_[l][i] -> RHS_s_ );
            }
        }

      // rhs_s = inv(L') * rhs_s                                                                                                                 
      solveL_GS(l);

      // VAR = inv(U') * rhs_s                                                                                                                   
      //      solveIU(l);

      //VAR_s += VAR                                                                                                                             
      for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
        {
          if ( superNodeList_[l][i] -> n() > 0 )
            {
              *( superNodeList_[l][i] -> VAR_s_ ) += *( superNodeList_[l][i] -> VAR() );
            }
        }
    }
}

// RHS_s = A_l * VAR_s                                                                                                                           
void tree::multiply( unsigned int l )
{
  // First set all rhs_s to zero                                                                                                                 
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      if ( superNodeList_[l][i] -> n() > 0 )
        {
          superNodeList_[l][i] -> RHS_s_ -> setZero();
        }
    }

  // Now apply multiplication                                                                                                                    
  for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
    {
      superNodeList_[l][i] -> multiply();
    }
}





void tree::GSbackward_smoother( unsigned int l )
{
  // Number of smoothing (applying ILU)                                                                                                             
  int Ns = param_->Ns();
  /*                                                                                                                                                
  if ( l < param_->treeLevelThreshold() )                                                                                                           
    {                                                                                                                                               
      Ns = 0;                                                                                                                                       
    }                                                                                                                                               
  */

  for ( int is = 0; is < Ns; is++ )
    {
      //rhs_s = A_l * var_s                                                                                                                         
      multiply(l);

      for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
        {
          if ( superNodeList_[l][i] -> n() > 0 )
            {
              // rhs_s = b - rhs_s                                                                                                                  
              *( superNodeList_[l][i] -> RHS_s_ ) = *( superNodeList_[l][i] -> RHS() ) -  *( superNodeList_[l][i] -> RHS_s_ );
            }
        }

      // rhs_s = inv(L') * rhs_s                                                                                                                    
      solveU_GS(l);

      // VAR = inv(U') * rhs_s                                                                                                                      
      //      solveIU(l);                                                                                                                           

      //VAR_s += VAR                                                                                                                                
      for ( unsigned int i = 0; i < superNodeList_[l].size(); i++ )
        {
          if ( superNodeList_[l][i] -> n() > 0 )
            {
              *( superNodeList_[l][i] -> VAR_s_ ) += *( superNodeList_[l][i] -> VAR() );
            }
        }
    }
}





