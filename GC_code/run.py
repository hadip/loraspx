import os,re

def writeInputFile(probSize,treeDepth, dimension=2, epsilon=0.1, case = 8, level = -1):
    fid = open('params.in','r')
    wid = open('py_params.in','w')
    skip = False
    for line in fid:
        if line[0]=='\n':
            wid.write(line)
        else:
            if skip==False:
                wid.write(line)
            else:
                skip = False
            
        if line[0]=='#':
            if re.search('# size of the Poisson problem ',line):
                wid.write(str(probSize)+'\n')
                skip = True
            elif re.search('#The maximum level of the tree',line):
                wid.write(str(treeDepth)+'\n')
                skip = True
            elif re.search('#Compression epsilon', line):
                wid.write(str(epsilon)+'\n')
                skip = True


def filterOutput():
    fid = open('py_output.txt','r')
    for line in fid:
        if len(line)>18 and line[0:18]=='Factorization TIME':
            fac_time = re.findall('^Factorization TIME  = ([0-9.]+)',line)
        if len(line)>10 and line[0:10]=='GMRES TIME':
            gmres_time = re.findall('^GMRES TIME  = ([0-9.e-]+)',line)
        if len(line)>9 and line[0:9]=='GMRES NUM':
            gmres_num = re.findall('^GMRES NUM ITERS  = ([0-9]+)',line)
    return [fac_time[0],gmres_time[0],gmres_num[0]]
            

dimension = 2
baseSize = 5
baseDepth = 7
baseEpsilon = 0.3
numProblem = 6
#probSize = baseSize
#writeInputFile(probSize = baseSize,treeDepth = baseDepth)
for increment in range(numProblem):
    probSize = (2**(baseSize+increment))
    treeDepth = baseDepth + increment*dimension
    epsilon = baseEpsilon #8*(1./probSize)
    output = 'problem: '+str(probSize)+'\t tree depth: '+str(treeDepth)+'\t epsilon: '+str(epsilon)+'\t results: '
    writeInputFile(probSize = probSize,treeDepth = treeDepth,epsilon = epsilon)
    os.system("./LoRaSp py_params.in > py_output.txt")
    print output+'\t'.join(filterOutput())

