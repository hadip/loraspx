#ifndef poisson_h
#define poisson_h
#include "Eigen/Sparse"
#include "Eigen/Dense"
#include <map>
#include <string>
#include<array>

//! TRIPLET Type
/*! Define a triplet type to store entries of a matrix */
typedef Eigen::Triplet<double> TRIPLET;

/*! Define a triplet type to store symbolic matrix */
typedef Eigen::Triplet<bool> TRIPLETBOOL;

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! Define a sparse matrix (column major) with boolean entries */
typedef Eigen::SparseMatrix<bool> spMatBool;

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! A dense vector class used for permutation */
typedef Eigen::Matrix<int, Eigen::Dynamic, 1> VectorXi;


typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> MatrixXd;

class Poisson
{
  //number of unknown in each direction
  int N_;

  //number of dimension
  int dimension_;
  
  //number of total unknowns
  int size_;

  //number of nonzeros
  int nnz_;
  
  spMat* matrix_;
  spMatBool* symb_matrix_;
  
  VectorXd*rhs_;

  //! the eigenvector computed
  VectorXd *eigenVector_;

  //! the coordinate vector
  VectorXd *coordinates_;


 public:
  //! Default constructor
  Poisson(){};

  //! Constructor specifiying the division in each direction
  Poisson(int,int);

  //! Destructor
  ~Poisson(){};
  
  //to do:
  //function to generate the i-th eigenvalue and eigenvectors

  //return the i-th eigenvalue
  double eigenValue(int i);

  //return the i-th eigenvalue                                                                                         
  double min_eigenValue(int i);

  //return the i-th eigenvector
  VectorXd *eigenVector(int i);

  //return the coordinates of i-th unknown
  VectorXd *coordinates(int i);

  //return the partition
  void partition(VectorXi *group);

  //return the i-th eigenvector                                                                                        
  VectorXd *min_eigenVector(int i);

  //return linear function x
  VectorXd *linear_x();

  //return linear function y
  VectorXd *linear_y();

  //! return the matrix
  spMat* Matrix() const{return matrix_;}

  //! returnt the symbo matrix
  spMatBool* symbMatrix() const{return symb_matrix_;}

  VectorXd* RHS() const{return rhs_;}

  int N(){return N_;}

  int rows(){return size_;}
  
};
#endif
