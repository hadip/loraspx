#ifndef node_h
#define node_h

class tree;
class redNode;
class blackNode;
class superNode;
class edge;

#include <vector>
#include "Eigen/Dense"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a dynamic size dense matrix used to store the inverse of pivot */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/**************************************************************************/
/*                           CLASS NODE (Abstract)                        */
/**************************************************************************/

//! Class node
/*!
  This (virtual) class is an interface for the three inherited classes
  blackNode, redNode, and superNode
*/

class node
{

  //! Pointer to the tree that this node belongs to
  tree* tree_ptr_;

  //! Pointer to the parent node
  node* parent_;

  //! Index of the first column in the matrix (inclusive) correspondig to this node
  int index_first_;

  //! Index of the last column in the matrix (inclusive) correspondig to this node
  int index_last_;

  //! Indicate number of variables in this node
  /*!
    Note that for a leaf redNode [n] is the number of columns of the matrix corresponding to this node.
    However, for redNodes upper in the tree, [n] is determined by low rank approximation.
   */
  int n_;

  //! Indicate number of equations in this node
  /*!
    The number of euqations [m], is also equal to the number of corresponding rows for leaf redNodes.
    But for other redNodes, it depends on the rank in our low-rank decomposition.
   */
  int m_;


  // Type of the node
  // 0: unspecified
  // 1: red node
  // 2: black node
  // 3: super node
  int type_;

  //! The right hand side vector
  VectorXd *RHS_;

  //! The vector of unknowns
  VectorXd *VAR_;


 public:

  //!  the number of additional vector to preserve for this node in svd
  int NumADDVEC_;

 //! The vector of addtional vec
  //std::vector<VectorXd *>ADDVEC;
  densMat *ADDVEC;

  //! Default constructor
  node(){};

  //! Constructor:
  /*!
    input arguments:
    pointer to parent, pointer to the tree, range of belonging rows/cols
  */
  //! Constructor by passing the ptr to the tree, and range of belonging columns
  node( node*, tree*, int, int );

  //! Destructor
  virtual ~node();

  //! Returns parent_
  node* parent() const { return parent_; }
   
  //! Returns index_first_
  int IndexFirst() const { return index_first_; }

  //! Returns index_last_
  int IndexLast() const { return index_last_; }

  //! Returns type
  int type() const{ return type_;}

  //! Set type
  void type(int val) {type_ = val;}
  
  //! Returns tree_ptr_
  tree* Tree() const { return tree_ptr_; }

  //! True if this node contains no column of the matrix
  bool isEmpty() { return index_first_>index_last_; }

  //! True if this node is eliminated
  bool isEliminated() const { return eliminated_; }

  //! Set the elimination flag to [true]
  void eliminate();
  
  //! Set the elimination flag to [false]
  void deEliminate();

  //! Returns m_
  int m() const { return m_; }

  //! Returns n_
  int n() const { return n_; }

  //! Set m_
  void m( int val ) { m_ = val; }

  //! Set n_
  void n( int val ) { n_ = val; }

    //! True if it has additional vector
  bool HasADDVEC() const{return (NumADDVEC_>0); }
  //! Returns pointer to the redParent (pure virtual)
  /*!
    redNode -> returns itself
    blackNode -> returns parent()
    superNode -> returns parent() of paranet()
   */
  virtual redNode* redParent()=0;
  
  //! Overload: int is the lvel of grand parent
  virtual redNode* redParent(int)=0;

  //! level
  virtual unsigned int level()=0;

  //! List of incoming edges
  std::vector<edge*> inEdges;
  //! a copy of inEdges before elimination
  //std::vector<edge*> inEdgesIntact;
  //! List of incoming edges for ILU
  //std::vector<edge*> inEdgesILU;

  //! List of outgoing edges
  std::vector<edge*> outEdges;
  //! a copy of outEdges before elimination
  //std::vector<edge*> outEdgesIntact;
  //! List of outgoing edges for ILU
  //std::vector<edge*> outEdgesILU;

  //! Copy all edges to intact lists
  void copyEdges();

  //! Copy to ILU edges
  //  void copyILUEdges();

  //! A boolean flag to keep track of elimination
  bool eliminated_;

  //! Erase removed edges from the list of incoming/outgoing edges
  void eraseCompressedEdges();

  //! Erase removed edges from the list of incoming/outgoing edgesILU
  //  void eraseCompressedEdgesILU();

  //! Access to the pointer of the RHS vector
  VectorXd* RHS() { return RHS_; }

  //! Access to the additonal vector 
  //std::vector<VectorXd*> *ADDVEC(){return &ADDVEC_;}

    //! Set the pointer of the RHS vector
  void RHS( VectorXd* in ) { RHS_ = in; }

  //! Set the pointer of the ADDVEC vector
  //  void ADDVEC(VectorXd* in) {ADDVEC_ = in;}

  //! Access to the pointer of the variables vector
  VectorXd* VAR() { return VAR_; }

  //! Set the pointer of the VAR vector
  void VAR( VectorXd* in ) { VAR_ = in; }

  //! LU decomposition of the pivot
    Eigen::PartialPivLU<densMat> *luPivot;
    //  Eigen::FullPivLU<densMat> *luPivot;

  //! The inevrse of the selfEdgeILU matrix (i.e., pivot)
    //  densMat *invPivotILU;

  //! solve L z = b
  /*!
    This function updates RHS, which is solve for z in L z = b
    It uses the order of elimination.
   */
  void solveL();

  //! solve U VAR = RHS
  /*!
    Solve for unknowns of this cluster.
    It uses the order of elimination.
   */
  void solveU();

  //! Multiplication: Apply vars through outgoing edges to rhs_s
  void multiply();
  
  //! A boolean flag to keep track of updated RHS
  bool rhsUpdated_;

  //! The order of elimination
  int order;

  //! The vector of unknowns for smoothing
  VectorXd *VAR_s_;

  //! The right hand side vector for smoothing
  VectorXd *RHS_s_;

  //! Incomplete version of solveL and solveU used for ILU smoother
  //  void solveIL();
  //void solveIU();
  
  // L and U solve using the original matrix-- for the GS
  void solveL_GS();
  void solveU_GS();
}; 

#endif
