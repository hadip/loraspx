#include "precondMat.h"
#include "tree.h"
#include "Eigen/Core"
#include "Eigen/Sparse"

precondMat::precondMat(tree* Tree_input):myoperator(Tree_input->n()){

  Tree = Tree_input;
  //n_ = Tree->n();
}

precondMat::~precondMat(){


}

void precondMat::MultMv(double*v,double*w)
{
    //consistency check
  assert(v!=NULL&&w!=NULL);

  VectorXd temp = VectorXd::Zero(n_);

  for(int i=0;i<n_;i++){
    temp(i) = v[i];
  }
  
  
  temp = (*(Tree->Matrix()))*(temp);
  temp = Tree->solve(temp);
  for(int i=0;i<n_;i++){
    w[i] = temp(i);
  }
}
