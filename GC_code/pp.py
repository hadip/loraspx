from __future__ import unicode_literals
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.lines as mlines

def readLogData( filename ):
    '''
    We store a list of dict.
    Each dict corresponds to one run.
    Keys of the dict are different parameters for that particular run (e.g., n, solvetime, rank, etc. )
    '''
    file = open( filename, 'r' )
    runs = file.readlines()
    results = [];
    for i in xrange( len(runs) ):
        res = {}
        data = runs[i].split();
        res[ 'n' ] = int(data[0]) # size of the matrix
        res[ 'nnz' ] = int(data[1]) # number of non-zeros in the matrix
        res[ 'padding' ] = int(data[2]) # show if any padding has happned
        res[ 'largePivot' ] = int(data[3]) # show if any large (>1e9) pivot has been found
        res[ 'lowRankMethod' ] = int(data[4]) # the method used for low-rank approximatoin in compression: 0->SVD 1->rSVD
        res[ 'epsilon' ] = float(data[5]) # epsilon used in low-rank cutOff for compression
        res[ 'cutOffMethod' ] = int(data[6]) # cutOff method for compression
        res[ 'aPrioriRank' ] = int(data[7]) # a priori rank (used for rSVD, and as the leaf nodes rank for geometric cap) for compression
        res[ 'rankCapFactor' ] = float(data[8]) # the growth factor of the geometric serie used for rank cap in compression
        res[ 'deployFactor' ] = float(data[9]) # the fraction of singular values resulted by rSVD that should be considered for compression
        res[ 'assembleTime' ] = float(data[10]) # time to create the tree
        res[ 'factorizationTime' ] = float(data[11]) # preconditioner factorization time
        res[ 'gmresTotalTime' ] = float(data[12]) # gmres time
        res[ 'totalSVDTime' ] = float(data[13]) # total SVD time
        res[ 'totalGEMMTime' ] = float(data[14]) # total GEMM time
        res[ 'totalINVTime' ] = float(data[15]) # total INV time
        res[ 'totalRESTTime' ] = float(data[16]) # total REST time
        res[ 'levels' ] = int(data[17]) # number of levels
        ranks = []
        Nl = int(data[17]) + 1
        for i in xrange( Nl ):
            ranks.append( float( data[18+i] ) )
        res[ 'ranks' ] = ranks
        res[ 'gmresEpsilon' ] = float(data[18+Nl]) # gmres convergence epsilon
        res[ 'gmresPC' ] = int(data[19+Nl]) # type of preconditioner
        res[ 'ILUDropTol' ] = float(data[20+Nl]) # DropTol used for ILU
        res[ 'ILUFill' ] = int(data[21+Nl]) # Fill used for ILU
        res[ 'residual' ] = float(data[22+Nl]) # Final residual
        res[ 'accuracy' ] = float(data[23+Nl]) # Final accuracy
        res[ 'gmresMaxIters' ] = int(data[24+Nl]) # maximum number of iters in gmres
        res[ 'gmresTotalIters' ] = int(data[25+Nl]) # total  number of iters in gmres
        Nt = int(data[25+Nl])
        residuals = []
        for i in xrange( Nt ):
            residuals.append( float(data[26+Nl+i]) )
        res[ 'residuals' ] = residuals
        results.append( res )
    return results

if __name__ == '__main__':
    results = readLogData( '../GMRES/rlog.txt' )
    #results = readLogData( '../GMRES/rlog_VCP.txt' )
    #results = readLogData( '../GMRES/rlog_conv_voro32.txt' )
    #results = readLogData( '../GMRES/rlog_conv_struct32.txt' )
    #results = readLogData( '../GMRES/rlog_varyN_eps1e1_GMRES.txt' )
    #results = readLogData( '../GMRES/paper_data/rlog_stiffness330k_some.txt' )
    #results = readLogData( '../GMRES/paper_data/rlog_stiffness330k.txt' )
    N = len( results )

    #########################################
    ################# PLOT ##################
    #########################################
    #mpl.rcParams['text.usetex'] = True
    #mpl.rcParams['text.latex.unicode'] = True

    ns = np.array( [ float(run['n']) for run in results ] )
    print ns
    epss = [ run['epsilon'] for run in results ]
    #print epss
    accs = [ run['accuracy'] for run in results ]
    print accs
    ress = [ run['residual'] for run in results ]
   # print ress
    
    SVDtimes = np.array([ run['totalSVDTime'] for run in results ])
   # print SVDtimes
    GEMMtimes = np.array([ run['totalGEMMTime'] for run in results ])
   # print GEMMtimes
    INVtimes = np.array([ run['totalINVTime'] for run in results ])
   # print INVtimes
    
    factTimes = [ run['factorizationTime'] for run in results ] 
    print factTimes
    solveTimes = [ run['gmresTotalTime'] for run in results ]
    print solveTimes
    print np.array(factTimes) + np.array(solveTimes)

    numIters = [ run['gmresTotalIters'] for run in results ]
    print numIters

    color = ['og', 'ob', 'om', 'or', 'oy', 'oc', '*g', '*b', '*m', '*r', '*y', '*c']
    
    
    
    ''' time componenets
    fig = plt.figure(1)
    font = {'family' : 'normal', 'size'   : 20}
    plt.rc('font', **font)
    #plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    width = .5
    ind = np.arange( len(epss) )+1
    p1 = plt.bar(ind, SVDtimes,  width, color='m')
    p2 = plt.bar(ind, GEMMtimes, width, color='c', bottom=SVDtimes)
    p3 = plt.bar(ind, INVtimes, width, color='b', bottom=SVDtimes+GEMMtimes)
    plt.ylabel('time (sec)', fontsize=20)
    plt.xlabel('$10^{4}\epsilon$', fontsize=20)
    plt.xticks(ind+width/2., ('4096', '2048', '1024', '512', '256', '128', '64', '32', '16', '8', '4','2','1'))
    plt.legend( (p1[0], p2[0],p3[0]), ('svd', 'gemm', 'inversion') , frameon=False, loc=2)
    plt.legend(frameon=False)
    fig.set_size_inches(14, 8)
    plt.savefig('time_components.pdf', format='pdf', dpi=400)
    '''

    ''' ranks
    color = ['oc', 'vb', '^m', '<r', '>c' , 'sb', '*y', 'pg', 'dr', '+m','xg','+y']
    fig = plt.figure(1)    
    for i, run in enumerate(results):
        print run['ranks']
        y = np.array( run['ranks'][2:-1] )
        x = np.ones_like(y) * epss[i]
        for j in xrange( x.shape[0] ):
            plt.plot( x[j], y[j], color[j], markersize = 14)
    font = {'family' : 'normal',
            'size'   : 20}
    plt.rc('font', **font)
    #plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    plt.xlim([5e-5,1])
    plt.xscale('log')
    plt.xlabel('$\epsilon$', fontsize=20)
    plt.ylabel('rank', fontsize=20)
    plt.legend(['level = '+str(num) for num in xrange(3,12)] ,fontsize=20, frameon=False, loc = 2)
    plt.legend(frameon=False)
    plt.gca().invert_xaxis()
    fig.set_size_inches(12, 8)
    plt.savefig('ranks.pdf', format='pdf', dpi=400)
    fig.show()
    '''

    #residuals_stiffness
    color = [':k', ':m', '--k', '--m', '--b', '--r', '-k','-m','-b','-r','-c','-y']
    color = [':k', ':m', '--r', '-b']
    fig = plt.figure(1)
    for i, run in enumerate(results):
        y = np.array( run['residuals'] )
        x = xrange(1,y.shape[0]+1)
        plt.plot( x, y, color[i],linewidth = 4)
    
    font = {'family' : 'normal',
            'size'   : 18}
    plt.rc('font', **font)
    plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    
    plt.yscale('log')
    plt.xlabel('iteration number', fontsize=18)
    plt.ylabel('residual', fontsize=18)
    #plt.legend(['no precond.', 'diagonal' , 'ILU 1', 'ILU 2', 'ILU 3', 'ILU 4', '$\mathcal{H}$-tree $\epsilon_1$', '$\mathcal{H}$-tree $\epsilon_2$', '$\mathcal{H}$-tree $\epsilon_3$', '$\mathcal{H}$-tree $\epsilon_4$', '$\mathcal{H}$-tree $\epsilon_5$', '$\mathcal{H}$-tree $\epsilon_6$'] ,fontsize=16, frameon=False, ncol = 2)
    plt.legend(['no precond.', 'diagonal' , 'ILU', '$\mathcal{H}$-tree'] ,fontsize=18, frameon=False, loc = 4)
    plt.legend(frameon=False)
    fig.set_size_inches(12, 8)
    #plt.savefig('residuals_stiffness330k.pdf', format='pdf', dpi=400)
    plt.savefig('residuals_VCP.pdf', format='pdf', dpi=400)
    fig.show()
    
    ''' optimization times
    fig = plt.figure(1)
    width = 0.5
    ind = np.arange( N )
    ftimeLine = plt.bar( ind, factTimes, width, color = 'm')
    stimeLine = plt.bar( ind, solveTimes, width, color = 'b', bottom = factTimes)
    plt.xticks(ind+width/2., ('ILU 2', 'ILU 3', 'ILU 4', '$\mathcal{H}$-tree $\epsilon_3$', '$\mathcal{H}$-tree $\epsilon_4$', '$\mathcal{H}$-tree $\epsilon_5$', '$\mathcal{H}$-tree $\epsilon_6$' ) )
    plt.ylabel('time (sec)', fontsize=16)
    font = {'family' : 'normal',
            'size'   : 16}
    plt.rc('font', **font)
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.legend( ['factorization','GMRES'],fontsize=16, frameon=False )
    plt.legend(frameon=False)
    fig.set_size_inches(12, 8)
    plt.savefig('optimization_stiffness330k.pdf', format='pdf', dpi=400)
    fig.show()
    '''

    ''' compression ratio
    
    fig = plt.figure(1)    
    run = results[-1]
    print run['ranks']
    y = np.array( run['ranks'][2:-1] )
    y2 = .5 * np.array( run['ranks'][2:-1] ) / np.array( run['ranks'][3:] )
    x = xrange(3,12)
    plt.plot( x, y, '-sk', markersize = 10)
    plt.plot( x, y2, '-ob', markersize = 10)
    font = {'family' : 'normal',
            'size'   : 18}
    plt.yscale('log')
    #plt.rc('font', **font)
    #plt.rc('text', usetex=True)
    #plt.rc('font', family='serif')
    plt.xlabel('level', fontsize=18)
    #plt.ylabel('rank', fontsize=18)
    plt.legend(['rank','compression ratio'] ,fontsize=18, frameon=False, loc = 2)
    plt.legend(frameon=False)
    plt.grid()
    plt.gca().invert_xaxis()
    fig.set_size_inches(8, 6)
    plt.savefig('VCP_indef.pdf', format='pdf', dpi=400)
    fig.show()
    '''
    '''
    fig = plt.figure(1)
    run = results[-1]
    y = np.array( run['residuals'] )
    x = xrange(1,y.shape[0]+1)
    plt.plot( x, y, '.k' )
    
    plt.yscale('log')
    plt.xlabel('iteration number', fontsize=18)
    plt.ylabel('residual', fontsize=18)
    plt.legend(frameon=False)
    fig.set_size_inches(8, 6)
    plt.savefig('VCP_indef_residuals.pdf', format='pdf', dpi=400)
    fig.show()
    '''
