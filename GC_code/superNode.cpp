#include "superNode.h"
#include "redNode.h"
#include "tree.h"
#include "edge.h"
#include "Eigen/Dense"
#include "Eigen/LU"
#include <iostream>
#include "params.h"
#include <cassert>
#include "rsvd.h"
#include "time.h"
#include <cmath>

#define DEBUG 0


// Constructor for superNode
superNode::superNode( blackNode* P, tree* T, int first, int last): node( (node*) P, T, first, last )
{
  // set the machine precision zero
  MACHINE_PRECISION = 1e-14;

  // Set its parent
  parent_ = P;
  
  // Set its level
  level_ = P->parent()->level()+1;

  type(3);
  // When a superNode born, it should be added to the superNode list of the tree
  T->addSuperNode(level_,this);

  // We set m, and n based on number of columns/rows corresponding to this redNode.
  // However, for non-leaf redNodes, m and n will be changed after low-rank approximation
  m( last - first + 1 );
  n( last - first + 1 );

  // Initially not eliminated
  eliminated_ = false;

  // Get values for compression and recompression accuracy
  epsilon_ = T->Parameters()->epsilon();

  epsilon_addvec_ = T->Parameters()->epsilon_addvec();
  // Get values for compression and recompression low-rank method
  lowRankMethod_ = T->Parameters()->lowRankMethod();

  // Get the projection approach
  projection_ = T->Parameters()->projection();
  // Get values for compression and recompression cut-off method
  cutOffMethod_ = T->Parameters()->cutOffMethod();

  // GET A PRIORY RANK FOR COMPRESSOIN AND RE-COMPRESSION
  aPrioriRank_ = T->Parameters()->aPrioriRank();

  // Get rank cap factor for compression and recompression
  rankCapFactor_ = T->Parameters()->rankCapFactor();

  // Get deploy factor for compression and re-compression
  deployFactor_ = T->Parameters()->deployFactor();
  
  // Pointer to the frobNorm list:
  frobNorms_ = &( T->frobNorms[0] );

  // Pointer to the globFrobNorm of the tree;
  globFrobNorm_ = &(T -> globFrobNorm);

  // Pointer to the globSize of the tree;
  globSize_ = &(T -> globSize);

  // Pointer to the totalSizes list:
  totalSizes_ = &( T->totalSizes[0] );

  // Tree depth
  treeDepth_ = T->Parameters()->treeLevelThreshold();
  exact_solve_from_leaf_ = T->Parameters()->exact_solve_from_leaf();

}

// Destructor
superNode::~superNode()
{
  // Delete interaction edges
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      delete outEdges[i];
    }
  
}

redNode* superNode::redParent( int l )
{
  // cross-recursive !

  if ( l == 0 )
    {
      return parent_->parent();
    }
  else
    {
      return redParent()->redParent( l );
    }
}



// Compress all well-separated interactions
// the original compress function
timeTuple2 superNode::compress(int flag)
{
  //std::cout<< "start super node compress"<<std::endl;
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  //int compressing_flag = (level_ > (treeDepth_ - exact_solve_from_leaf_))*flag;
  int compressing_flag;
  double B_norm=0.0;
  if(exact_solve_from_leaf_<0){
    compressing_flag = 1;
  }
  else{
    compressing_flag = (level_ > (treeDepth_ - exact_solve_from_leaf_))*flag;
  }
  
  
 
  start = clock();
  // Check if superNode has any variable
  if ( m() == 0 )
    {
      //      std::cout<< "super node empty"<<std::endl;
      parent() -> m( 0 );
      parent() -> n( 0 );
      redParent() -> m( 0 );
      redParent() -> n( 0 );
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }
  
  // First erase all previously compressed edges from in/out edge lists
  eraseCompressedEdges();

  
  //std::cout<<"// Concatenate U matrices:"<<std::endl;
  //std::cout<< "start concatenate U matrices"<<std::endl;
  // calculate the total number of columns
  int nCols = 0,nColsCount = 0;
  // Go over all incoming edges, and compress those are well-separated
  
  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      // Check if the edge is well-separated
      
      
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nCols += inEdges[i] -> matrix ->cols();
	  nColsCount+=1;
	}
    }

  
  densMat bigU( m(), nCols );
  int nColsFilled = 0;
  //int add_vec1=0,add_vec2=0; 
  densMat Utemp(m(),1);
  densMat matrixU(1,1);
  densMat matrixV(1,1);
  VectorXd svalues(1);
  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      //std::cout<<"Check if the edge is well-separated"<<std::endl;
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  bigU.block( 0, nColsFilled, m(), inEdges[i]->matrix->cols() ) << *(inEdges[i]->matrix);   
	  
	
	  nColsFilled += inEdges[i]->matrix->cols();
	}
    }
    // calculate the total number of rows
  int nRows = 0, nRowsCount = 0;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      // Check if the edge is well-separated

      if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nRows += outEdges[i]->matrix->rows();
	  nRowsCount +=1;
	}
    }

    int kU = 0;
    
    if ( (nCols == 0) && (nRows == 0) )
      {
#if DEBUG
	//std::cout<<"super node has 0 ncols, nRows for well separated interactions"<<std::endl;
#endif
	
	parent() -> m( 0 );
	parent() -> n( 0 );
	redParent() -> m( 0 );
	redParent() -> n( 0 );
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }
    
    if ( (nCols == 0) || (nRows == 0) )
      {
	std::cout<<"TROUBLE! nCols= "<<nCols<<", nRows= "<<nRows<<", nColsCount= "<<nColsCount<<", nRowsCount= "<<nRowsCount<<std::endl;
	parent() -> m( 0 );
	parent() -> n( 0 );
	redParent() -> m( 0 );
	redParent() -> n( 0 );
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }

    // Now we should reCommpress bigU and bigV with the same rank (and smaller than m() and n())
    // reCompress bigU = Unew * R and bigV = L * Vnew
    finish = clock();
    times[1] += double(finish-start)/CLOCKS_PER_SEC;
    start = clock();

    #if DEBUG
    std::cout<<"before SVD"<<std::endl;
#endif
    if(lowRankMethod_==0){
      Eigen::JacobiSVD<densMat> svdU( bigU, Eigen::ComputeThinU | Eigen::ComputeThinV );

      matrixU.resize(svdU.matrixU().rows(),svdU.matrixU().cols());
      matrixU = svdU.matrixU();
      matrixV.resize(svdU.matrixV().rows(),svdU.matrixV().cols());
      matrixV = svdU.matrixV();
      svalues.resize(svdU.singularValues().size());
      svalues = svdU.singularValues();

      double adaptive_epsilon = epsilon_;
      //*pow(2.,-(treeDepth_-level_)/3.);
	//*pow(2.,(treeDepth_-level_)/3.);
	//*pow(2.,treeDepth_-level_);
      
      B_norm = bigU.norm();
      //      kU = cutOff( svdU, cutOffMethod_, epsilon_, aPrioriRank_, rankCapFactor_, B_norm );
      kU = cutOff( svdU, cutOffMethod_, adaptive_epsilon, aPrioriRank_, rankCapFactor_, B_norm );
    }
    else{
      //int rankCap = (int) ( aPrioriRank_ * std::pow( rankCapFactor_, treeDepth_-level_) );
      int rankCap = std::min(m()/2+1,(int) ( aPrioriRank_));// * std::pow( rankCapFactor_, treeDepth_-level_) );
      std::cout<<"ratio: "<<rankCap*1.0/m()<<"\t";
      //std::cout<<"Rank cap: "<<rankCap<<"aPrioriRank: "<<aPrioriRank_<<", rankCapFactor_: "<<rankCapFactor_<<", tree depth: "<<treeDepth_<<", level:"<<level_<<std::endl;
      RedSVD::RedSVD<densMat> svdU( bigU, rankCap );
      kU = cutOff( svdU, cutOffMethod_, epsilon_, aPrioriRank_, rankCapFactor_,B_norm );
      matrixU.resize(svdU.matrixU().rows(),svdU.matrixU().cols());
      matrixU = svdU.matrixU();
      matrixV.resize(svdU.matrixV().rows(),svdU.matrixV().cols());
      matrixV = svdU.matrixV();
      svalues.resize(svdU.singularValues().size());
      svalues = svdU.singularValues();
      
      
    }
    #if DEBUG
    std::cout<<"finish SVD"<<std::endl;
    #endif
    
    finish = clock();
    times[0] += double(finish-start)/CLOCKS_PER_SEC;
    start = clock();
    
    
  
    //    kU = std::min(kU,(int)(1.2*m()/2));
    // take the maximum: This is going to be the size (Rank) of both interpolation and anterpolation operators associated to this superNode
    int k = kU;
    
    if ( k == 0 )
      {
	
	parent() -> m( k );
	parent() -> n( k );
	redParent() -> m( k );
	redParent() -> n( k ); 
#if DEBUG

#endif
	for ( unsigned int i = 0; i < inEdges.size(); i++ )
	  {
	    // Check if the edge is well-separated
	    if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	      {
		inEdges[i]->compress();
	      }
	  }
	for ( unsigned int i = 0; i < outEdges.size(); i++ )
	  {
	    // Check if the edge is well-separated
	    if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	      {
		outEdges[i]->compress();
	      }
	  }
	eraseCompressedEdges();
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }


    parent() -> m( k );
    parent() -> n( k );
    redParent() -> m( k );
    redParent() -> n( k ); 
  
    // Now that we have the exact size of parents, we can set RHS and VAR
    
    parent() -> RHS( new VectorXd( parent() -> m() ) );
    
    //*( parent() -> RHS() ) = VectorXd::Zero( parent() -> m() );
    parent() -> RHS() -> setZero();
    parent() -> VAR( new VectorXd( parent() -> n() ) );
    
    redParent() -> RHS( new VectorXd( redParent() -> m() ) );
    //*( redParent() -> RHS() ) = VectorXd::Zero( redParent() -> m() );
    
    redParent() -> RHS() -> setZero();
    redParent() -> VAR( new VectorXd( redParent() -> n() ) );

    

    // Create incoming edge from the black parent
    parent() -> outEdges.push_back( new edge( parent(), this ) );
    inEdges.push_back( parent() -> outEdges.back() );
    // Assign memory for the matrix Unew (from parent to this)
    
    inEdges.back() -> matrix = new densMat( m(), parent()->n());
    
    // Create outgoing edge to the black parent
    outEdges.push_back( new edge( this, parent() ) );
    parent() -> inEdges.push_back( outEdges.back() );
    
    
    // Assign memory for the matrix Vnew (from this to parent)
    outEdges.back() -> matrix = new densMat( parent()->m(), n() );
    
    Utemp.resize(m(), k);
    //    Utemp.block(0,0,m(),k) = svdU.matrixU().leftCols(k);
    Utemp.block(0,0,m(),k) = matrixU.leftCols(k);

    *( inEdges.back() -> matrix ) = Utemp;
    *( outEdges.back() -> matrix) = Utemp.transpose();

    //    if(HasADDVEC()){
    //here we specify the vector to preserve for the red parent. We only add it if the current supernode has addvec
    // it is the e1 vector
      /*
    redParent()->ADDVEC(new VectorXd(redParent()->n()));
         *(redParent()->ADDVEC()) = Utemp.transpose()*(*ADDVEC());

    // *(redParent()->ADDVEC()) = VectorXd::Zero(redParent()->n());
    // (*(redParent()->ADDVEC()))(0) = 1;

    
    redParent()->HasADDVEC_ = 1;

    }
    
    */
    densMat Rttemp(redParent()->m(),1);
    int firstRow = 0;
    
    for ( unsigned int i = 0; i < inEdges.size(); i++ )
      {
	// Check if the edge is well-separated
	
	
	if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	  {
	    //	std::cout<<"start inEdge "<<i<<std::endl;
	    // Say the matrix on the edge is K [ kold * nCols ]
	    // Update K = R [k * kold] * K, which will be [k * nCols]
	    int nColsi = inEdges[i] -> matrix -> cols();

	    // build inedge and outedge between the red parent and the current source of super node under consideration. 
	    redParent() -> inEdges.push_back( new edge( inEdges[i] -> source(), redParent() ) );
	    inEdges[i] -> source() -> outEdges.push_back( redParent() -> inEdges.back() );

	    redParent() -> inEdges.back() -> matrix = new densMat( redParent()->m(), nColsi );
	    //std::cout<<"finish creating new mat"<<std::endl;
	    Rttemp.resize(redParent()->m(),nColsi);
	    /*    
	    if(add_vec1>0){
	      Rttemp.row(0) = KT1.block(0,firstRow,1,nColsi);
	    }
	    if(add_vec2>0){
	      Rttemp.row(add_vec1) = KT2.block(0,firstRow,1,nColsi);
	    }
	    */
	    //std::cout<< "Rttemp has size:"<<redParent()->m()<<" , "<<nColsi<<"("<<K-k<<", "<< k<<" , "<<nColsi<<std::endl;
	    //	    Rttemp.block(0,0,k,nColsi) = (  svdU.singularValues().head(k).asDiagonal() * (svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() );
	    Rttemp.block(0,0,k,nColsi) = (  svalues.head(k).asDiagonal() * (matrixV.block( firstRow, 0, nColsi, k ) ).transpose() );


	    *(redParent()->inEdges.back()->matrix) = Rttemp;

	    inEdges[i] -> compress();


	    redParent() -> outEdges.push_back( new edge( redParent(), inEdges[i] -> source() ) );
	    inEdges[i] -> source() -> inEdges.push_back( redParent() -> outEdges.back() );

	    redParent() -> outEdges.back() -> matrix  =  new densMat( nColsi, redParent()->n() );

	    *(redParent()->outEdges.back()->matrix) = Rttemp.transpose();

	    firstRow += nColsi;
	    //std::cout<<"finish the other side"<<std::endl;

	  }
      }


    // Repeat all in the above for outEdges

    firstRow = 0;
    for ( unsigned int i = 0; i < outEdges.size(); i++ )
      {
	// Check if the edge is well-separated
	if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	  {	    
	    outEdges[i] -> compress();
	  }
      } 
    
    eraseCompressedEdges();

    finish = clock();
    times[1] += double(finish-start)/CLOCKS_PER_SEC;
    //std::cout<< "end super node compress"<<std::endl;
    return times;
    }




// Compress all well-separated interactions
// modified version: SVD with addvec
timeTuple2 superNode::compress_addvec(int flag)
{
  //std::cout<< "start super node compress"<<std::endl;

  //if(NumADDVEC_==0){
    //    std::cout<<"I assumed this won't happen!"<<std::endl;
  // return compress(flag);
  //}
  //else{
  
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  //int compressing_flag = (level_ > (treeDepth_ - exact_solve_from_leaf_))*flag;
  int compressing_flag;
  //double addvec_norm = 0.0;
  double B_norm = 0.0;
  if(exact_solve_from_leaf_<0){
    compressing_flag = 1;
  }
  else{
    //std::cout<<"level_= "<<level_<<", treeDepth_= "<< treeDepth_<<std::endl;
    compressing_flag = (level_ > (treeDepth_ - exact_solve_from_leaf_))*flag;
  }
  
  
 
  start = clock();
  // Check if superNode has any variable
  if ( m() == 0 )
    {
      //      std::cout<< "super node empty"<<std::endl;
      parent() -> m( 0 );
      parent() -> n( 0 );
      redParent() -> m( 0 );
      redParent() -> n( 0 );
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }
  
  // First erase all previously compressed edges from in/out edge lists
  eraseCompressedEdges();

  
  //std::cout<<"// Concatenate U matrices:"<<std::endl;
  //std::cout<< "start concatenate U matrices"<<std::endl;
  // calculate the total number of columns
  int nCols = 0;
  // Go over all incoming edges, and compress those are well-separated
  
  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      // Check if the edge is well-separated      
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nCols += inEdges[i] -> matrix ->cols(); 
	}
    }



  int nRows = 0;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      // Check if the edge is well-separated

      if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nRows += outEdges[i]->matrix->rows();
	}
    }

    int kU = 0;
    
    if ( (nCols == 0) && (nRows == 0) )
      {
	
	parent() -> m( 0 );
	parent() -> n( 0 );
	redParent() -> m( 0 );
	redParent() -> n( 0 );
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }
    
    if ( (nCols == 0) || (nRows == 0) )
      {
	std::cout<<"TROUBLE!\n";
	parent() -> m( 0 );
	parent() -> n( 0 );
	redParent() -> m( 0 );
	redParent() -> n( 0 );
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }

    if(projection_!=0 && projection_!=4){
      std::cout<<"Projection other than the option 0 is not yet supported!"<<std::endl;
      return times;

    }
  // bigU is for the well-separated interaction B^T.
  densMat bigU( m(), nCols );
  densMat KT(NumADDVEC_*2,nCols);
  //densMat addvec(m(),NumADDVEC_*2);// used to form columns of U

  densMat XsBtXw(m(),NumADDVEC_*2);// used to form [X_s,B^TX_w]  Here s is the index for the current super node, and w is for its well-separated nodes.
  densMat XwBXs(nCols, NumADDVEC_*2); //used to form [X_w,BX_s]
  XsBtXw = densMat::Zero(m(),NumADDVEC_*2);
  XwBXs = densMat::Zero(nCols,NumADDVEC_*2);
  // densMat KT(NumADDVEC_*2,nCols);// the row to be added on top of R^T
  int nColsFilled = 0;
  int num_addvec = NumADDVEC_;

  //size to be determined, set column number to be 1 at first
  densMat Utemp(m(),1);
  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      //std::cout<<"Check if the edge is well-separated"<<std::endl;
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{

	  if(inEdges[i]->source()->HasADDVEC()){
	    //Calculate B^TX_w in [X_s,B^TX_w]
	    if(projection_!=2){
	      // we need the following if the projection is not second order symmetric
	      XsBtXw.block(0,num_addvec,m(),num_addvec) += (*(inEdges[i]->matrix))*(*inEdges[i]->source()->ADDVEC);
	    //Calculate BX_s in [X_w,BX_s]
	    XwBXs.block(nColsFilled,num_addvec,inEdges[i]->matrix->cols(),num_addvec) = (*(inEdges[i]->matrix)).transpose()*(*ADDVEC);

	    }
	    //Obtain X_w in [X_w,BX_s]
	    assert(inEdges[i]->matrix->cols()==inEdges[i]->source()->m());
	    XwBXs.block(nColsFilled,0,inEdges[i]->matrix->cols(), num_addvec) = *inEdges[i]->source()->ADDVEC;
	  }
	  else{

	    //#if DEBUG
	    std::cout<<"A well-separated node does not have addvec available. This should not happen!"<<std::endl;
	    //#endif
	  }
	  if(!std::isnormal(inEdges[i]->matrix->norm())&&(!(inEdges[i]->matrix->norm()<MACHINE_PRECISION))){
	    std::cout<<"Supernode: inEdge mat norm:"<<inEdges[i]->matrix->norm()<<std::endl;
	    std::cout<<*(inEdges[i]->matrix)<<std::endl;
	    exit(0);
	  }
	  bigU.block( 0, nColsFilled, m(), inEdges[i]->matrix->cols() ) << *(inEdges[i]->matrix);   
	
	  nColsFilled += inEdges[i]->matrix->cols();
	}
    }
#if DEBUG
  std::cout<<"bigU norm: "<<bigU.norm()<<std::endl;

  std::cout<<"Form addvec2 and bigU"<<std::endl;
#endif

  
  
    if(ADDVEC->rows()!=m()){
      std::cout<<"ADDVEC got the wrong size!"<<std::endl;
      exit(0);
    }

    //obtain X_s in [X_s,B^TX_w]
    XsBtXw.block(0,0,m(),num_addvec) = *ADDVEC;

    /*
      //normalize XsBtXw and XwBXs
    for(int i=0;i<XsBtXw.cols();i++){
      if(XsBtXw.col(i).norm()>MACHINE_PRECISION){
      XsBtXw.col(i)=XsBtXw.col(i)/XsBtXw.col(i).norm();
      }
    }

    for(int i=0;i<XwBXs.cols();i++){
      if(XwBXs.col(i).norm()>MACHINE_PRECISION){
      XwBXs.col(i)=XwBXs.col(i)/XwBXs.col(i).norm();
      }
    }
    */

    //orthonormalize [X_s,B^TX_w]
    Eigen::FullPivHouseholderQR<densMat> *QR;
    densMat Q;
    
    if(projection_==4){
      QR = new Eigen::FullPivHouseholderQR<densMat>(XsBtXw);
      Q.resize(QR->matrixQ().rows(), QR->rank());
      Q = QR->matrixQ().leftCols(QR->rank());
    }
    int addvec_rank;


    //    std::cout<<"SVD for Xs and Xw"<<std::endl;
    //std::cout<<"norm of S: "<< addvec.norm()<<", norm of W: "<<XwBXs.norm()<<std::endl;

    //Eigen::JacobiSVD<densMat> *svd_s,*svd_w;
    
    //*svd_s = Eigen::JacobiSVD<densMat>(XsBtXw,Eigen::ComputeThinU);
    //*svd_w = Eigen::JacobiSVD<densMat>(XwBXs,Eigen::ComputeThinU);

    densMat Us, Uw;
    int rank_s,rank_w, rank_prod;
    if(projection_!=4){
      Eigen::JacobiSVD<densMat> svd_s( XsBtXw, Eigen::ComputeThinU);
      rank_s = cutOff(svd_s, cutOffMethod_, epsilon_addvec_, aPrioriRank_, rankCapFactor_,XsBtXw.norm());
      Us.resize(XsBtXw.rows(),rank_s);
      Us = svd_s.matrixU().leftCols(rank_s);
    }
    if (projection_==1||projection_==2){
      Eigen::JacobiSVD<densMat> svd_w( XwBXs, Eigen::ComputeThinU);
      rank_w = cutOff(svd_w, cutOffMethod_, epsilon_addvec_, aPrioriRank_, rankCapFactor_,XwBXs.norm());
      Uw.resize(XwBXs.rows(),rank_w);
      Uw = svd_w.matrixU().leftCols(rank_w);  
    }
    

/*
    std::cout<<"Check orthogonalization:"<<std::endl;
    std::cout<<addvec<<std::endl;
    std::cout<<QR_addvec->matrixQ()<<std::endl;
    std::cout<<Q<<std::endl;
    std::cin.get();
*/
    #if DEBUG
    std::cout<<"Project using Us and Uw"<<std::endl;
    #endif

    //form the product U_sU_s^TB^TU_wU_w^T
    densMat prod,Ur;
    if(projection_==1 || projection_==2){
   
      //      if(projection_==0){
	// first order one-sided projection
      //	prod.resize(Us.rows(),Uw.rows());
      //prod = Us*(Us.transpose()*bigU);
      //}
      if(projection_==1){
	// first order symmetric projection
	prod.resize(Us.rows(),Uw.rows());
	prod = (Us*(Us.transpose()*bigU*Uw)*Uw.transpose());
	
      }
      else if(projection_==2){
	// second order symmetric projection
	prod.resize(Us.rows(),Uw.rows());
	prod = Us*(Us.transpose()*bigU) + (bigU*Uw)*Uw.transpose()  - (Us*(Us.transpose()*bigU*Uw)*Uw.transpose());
      }
      Eigen::JacobiSVD<densMat> svd_prod(prod,Eigen::ComputeThinU|Eigen::ComputeThinV);
      rank_prod = cutOff(svd_prod, cutOffMethod_,epsilon_addvec_, aPrioriRank_, rankCapFactor_,bigU.norm());
      addvec_rank = rank_prod;
      Ur.resize(svd_prod.matrixU().rows(),addvec_rank);
      Ur = svd_prod.matrixU().leftCols(addvec_rank);

    }
    else if(projection_==0){
      Ur.resize(Us.rows(),Us.cols());
      Ur = Us;
      addvec_rank = rank_s;
    }
    else if(projection_==4){
      //we use QR to get orthogonal basis
      addvec_rank = QR->rank();
      Ur.resize(Q.rows(),addvec_rank);
      Ur = Q;
    }
    else{
      std::cout<<"Undefined projection approach!"<<std::endl;
      assert(0);
    }

    /*
    //We check the accuracy of SVD
    densMat svd_error;
    svd_error.resize(bigU.rows(),bigU.cols());
    svd_error = svd_prod.matrixU()*svd_prod.singularValues().asDiagonal()*svd_prod.matrixV().transpose();
    svd_error = prod-svd_error;
    std::cout<<"Relative error of SVD_prod: "<<svd_error.norm()/prod.norm()<<" , norm of prod: "<<prod.norm()<<std::endl;

    */
    


    

    #if DEBUG
    std::cout<<"additional rank: "<<rank_prod<<std::endl;
    #endif
    
    KT.setZero();
    KT = Ur.transpose()*bigU;
    /*
    if(projection_==0|| projection_==4)
    {
      
    }
    else{
      std::cout<<"Unsupported projection type"<<std::endl;
      assert(0);
    }
    */
    //std::cout<<"row of prod: "<< prod.rows()<<" , rank of prod: "<< rank_prod<<" , rank of w: "<<rank_w<<" , rank of s: "<<rank_s<<" , rank of svdU: "<<kU<<std::endl;
    #if DEBUG
    std::cout<<"norm of KT after projection:"<< KT.norm()<<std::endl;
    std::cout<<"KT size: "<<KT.rows()<<" , "<<KT.cols()<<std::endl;
    #endif
    //std::cout<< Uw<<std::endl;
    if(!(std::isnormal(KT.norm())||KT.norm()<MACHINE_PRECISION)){
      std::cout<<KT<<std::endl;
    }
      
    // KT = QR_addvec->matrixQ().leftCols(addvec_rank).transpose()*bigU;
    //addvec_norm = std::max(KT.norm(),addvec_norm);
    //B_norm = KT.norm();//bigU.norm();
    B_norm = bigU.norm();
    if(B_norm<=MACHINE_PRECISION){
      std::cout<<"B_norm "<<B_norm<<std::endl;
      //std::cout<<bigU<<std::endl;
    }
    bigU -= Ur*KT;
    
    //assert(B_norm>MACHINE_PRECISION);
    //bigU /=B_norm;
    //B_norm = 1;
    // Now we should reCommpress bigU and bigV with the same rank (and smaller than m() and n())
    // reCompress bigU = Unew * R and bigV = L * Vnew
    finish = clock();
    times[1] += double(finish-start)/CLOCKS_PER_SEC;
    start = clock();

    #if DEBUG
    std::cout<<"before SVD"<<std::endl;
#endif
    Eigen::JacobiSVD<densMat> svdU( bigU, Eigen::ComputeThinU | Eigen::ComputeThinV );

    #if DEBUG
    std::cout<<"finish SVD"<<std::endl;
    #endif
    
    finish = clock();
    times[0] += double(finish-start)/CLOCKS_PER_SEC;
    start = clock();
    kU = cutOff( svdU, cutOffMethod_, epsilon_, aPrioriRank_, rankCapFactor_, B_norm );
    // std::cout<<"row of prod: "<< prod.rows()<<" , rank of prod: "<< rank_prod<<" , rank of w: "<<rank_w<<" , rank of s: "<<rank_s<<" , rank of svdU: "<<kU<<std::endl;
    int k = kU;
    #if DEBUG
    std::cout<<"orthogonality test: "<< (Us.transpose()*svdU.matrixU().leftCols(k)).norm()<<std::endl;
    #endif
    int K=0;
    K=k+addvec_rank;
    //   std::cout<<"rank: "<<k<<" , "<<addvec_rank<<std::endl;

    if ( K == 0 )
      {
	assert(k==K);	
	parent() -> m( k );
	parent() -> n( k );
	redParent() -> m( k );
	redParent() -> n( k ); 

	for ( unsigned int i = 0; i < inEdges.size(); i++ )
	  {
	    // Check if the edge is well-separated
	    if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	      {
		inEdges[i]->compress();
	      }
	  }
	for ( unsigned int i = 0; i < outEdges.size(); i++ )
	  {
	    // Check if the edge is well-separated
	    if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	      {
		outEdges[i]->compress();
	      }
	  }
	eraseCompressedEdges();
	finish = clock();
	times[1] += double(finish-start)/CLOCKS_PER_SEC;
	return times;
      }

    parent() -> m( K );
    parent() -> n( K );
    redParent() -> m( K );
    redParent() -> n( K ); 
  
    // Now that we have the exact size of parents, we can set RHS and VAR
    
    parent() -> RHS( new VectorXd( parent() -> m() ) );
    parent() -> RHS() -> setZero();
    parent() -> VAR( new VectorXd( parent() -> n() ) );
    
    redParent() -> RHS( new VectorXd( redParent() -> m() ) );
    redParent() -> RHS() -> setZero();
    redParent() -> VAR( new VectorXd( redParent() -> n() ) );

    

    // Create incoming edge from the black parent
    parent() -> outEdges.push_back( new edge( parent(), this ) );
    inEdges.push_back( parent() -> outEdges.back() );
    // Assign memory for the matrix Unew (from parent to this)
    
    inEdges.back() -> matrix = new densMat( m(), parent()->n());
    
    // Create outgoing edge to the black parent
    outEdges.push_back( new edge( this, parent() ) );
    parent() -> inEdges.push_back( outEdges.back() );
    
    
    // Assign memory for the matrix Vnew (from this to parent)
    outEdges.back() -> matrix = new densMat( parent()->m(), n() );
    
    //before pushing the matrices, we add the addvec part
    Utemp.resize(m(), K);
    Utemp.block(0,0,m(),addvec_rank) = Ur;
    Utemp.block(0,K-k,m(),k) = svdU.matrixU().leftCols(k);
  
    assert(std::isnormal(Utemp.norm()));
    if(!std::isnormal(Utemp.norm())){
      std::cout<<"Utemp abnormal with norm: "<<Utemp.norm()<<std::endl;
            exit(0);
    };
    *( inEdges.back() -> matrix ) = Utemp;
    *( outEdges.back() -> matrix) = Utemp.transpose();

    if(HasADDVEC()){
    //here we specify the vector to preserve for the red parent. We only add it if the current supernode has addvec
    // it is the e1 vector

      redParent()->ADDVEC = new densMat(Utemp.cols(),num_addvec);
     
      *(redParent()->ADDVEC) = Utemp.transpose()*(*ADDVEC);

    // *(redParent()->ADDVEC()) = VectorXd::Zero(redParent()->n());
    // (*(redParent()->ADDVEC()))(0) = 1;

    
    redParent()->NumADDVEC_ = num_addvec;
  
    }
    else{
  std::cout<<"Debug: no addvec?"<<std::endl;
    }
    
    
    densMat Rttemp(redParent()->m(),1);
    int firstRow = 0;
    
    for ( unsigned int i = 0; i < inEdges.size(); i++ )
      {
	// Check if the edge is well-separated
	if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	  {
	    //	std::cout<<"start inEdge "<<i<<std::endl;
	    // Say the matrix on the edge is K [ kold * nCols ]
	    // Update K = R [k * kold] * K, which will be [k * nCols]
	    int nColsi = inEdges[i] -> matrix -> cols();

	    // build inedge and outedge between the red parent and the current source of super node under consideration. 
	    redParent() -> inEdges.push_back( new edge( inEdges[i] -> source(), redParent() ) );
	    inEdges[i] -> source() -> outEdges.push_back( redParent() -> inEdges.back() );

	    redParent() -> inEdges.back() -> matrix = new densMat( redParent()->m(), nColsi );
	    Rttemp.resize(redParent()->m(),nColsi);
	    
	    Rttemp.block(0,0,addvec_rank, nColsi)= KT.block(0,firstRow,addvec_rank,nColsi);
	    Rttemp.block(K-k,0,k,nColsi) = (  svdU.singularValues().head(k).asDiagonal() * ( svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() );


	    if(!std::isnormal(Rttemp.norm())&&(!(Rttemp.norm()<MACHINE_PRECISION))){
	      std::cout<<"Rttemp abnormal with norm: "<<Rttemp.norm()<<", bigU has norm:"<< bigU.norm()<<std::endl;
	      //std::cout<< Rttemp<<std::endl;
      std::cout<<"bigU:"<<bigU.rows()<<" , "<<bigU.cols()<<std::endl;
      exit(0);
    };
	    *(redParent()->inEdges.back()->matrix) = Rttemp;

	    //std::cout<<"finish one side"<<std::endl;
	      //( svdU.singularValues().head(k).asDiagonal() * ( svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() );
	    inEdges[i] -> compress();


	    redParent() -> outEdges.push_back( new edge( redParent(), inEdges[i] -> source() ) );
	    inEdges[i] -> source() -> inEdges.push_back( redParent() -> outEdges.back() );

	    redParent() -> outEdges.back() -> matrix  =  new densMat( nColsi, redParent()->n() );

	    *(redParent()->outEdges.back()->matrix) = Rttemp.transpose();

	    firstRow += nColsi;

	  }
      }




    firstRow = 0;
    for ( unsigned int i = 0; i < outEdges.size(); i++ )
      {
	// Check if the edge is well-separated
	if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	  {	    
	    outEdges[i] -> compress();
	  }
      } 
    
    eraseCompressedEdges();

    finish = clock();
    times[1] += double(finish-start)/CLOCKS_PER_SEC;
    //std::cout<< "end super node compress"<<std::endl;
    
    return times;
    //}
}




// Compress all well-separated interactions, // modified version: with addvec only, no SVD
timeTuple2 superNode::compress_addvec_only(int flag)
{
  std::cout<<"Not debugged yet!"<<std::endl;
  assert(0);
  //std::cout<< "start super node compress"<<std::endl;
  timeTuple2 times;

  // we comment this function for debugging
  /*
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  int compressing_flag;
  if(exact_solve_from_leaf_<0){
    compressing_flag = 1;
  }
  else{
    compressing_flag = (level_ > (treeDepth_ - exact_solve_from_leaf_))*flag;
  }
  
  
 
  start = clock();
  // Check if superNode has any variable
  if ( m() == 0 )
    {
      //      std::cout<< "super node empty"<<std::endl;
      parent() -> m( 0 );
      parent() -> n( 0 );
      redParent() -> m( 0 );
      redParent() -> n( 0 );
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }
  
  // First erase all previously compressed edges from in/out edge lists
  eraseCompressedEdges();

  
  //std::cout<<"// Concatenate U matrices:"<<std::endl;
  //std::cout<< "start concatenate U matrices"<<std::endl;
  // calculate the total number of columns
  int nCols = 0;
  // Go over all incoming edges, and compress those are well-separated
  
  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      // Check if the edge is well-separated
      
      
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nCols += inEdges[i] -> matrix ->cols(); 
	}
    }

  densMat bigU( m(), nCols );
  densMat KT1(1,nCols), KT2(1,nCols);// the row to be added on top of R^T
  int nColsFilled = 0;
  int add_vec1=0,add_vec2=0; 
  densMat Utemp(m(),1);
  VectorXd addvec1(m()), addvec2(m());
  addvec1 = VectorXd::Zero(m());
  addvec2 = VectorXd::Zero(m());
        

  
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      //std::cout<<"Check if the edge is well-separated"<<std::endl;
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{

	  // if we have additional vector for SVD, we normalize the columns of incoming edges against the additional vector
	  if(inEdges[i]->source()->HasADDVEC()){
	    //std::cout<<"source has addvec"<<std::endl;
	    // if we have additional vector for SVD, we also constructe addvec2
	    addvec2 += (*(inEdges[i]->matrix))*(*inEdges[i]->source()->ADDVEC());
  }
	  else{
	        #if DEBUG
	    std::cout<<"A well-separated node does not have addvec available. This should not happen!"<<std::endl;
	        #endif
	  }
	  bigU.block( 0, nColsFilled, m(), inEdges[i]->matrix->cols() ) << *(inEdges[i]->matrix);   
	  
	  nColsFilled += inEdges[i]->matrix->cols();
	}
    }
#if DEBUG
  std::cout<<"Form addvec2 and bigU"<<std::endl;
#endif
  if(HasADDVEC()){
    // normalize the ADDVEC
    //std::cout<<"right now this shouldn't be visited"<<std::endl;
    if(ADDVEC()->size()!=m()){
      std::cout<<"addVEC got the wrong size!"<<std::endl;
      exit(0);
    }
    if(ADDVEC()->norm()>MACHINE_PRECISION){
      addvec1 = *(ADDVEC());
      addvec1 = addvec1/addvec1.norm();
      KT1 = addvec1.transpose()*bigU;
      //   if(KT1.norm()>MACHINE_PRECISION)
      if(KT1.norm()>epsilon_){
	//	std::cout<<"KT1 norm: "<<KT1.norm();
	add_vec1 = 1;
      }
      //we add addvec1 only if both addvec1 and the KT1 are nonzero.
    }
    
  }

  // orthogonalize addvec2 against addvec1 if addvec1 is nonzero
  if(add_vec1)
    addvec2-= addvec1.dot(addvec2)*addvec1;
  
  if(addvec2.norm()>MACHINE_PRECISION){
    addvec2 /=addvec2.norm();
    KT2 = addvec2.transpose()*bigU;
    // we always add addvec2
    //if(KT2.norm()>MACHINE_PRECISION)
    if(KT2.norm()>epsilon_){
      //      std::cout<<"KT2 norm: "<<KT2.norm()<<std::endl;
      add_vec2 = 1;
    }
    //we add addvec2 only if both addvec2 and KT2 are nonzero.
  }
  int add_vec_sum = add_vec1+add_vec2;
    
  //add_vec1 = 0;
  //add_vec2 = 0;
   

  
  if(add_vec1)
    bigU -= addvec1*KT1;
  if(add_vec2)
    bigU -= addvec2*KT2;
  
  #if DEBUG
  std::cout<<"check orthogonality"<<std::endl;
  
  for(unsigned int i=0;i< bigU.cols();i++){
    double inner_prod =1;
    inner_prod = addvec1.transpose()*bigU.col(i);
    if(abs(inner_prod)>MACHINE_PRECISION){
      std::cout<<"B^T not orthogonal to addvec1!"<<std::endl;
    }
    inner_prod = addvec2.transpose()*bigU.col(i);
    if(abs(inner_prod)>MACHINE_PRECISION){
      std::cout<<"B^T not orthogonal to addvec2!"<<std::endl;
    }    
  }
  if(add_vec1&&(abs(addvec1.norm()-1)>MACHINE_PRECISION))
    {
      std::cout<<"Addvec1 is not of unit length!"<<std::endl;
    }

  if(add_vec2&&(abs(addvec2.norm()-1)>MACHINE_PRECISION))
    {
      std::cout<<"Addvec2 is not of unit length!"<<std::endl;
    }
  if(abs(addvec1.transpose()*addvec2)>MACHINE_PRECISION)
    {
      std::cout<<"Addvec1 and addvec2 are not orthogonal!"<<std::endl;
    }

  std::cout<<"finish check orthogonality"<<std::endl;
#endif
    

  // calculate the total number of rows
  int nRows = 0;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      // Check if the edge is well-separated

      if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  nRows += outEdges[i]->matrix->rows();
	}
    }

  int kU = 0;
    
  if ( (nCols == 0) && (nRows == 0) )
    {
#if DEBUG
      //std::cout<<"super node has 0 ncols, nRows for well separated interactions"<<std::endl;
#endif
      
      parent() -> m( 0 );
      parent() -> n( 0 );
      redParent() -> m( 0 );
      redParent() -> n( 0 );
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }
    
  if ( (nCols == 0) || (nRows == 0) )
    {
      std::cout<<"TROUBLE!\n";
      parent() -> m( 0 );
      parent() -> n( 0 );
      redParent() -> m( 0 );
      redParent() -> n( 0 );
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }

  // Now we should reCommpress bigU and bigV with the same rank (and smaller than m() and n())
  // reCompress bigU = Unew * R and bigV = L * Vnew
  finish = clock();
  times[1] += double(finish-start)/CLOCKS_PER_SEC;
  start = clock();

    #if DEBUG
  std::cout<<"before SVD"<<std::endl;
#endif
  //Eigen::JacobiSVD<densMat> svdU( bigU, Eigen::ComputeThinU | Eigen::ComputeThinV );

    #if DEBUG
  std::cout<<"finish SVD"<<std::endl;
    #endif
    
  finish = clock();
  times[0] += double(finish-start)/CLOCKS_PER_SEC;
  start = clock();
  kU = 0;//cutOff( svdU, cutOffMethod_, epsilon_, aPrioriRank_, rankCapFactor_ );
  
  // take the maximum: This is going to be the size (Rank) of both interpolation and anterpolation operators associated to this superNode
  int k = kU;
    
  int K=add_vec1+add_vec2;
  
  /*
    int margin = svdU.singularValues().size()-k;
    int num2add=std::min(margin, add_vec_sum-add_vec1-add_vec2);
    if(add_vec_sum>add_vec1+add_vec2){
      if(num2add==1){
      if(abs(svdU.singularValues()(k))>MACHINE_PRECISION){
        k++;
	std::cout<<"We compensate the rank by 1"<<std::endl;
	}
      }
      else if(num2add==2){
      if(abs(svdU.singularValues()(k+1))>MACHINE_PRECISION){
        k+=2;
	std::cout<<"We compensate the rank by 2"<<std::endl;
	}
	else if(abs(svdU.singularValues()(k))>MACHINE_PRECISION){
	  k++;
	  std::cout<<"We compensate the rank by 1"<<std::endl;
	  }
      }
      else{
      std::cout<<"We don't compensate since margin is not enough!"<<std::endl;
      }
      K=k;
    }
    else if(add_vec_sum==add_vec1+add_vec2){
      
    // std::cout<<"this is the case add_vec is added, addvec:"<<add_vec1<<" , "<<add_vec2<<std::endl;
      K= k+add_vec1 + add_vec2;
    }
    else{
    std::cout<<"Wrong add_vec_sum!"<<std::endl;
    }
  */
  //    std::cout<<"addvec/K= "<<add_vec1+add_vec2<<" / "<<K<<std::endl;
  // the matrix is zero only if the rank of SVD and the projection on add vecs are both zero
  /*
  if ( K == 0 )
    {
      
      parent() -> m( K );
      parent() -> n( K );
      redParent() -> m( K );
      redParent() -> n( K ); 
#if DEBUG
      std::cout<<"case of zero rank,addvec= "<<add_vec1+add_vec2<<std::endl;
#endif
      for ( unsigned int i = 0; i < inEdges.size(); i++ )
	{
	  // Check if the edge is well-separated
	  if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	    {
#if MEMORY
	      change_size(-1.0*inEdges[i]->matrix->rows()*inEdges[i]-> matrix->cols());
	      inEdges[i]->source()->change_size(-1.0*inEdges[i]->matrix->rows()*inEdges[i]-> matrix->cols());
#endif 
	      inEdges[i]->compress();
	    }
	}
      for ( unsigned int i = 0; i < outEdges.size(); i++ )
	{
	  // Check if the edge is well-separated
	  if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	    {
#if MEMORY
	      change_size(-1.0*outEdges[i]->matrix->rows()*outEdges[i]-> matrix->cols());
	      outEdges[i]->destination()->change_size(-1.0*outEdges[i]->matrix->rows()*outEdges[i]-> matrix->cols());
#endif 
	      outEdges[i]->compress();
	    }
	}
      eraseCompressedEdges();
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }

    #if DEBUG
  std::cout<<"finish defining K"<<std::endl;
    #endif

  parent() -> m( K );
  parent() -> n( K );
  redParent() -> m( K );
  redParent() -> n( K ); 
  
  // Now that we have the exact size of parents, we can set RHS and VAR
  parent() -> RHS( new VectorXd( parent() -> m() ) );
  //*( parent() -> RHS() ) = VectorXd::Zero( parent() -> m() );
  parent() -> RHS() -> setZero();
  parent() -> VAR( new VectorXd( parent() -> n() ) );
    
  redParent() -> RHS( new VectorXd( redParent() -> m() ) );
  //*( redParent() -> RHS() ) = VectorXd::Zero( redParent() -> m() );
  redParent() -> RHS() -> setZero();
  redParent() -> VAR( new VectorXd( redParent() -> n() ) );

  // Create incoming edge from the black parent
  parent() -> outEdges.push_back( new edge( parent(), this ) );
  inEdges.push_back( parent() -> outEdges.back() );
  // Assign memory for the matrix Unew (from parent to this)
  inEdges.back() -> matrix = new densMat( m(), parent()->n());
#if MEMORY
  change_size(1.0*m()*n());
  parent()->change_size(1.0*m()*n());
#endif
  // Create outgoing edge to the black parent
  outEdges.push_back( new edge( this, parent() ) );
  parent() -> inEdges.push_back( outEdges.back() );
    
  // Assign memory for the matrix Vnew (from this to parent)
  outEdges.back() -> matrix = new densMat( parent()->m(), n() );
#if MEMORY
  change_size(1.0*parent()->m()*n());
  outEdges.back()->destination()->change_size(1.0*parent()->m()*n());
#endif
    

  //before pushing the matrices, we add the addvec part
  Utemp.resize(m(), K);
  if(add_vec1){
    Utemp.col(0) = addvec1;
    //    for(int i=0;i<m();i++)
    //Utemp(i,0) = (*ADDVEC())(i);
  }
  if(add_vec2){
    Utemp.col(add_vec1) = addvec2;
  }

  //since we only use constant, we don't add svd part
  //Utemp.block(0,K-k,m(),k) = svdU.matrixU().leftCols(k);

  *( inEdges.back() -> matrix ) = Utemp;
  *( outEdges.back() -> matrix) = Utemp.transpose();

  if(HasADDVEC()){
    //here we specify the vector to preserve for the red parent. We only add it if the current supernode has addvec
    // it is the e1 vector

    redParent()->ADDVEC(new VectorXd(redParent()->n()));
    *(redParent()->ADDVEC()) = Utemp.transpose()*(*ADDVEC());

     
    //*(redParent()->ADDVEC()) = VectorXd::Zero(redParent()->n());
    //(*(redParent()->ADDVEC()))(0) = 1;

    
    redParent()->NumADDVEC_ = 1;

  }
    
    
  densMat Rttemp(redParent()->m(),1);
  int firstRow = 0;
    
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {
      // Check if the edge is well-separated
      
      
      if ( inEdges[i] -> isWellSeparated()&&compressing_flag )
	{
	  //std::cout<<"start inEdge "<<i<<std::endl;
	  // Say the matrix on the edge is K [ kold * nCols ]
	  // Update K = R [k * kold] * K, which will be [k * nCols]
	  int nColsi = inEdges[i] -> matrix -> cols();

	  // build inedge and outedge between the red parent and the current source of super node under consideration. 
	  redParent() -> inEdges.push_back( new edge( inEdges[i] -> source(), redParent() ) );
	  inEdges[i] -> source() -> outEdges.push_back( redParent() -> inEdges.back() );

	  redParent() -> inEdges.back() -> matrix = new densMat( redParent()->m(), nColsi );

#if MEMORY
	  redParent() -> change_size(1.0*redParent()->m()*nColsi);
	  inEdges[i] -> source()-> change_size(1.0*redParent()->m()*nColsi);
#endif 
	      
	  //std::cout<<"finish creating new mat"<<std::endl;
	  Rttemp.resize(redParent()->m(),nColsi);
	      
	  if(add_vec1>0){
	    Rttemp.row(0) = KT1.block(0,firstRow,1,nColsi);
	  }
	  if(add_vec2>0){
	    Rttemp.row(add_vec1) = KT2.block(0,firstRowv,1,nColsi);
	  }

	  //std::cout<< "Rttemp has size:"<<redParent()->m()<<" , "<<nColsi<<"("<<K-k<<", "<< k<<" , "<<nColsi<<std::endl;

	  //since here we only consider constant part, we don't add the svd part
	  //Rttemp.block(K-k,0,k,nColsi) = (  svdU.singularValues().head(k).asDiagonal() * ( svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() );


	  *(redParent()->inEdges.back()->matrix) = Rttemp;

	  //std::cout<<"finish one side"<<std::endl;
	  //( svdU.singularValues().head(k).asDiagonal() * ( svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() );
#if MEMORY
	  change_size(-1.0*inEdges[i]-> matrix->rows()*inEdges[i]-> matrix->cols());
	  inEdges[i]->source()-> change_size(-1.0*inEdges[i]-> matrix->rows()*inEdges[i]-> matrix->cols());
#endif 
	  inEdges[i] -> compress();


	  redParent() -> outEdges.push_back( new edge( redParent(), inEdges[i] -> source() ) );
	  inEdges[i] -> source() -> inEdges.push_back( redParent() -> outEdges.back() );

	  redParent() -> outEdges.back() -> matrix  =  new densMat( nColsi, redParent()->n() );

#if MEMORY
	  redParent() -> change_size(1.0*redParent()->n()*nColsi);
	  inEdges[i]->source() -> change_size(1.0*redParent()->n()*nColsi);
#endif 
	  *(redParent()->outEdges.back()->matrix) = Rttemp.transpose();

	  //( ( svdU.singularValues().head(k).asDiagonal() * ( svdU.matrixV().block( firstRow, 0, nColsi, k ) ).transpose() ) ).transpose();

	  firstRow += nColsi;
	  //std::cout<<"finish the other side"<<std::endl;

	}
    }


  // Repeat all in the above for outEdges

  firstRow = 0;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      // Check if the edge is well-separated
      if ( outEdges[i] -> isWellSeparated()&&compressing_flag )
	{
#if MEMORY
	  change_size(-1.0*outEdges[i]->matrix->rows()*outEdges[i]-> matrix->cols());
	  outEdges[i]->destination()->change_size(-1.0*outEdges[i]->matrix->rows()*outEdges[i]-> matrix->cols());
#endif 
	  outEdges[i] -> compress();
	}
    } 
    
  eraseCompressedEdges();

  finish = clock();
  times[1] += double(finish-start)/CLOCKS_PER_SEC;
  //std::cout<< "end super node compress"<<std::endl;
  */

  
  return times;
}








// Calculate k, the number of important singular values (used for compression)
int superNode::cutOff( Eigen::JacobiSVD<densMat> &svd, int method, double epsilon, int apRank, double rankCapFactor , double B_norm)
{
  int return_value = 0;
  // number of singular values
  int p = svd.singularValues().size();
  
  if ( p == 0 )
    {
      return 0;
    }
  
  if ( svd.singularValues()(0)< MACHINE_PRECISION )
    {
      return 0;
    }
  if(std::abs(B_norm)>MACHINE_PRECISION&&method!=1&method!=9){
    printf("In the case with addvec, only method 1 is supported right now!");
    assert(0);
  }

  // method0 : cutofff based on the absolute value
  if ( method == 0 )
    {
     
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }
  // method1: cutoff based on relative values
  else if ( method == 1 )
    {
      //double sigma1 = std::max(B_norm,svd.singularValues()(0));
      double sigma1 = B_norm;
      //      std::cout<<"CutOff: B: "<< B_norm<<" , svd0: "<<svd.singularValues()(0)<<", SVD precision: "<<svd.threshold()<<std::endl;
      //            std::cout<<svd.singularValues()<<std::endl;
      //double sigma1 = svd.singularValues()(0);
      
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( svd.singularValues()(i) <= epsilon * sigma1 )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      //      return_value = k_return;
      	return_value = std::min( k_return, apRank );
    }  
  else if ( method == 2 )
    {      
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon * frobNorms_[ level_ ]  )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }
  else if ( method == 3 )
    {
      
      int k_return = p;
      
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon * (*globFrobNorm_) )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }

  else if (method == 4 )
    {
      
      int k_return = p;
      double frobNorm2 = 0;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm2 += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( std::sqrt(frobNorm2) > frobNorms_[ level_ ] * epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( std::sqrt(frobNorm2) <= frobNorms_[ level_ ] * epsilon )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
  else if ( method == 5 )
    {
      
      int k_return = p;
      double frobNorm = 0;
      double blockSize = ( svd.matrixU().rows() * svd.matrixV().rows() );
      double epsilon2 = blockSize * std::pow(frobNorms_[ level_ ],2) * epsilon * epsilon / totalSizes_[ level_ ];
      //double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( frobNorm > epsilon2 )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( frobNorm <= epsilon2 )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
    else if (method == 6 )
    {
      
      int k_return = p;
      double frobNorm2 = 0;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm2 += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( std::sqrt(frobNorm2) > (*globFrobNorm_) * epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( std::sqrt(frobNorm2) <= (*globFrobNorm_) * epsilon )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
    else if ( method == 7 )
      {
	
	int k_return = p;
	double frobNorm = 0;
	double blockSize = ( svd.matrixU().rows() * svd.matrixV().rows() );
	double epsilon2 = blockSize * std::pow((*globFrobNorm_),2) * epsilon * epsilon / (*globSize_);
	//double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
	for (int i = p-1; i >= 0; i-- )
	  {
	    frobNorm += std::pow( svd.singularValues()(i), 2 );
	    // Find the first place that the frobNorm is greater than epsilon
	    if ( frobNorm > epsilon2 )
	      {
		k_return = i;
		break;
	      }
	  }
	if ( frobNorm <= epsilon2 )
	  {
	    return 0;
	  }
	else
	  {
	    return_value = k_return + 1;
	  }
      }
    else if ( method == 8 )
      {
	return_value = std::min( p, apRank );
      }
  // using relative Frobenius norm
    else if ( method == 9 )
    {
      //double sigma1 = std::max(B_norm,svd.singularValues()(0));
      //double sigma1 = std::max(B_norm,MACHINE_PRECISION);
      double sigma1 = B_norm;
      //      std::cout<<"CutOff: B: "<< B_norm<<" , svd0: "<<svd.singularValues()(0)<<", SVD precision: "<<svd.threshold()<<std::endl;
      //            std::cout<<svd.singularValues()<<std::endl;
      //double sigma1 = svd.singularValues()(0);
      
      int k_return = 0;
      double accum_error = 0.0;
      for (int i = p-1; i >= 0; i-- )
	{
	  // Find the first singular value that is smaller than epsilon
	  accum_error += svd.singularValues()(i)*svd.singularValues()(i);
	  if(accum_error>(epsilon*sigma1)*(epsilon*sigma1)){
	    k_return = i+1;
	    break;
	  }
	}
      
      //      return_value = k_return;
      	return_value = std::min( k_return, apRank );
    }  

  if ( rankCapFactor > MACHINE_PRECISION )
    {
      int rankCap = (int) ( apRank * std::pow( rankCapFactor, treeDepth_-level_) );
      return std::min( return_value, rankCap );
    }
  
  else
    {
      return return_value;
    }
}

bool superNode::criterionCheck( int meth, double eps, RedSVD::RedSVD<densMat> &RSVD, densMat &A, int r )
{
  if ( r == 0 || (meth == 8) )
    {
      return true;
    }
  
  else if ( meth == 0 )
    {
      return ( RSVD.singularValues()(r-1) <= eps );
    }
  else if ( meth == 1 )
    {
      if ( RSVD.singularValues()(0)< MACHINE_PRECISION )
	{
	  return true;
	}
      else if ( r == 1 )
	{
	  return false;
	}
      else
	{
	  return ( RSVD.singularValues()(r-1) / RSVD.singularValues()(0) <= eps );
	}
    }
  else if ( meth == 2 )
    {
      return ( RSVD.singularValues()(r-1) <= eps * frobNorms_[ level_ ] );
    }
  else if ( meth == 3 )
    {
      return ( RSVD.singularValues()(r-1) <= eps * (*globFrobNorm_) );
    }
  else
    {
      densMat residu = A - RSVD.matrixU().leftCols(r) * RSVD.singularValues().head(r).asDiagonal() * RSVD.matrixV().leftCols(r).transpose();
      double normResidu = residu.norm();
      double blockSize = ( RSVD.matrixU().rows() * RSVD.matrixV().rows() );
      if ( meth == 4 )
	{
	  return ( normResidu <= eps * frobNorms_[ level_ ] );
	}
      else if ( meth == 5 )
	{
	  double epsilon2 = blockSize * std::pow(frobNorms_[ level_ ],2) * eps*eps / totalSizes_[ level_ ];
	  return ( normResidu*normResidu <= epsilon2 );
	}
      else if ( meth == 6 )
	{
	  return ( normResidu <= eps * (*globFrobNorm_) );
	}
      else if ( meth == 7 )
	{
	  double epsilon2 = blockSize * std::pow((*globFrobNorm_),2) * eps*eps / (*globSize_);
	  return ( normResidu*normResidu <= epsilon2 );
	}
    }
  return false;
}


// Calculate k, the number of important singular values (used for compression)
int superNode::cutOff( RedSVD::RedSVD<densMat> &svd, int method, double epsilon, int apRank, double rankCapFactor,double B_norm )
{
  int return_value = 0;
  // number of singular values
  int p = svd.singularValues().size();
  
  if ( p == 0 )
    {
      return 0;
    }
  
  if ( svd.singularValues()(0)< MACHINE_PRECISION )
    {
      return 0;
    }

  // method0 : cutofff based on the absolute value
  if ( method == 0 )
    {
     
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }
  // method1: cutoff based on relative values
  else if ( method == 1 )
    {
      double sigma1 = svd.singularValues()(0);
      
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( svd.singularValues()(i) < epsilon * sigma1 )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }  
  else if ( method == 2 )
    {      
      int k_return = p;
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon * frobNorms_[ level_ ]  )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }
  else if ( method == 3 )
    {
      
      int k_return = p;
      
      for (int i = 0; i < p; i++ )
	{
	  // Find the first singular value that is smaller than epsilon 
	  if ( ( svd.singularValues() )(i) < epsilon * (*globFrobNorm_) )
	    {
	      k_return = i;
	      break;
	    }
	}
      
      return_value = k_return;
    }

  else if (method == 4 )
    {
      
      int k_return = p;
      double frobNorm2 = 0;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm2 += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( std::sqrt(frobNorm2) > frobNorms_[ level_ ] * epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( std::sqrt(frobNorm2) <= frobNorms_[ level_ ] * epsilon )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
  else if ( method == 5 )
    {
      
      int k_return = p;
      double frobNorm = 0;
      double blockSize = ( svd.matrixU().rows() * svd.matrixV().rows() );
      double epsilon2 = blockSize * std::pow(frobNorms_[ level_ ],2) * epsilon * epsilon / totalSizes_[ level_ ];
      //double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( frobNorm > epsilon2 )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( frobNorm <= epsilon2 )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
    else if (method == 6 )
    {
      
      int k_return = p;
      double frobNorm2 = 0;
      for (int i = p-1; i >= 0; i-- )
	{
	  frobNorm2 += std::pow( svd.singularValues()(i), 2 );
	  // Find the first place that the frobNorm is greater than epsilon
	  if ( std::sqrt(frobNorm2) > (*globFrobNorm_) * epsilon )
	    {
	      k_return = i;
	      break;
	    }
	}
      if ( std::sqrt(frobNorm2) <= (*globFrobNorm_) * epsilon )
	{
	  return 0;
	}
      else
	{
	  return_value = k_return + 1;
	}
    }
    else if ( method == 7 )
      {
	
	int k_return = p;
	double frobNorm = 0;
	double blockSize = ( svd.matrixU().rows() * svd.matrixV().rows() );
	double epsilon2 = blockSize * std::pow((*globFrobNorm_),2) * epsilon * epsilon / (*globSize_);
	//double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
	for (int i = p-1; i >= 0; i-- )
	  {
	    frobNorm += std::pow( svd.singularValues()(i), 2 );
	    // Find the first place that the frobNorm is greater than epsilon
	    if ( frobNorm > epsilon2 )
	      {
		k_return = i;
		break;
	      }
	  }
	if ( frobNorm <= epsilon2 )
	  {
	    return 0;
	  }
	else
	  {
	    return_value = k_return + 1;
	  }
      }
    else if ( method == 8 )
      {
	return_value = std::min( p, apRank );
      }
  if ( rankCapFactor > MACHINE_PRECISION )
    {
      int rankCap = (int) ( apRank * std::pow( rankCapFactor, treeDepth_-level_) );
      return std::min( return_value, rankCap );
    }
  else
    {
      return return_value;
    }
}

// Go through all edges and 
timeTuple2 superNode::schurComp()
{
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  int edge_created=0, edge_modified=0;
  start = clock();
  // Step 0: Check if this node has any variable
  if ( n() == 0 )
    {
      eliminate();
      parent() -> leftChild() -> eliminate();
      parent() -> rightChild() -> eliminate();
      parent()->eliminate();
      finish = clock();
      times[1] += double(finish-start)/CLOCKS_PER_SEC;
      return times;
    }
  
  // First we detect the selfEdge (i.e., the edge from supernode to itself)
  edge* selfEdge = NULL;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      if ( outEdges[i] -> destination() == this )
	{
	  selfEdge = outEdges[i];
	  break;
	}
    }


  if ( selfEdge == NULL )
    {
      std::cout<<" ZERO PIVOT !"<<std::endl;
      std::exit(0);
    }

  // Now compute the inverse of the selfEdge matrix (i.e., inverse of the pivot)

  finish = clock();
  times[1] += double(finish-start)/CLOCKS_PER_SEC;
  start = clock();

  //  luPivot = new Eigen::PartialPivLU<densMat>( *(selfEdge -> matrix) );
  //    luPivot = new Eigen::FullPivLU<densMat>( *(selfEdge -> matrix) );
      luPivot = new Eigen::PartialPivLU<densMat>( *(selfEdge -> matrix) );
  
  finish = clock();
  times[0] += double(finish-start)/CLOCKS_PER_SEC;
  start = clock();

  edge* X;
  edge* Y;
  // Loop over all incoming edges ( exclude the selfEdge ): X
    
  for ( unsigned int i = 0; i < inEdges.size(); i++ )
    {

      X = inEdges[i];
      if ( ( X -> source() != this ) && !( X -> isEliminated() ) )
	{
	  
	  // At this point we can compute invPivot * X
	  //assert(std::isnormal(invPivot->norm())||(X->matrix->norm()<MACHINE_PRECISION));
	  assert((std::isnormal(X->matrix->norm()))||(X->matrix->norm()<MACHINE_PRECISION));
	  densMat factor = luPivot -> solve( *(X->matrix) );	 // this is what we have to L matrix in the LU factorization 
	    
	  // Loop over all outgoing edges ( exclude the selfEdge ):Y
	  for ( unsigned int j = 0; j < outEdges.size(); j++ )
	    {
	      
	      Y = outEdges[j];
	      if ( ( Y -> destination() != this ) && !( Y -> isEliminated() ) )
		{
		  
		  // Here we have ei -> selfEdge -> ej
		  // After elimination we have: i->j  with matrix = -X * invPivot * Y
		  // We first check if the edge from i to j already exist or not.
		  // if exist we just add the fill-in, otherwise create a new edge
		  edge* fillinEdge = NULL;
		  for ( unsigned int k = 0; k < X->source()->outEdges.size(); k++ )
		    {
		      if ( X->source()->outEdges[k]->destination() == Y->destination() )
			{
			  fillinEdge = X->source()->outEdges[k];
			  break;
			}
		    }
		  if ( fillinEdge == NULL )
		    {
		      X->source()->outEdges.push_back( new edge( X->source(), Y->destination() ) );
		      Y->destination()->inEdges.push_back( X->source()->outEdges.back() );
		      X->source()->outEdges.back()->matrix = new densMat( Y->matrix->rows(), X->matrix->cols() );
		      *( X->source()->outEdges.back()->matrix ) = - (*(Y->matrix)) * factor;
		      edge_created++;
		    }
		  else
		    {
		      *(fillinEdge -> matrix) -= (*(Y->matrix)) * factor;
		      edge_modified++;
		    }
		    if(!std::isnormal(Y->matrix->norm())){
		      // std::cout<<"The abnormal norm: "<< Y->matrix->norm()<<std::endl;
		      if(!(Y->matrix->norm()<MACHINE_PRECISION)){ 
			assert(std::isnormal(Y->matrix->norm()));}
		    }
		    
		}
	    }
	}
    }

  //  std::cout<<"schurComp: edge created/modified: "<<edge_created<<"/"<<edge_modified<<std::endl;
  edge_created=0;
  edge_modified = 0;

  // Now, mark the node as eliminated
  eliminate();

  // eliminate left and right redChildren

  parent() -> leftChild() -> eliminate();
  parent() -> rightChild() -> eliminate();

  finish = clock();
  times[1] += double(finish-start)/CLOCKS_PER_SEC;
  
  timeTuple2 times_black;
  // After eliminating a superNode, we can immediately eliminate its black-parent
  times_black = parent()->schurComp();
  times[0] += times_black[0];
  times[1] += times_black[1];
  return times;
}

// Split the vector VAR to the left and right redNodes
void superNode::splitVAR()
{

  if ( parent() -> leftChild() -> n() > 0 ) // i.e., if the left redNode has any unknowns
    {
      *( parent() -> leftChild() -> VAR() ) = VAR() -> segment( 0, parent() -> leftChild() -> n() );
    }

  if ( parent() -> rightChild() -> n() > 0 ) // i.e., if the right redNode has any unknowns
    {
      *( parent() -> rightChild() -> VAR() ) = VAR() -> segment( parent() -> leftChild() -> n(), parent() -> rightChild() -> n() );
    }

}


// Add a block of random columns to the right side of a matrix
void superNode::addRandomCols( densMat &A, int p ) 
{
  // Number of rows and cols:
  int m = A.rows();
  int n = A.cols();
  
  // Resize A: resizing is not conservative in Eigen!
  densMat B = A;
  A.resize( Eigen::NoChange, n + p );

  A.block( 0, 0, m, n ) = B;
  A.block( 0, n, m, p ) = densMat::Random( m, p );
  
}

// Add a block of random rows at the bottom of a matrix
void superNode::addRandomRows( densMat &A, int p ) 
{
  // Number of rows and cols:
  int m = A.rows();
  int n = A.cols();
  
  // Resize A
  densMat B = A;
  A.resize( m + p, Eigen::NoChange );

  A.block( 0, 0, m, n ) = B;
  A.block( m, 0, p, n ) = densMat::Random( p, n );
  
}

// Add a block of random columns to the right side of a matrix
void superNode::extendOrthoCols( densMat &A, int p ) 
{
  // Number of rows and cols:
  int m = A.rows();
  int n = A.cols() - p;
  
  /*
  // Resize A: resizing is not conservative in Eigen!
  densMat B = A;
  A.resize( Eigen::NoChange, n + p );
  
  A.block( 0, 0, m, n ) = B;
  */
  
  // Go through all columns and subtract from the new random col
  VectorXd newVec( m );
  for ( int i = 0; i < p; i++ )
    {
      newVec = VectorXd::Random( m );
      for ( int j = 0; j < n + i; j ++ )
	{
	  // Note: columns of A are supposed to have unit norm
	  double innerProd = newVec.dot( A.col(j) );
	  // subtract
	  newVec -= innerProd * A.col(j);
	}
      // normalize the new vector and add it to the matrix
      newVec /= newVec.norm();
      A.col( n + i ) = newVec;
    }

  // check if columns are orthogonal
  for ( int i = 0; i < n + p; i++ ) 
    {
      for ( int j = i+1; j < n + p; j++ ) 
	{
	  double innerProd = (A.col(i)).dot(A.col(j));
	  if ( innerProd / double(m)  > MACHINE_PRECISION )
	    {
	      std::cout<<" m = "<<m<<" n = "<<n<<" p = "<<p<<std::endl;
	      std::cout<<" i = "<<i<<" j = "<<j<<std::endl;
	      std::cout<<" inner product is = " << innerProd << std::endl;
	      std::exit(1);
	    }
	}
    }
}

// Add a block of random columns to the right side of a matrix
void superNode::extendOrthoRows( densMat &A, int p ) 
{
  // Number of rows and cols:
  int m = A.rows() - p;
  int n = A.cols();
  
  /*
  // Resize A: resizing is not conservative in Eigen!
  densMat B = A;
  A.resize( m + p, Eigen::NoChange );
  
  A.block( 0, 0, m, n ) = B;
  */

  // Go through all rows and subtract from the new random row
  VectorXd newVec( n );
  for ( int i = 0; i < p; i++ )
    {
      newVec = VectorXd::Random( n );
      for ( int j = 0; j < m + i; j ++ )
	{
	  // Note: columns of A are supposed to have unit norm
	  double innerProd = newVec.dot( A.row(j) );
	  // subtract
	  newVec -= innerProd * A.row(j).transpose();
	}
      // normalize the new vector and add it to the matrix
      newVec /= newVec.norm();
      A.row( m+i ) = newVec.transpose();
    }

    // check if columns are orthogonal
  for ( int i = 0; i < m + p; i++ ) 
    {
      for ( int j = i+1; j < m + p; j++ ) 
	{
	  double innerProd = (A.row(i)).dot(A.row(j));
	  if ( innerProd / double(n)  > MACHINE_PRECISION )
	    {
	      std::cout<<" m = "<<m<<" n = "<<n<<" p = "<<p<<std::endl;
	      std::cout<<" i = "<<i<<" j = "<<j<<std::endl;
	      std::cout<<" inner product is = " << innerProd << std::endl;
	      std::exit(1);
	    }
	}
    }

}

// Project to edge to parent
double superNode::project()
{
  // return zero if size is zero
  if ( n() == 0 )
    return 0;
  // first find edge to parent
  edge* parentEdge = NULL;
  for ( unsigned int i = 0; i < outEdges.size(); i++ )
    {
      if ( outEdges[i] -> destination() == parent() )
	{
	  parentEdge = outEdges[i];
	  break;
	}
    }

  if ( parentEdge == NULL )
    {
      // std::cout<<" No parent edge !"<<std::endl;
      return 0;
    }

  VectorXd p = parentEdge->matrix->transpose() * ( *(parentEdge -> matrix) * ( *VAR_s_ ) );
  double norm_p = p.norm();
  return norm_p*norm_p;
  
}
