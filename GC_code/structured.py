import numpy as np
class grid:
    '''
    Form a cubic grid with Nx x Ny x Nz interior points,
    and corresponding boundary points.
    i.e., at each direction we have:
    i=0, i=1, ... , i = Nx, i = Nx+1
    points, where the first and the last points are boundary points.
    The equation is 2nd order finite differece approximation of
    \nabala^2 T = 0, where T ( @ boundary) = f
    The resulted matrix and RHS will be stored in the given files
    in Matrix Market format
    '''
    def __init__(self, Nx, Ny, Nz, Lx, Ly, Lz, f, outputMatrixFile, outputRHSFile):
        self.Nx = Nx
        self.Ny = Ny
        self.Nz = Nz
        self.Lx = Lx
        self.Ly = Ly
        self.Lz = Lz
        self.f = f
        self.outputMatrixFile = outputMatrixFile
        self.outputRHSFile = outputRHSFile
        self.dx = Lx / (Nx + 1)
        self.dy = Ly / (Ny + 1)
        self.dz = Lz / (Nz + 1)
        self.idx2 = 1. / ( self.dx ) ** 2
        self.idy2 = 1. / ( self.dy ) ** 2
        self.idz2 = 1. / ( self.dz ) ** 2
        

    def value(self, idx):
        '''
        Get index of a point, and evaluate function f at that point
        '''
        i, j, k = idx
        x = self.dx * i
        y = self.dx * j
        z = self.dx * k
        return self.f( x, y, z )

    def isBoundary( self, idx ):
        '''
        Get index of a point and determine if it is on the boundary or not
        '''
        i, j, k = idx
        return ( i==0 or i==self.Nx+1 or j==0 or j==self.Ny+1 or k==0 or k==self.Nz+1 )
         
    
    def stencil(self, idx ):
        '''
        Get index of a point idx = (i,j,k)
        returns a tuple ( L, b )
        where L is a list of tuples of indices of interior neighbors, and tehir coeff.
        and b is the RHS of the equation for the given point
        '''
        i, j, k = idx
        
        # RHS value
        b = 0

        # List of interactions
        L = []

        # Add the self interaction
        L.append( (idx, -2.*( self.idx2 + self.idy2 + self.idz2 ) ) )

        # Determine neighbors:
        neighbors = [ (i-1,j,k), (i+1,j,k), (i,j-1,k), (i,j+1,k), (i,j,k-1), (i,j,k+1) ]
        coeffs = [ self.idx2, self.idx2, self.idy2, self.idy2, self.idz2, self.idz2 ]

        for i in xrange( len( neighbors ) ):
            neighb = neighbors[i]
            coeff = coeffs[i]
            if self.isBoundary( neighb ):
                b -= self.value( neighb ) * coeff
            else:
                L.append( (neighb, coeff) )

        return ( L, b )

    def idx2row(self, idx):
        '''
        get an index (i,j,k) and trasnform to a unique row number.
        '''
        i ,j, k = idx
        return (i-1) + (j-1) * self.Nx + (k-1) * self.Nx * self.Ny + 1
    
    def formMatrixRHS(self):
        '''
        Go through all interior points, and create correponsing enrties.
        Outputs are lists of the form
        ( row_index, col_index, value )
        one for the actual matrix, and for RHS
        '''
        listA = []
        listRHS = []
        for i in xrange(1, self.Nx+1):
            for j in xrange(1, self.Ny+1):
                for k in xrange(1, self.Nz+1):
                    L, b = self.stencil( (i,j,k) )
                    row = self.idx2row( (i,j,k) )
                    if b != 0:
                        listRHS.append( (row, 1, b) )
                    for entry in L:
                        neighb, coeff = entry
                        col = self.idx2row( neighb )
                        listA.append( (row, col, coeff) )
        return( listA, listRHS )

    def createMMfiles(self):
        '''
        Store the sparse matrix and RHS in Matrix Market format in files
        '''
        listA, listRHS = self.formMatrixRHS()
        
        # number of none-zeros
        nnzA = len(listA)
        nnzRHS = len(listRHS)
        
        # add the header line
        stringA = str(self.Nx*self.Ny*self.Nz) + ' ' + str(self.Nx*self.Ny*self.Nz) + ' ' + str(nnzA) + '\n'
        stringRHS = str(self.Nx*self.Ny*self.Nz) + ' 1 ' + str(nnzRHS) + '\n'

        # add entries as triples
        for entry in listA:
            row, col, val = entry
            stringA += str(row) + ' ' + str(col) + ' ' + str(val) + '\n'

        for entry in listRHS:
            row, col, val = entry
            stringRHS += str(row) + ' ' + str(col) + ' ' + str(val) + '\n'

        # Store in files
        Afile = open( self.outputMatrixFile , 'w' )
        Afile.write( stringA )
        Afile.close()
        RHSfile = open( self.outputRHSFile , 'w' )
        RHSfile.write( stringRHS )
        RHSfile.close()


if __name__ == '__main__':
    N=5
    dof = 2**N
    f = lambda x,y,z: np.exp( -( (x-.5)**2 + (y-.5)**2 + (z-.5)**2) )
    G = grid(Nx=2*dof, Ny=2*dof, Nz=dof, Lx=1., Ly=1., Lz=1., f=f, outputMatrixFile = 'A.mtx' , outputRHSFile = 'b.mtx')
    G.createMMfiles()
