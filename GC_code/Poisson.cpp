#include "tree.h"
#include "redNode.h"
#include "blackNode.h"
#include "superNode.h"
#include "params.h"
#include "edge.h"
#include "Eigen/Sparse"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include "time.h"
#include "Poisson.h"
#include <ctime>
#include <cstdlib>

//Constructor
Poisson::Poisson(int N,int dimension)
{
  N_ = N;

  dimension_ = dimension;

  if(dimension_==2){
    size_ = N_*N_;
    nnz_ = (3*N_-2)*N_+2*N_*(N_-1);
  }
  else if(dimension_==3){
    size_ = N_*N_*N_;
    nnz_ = 7*N_*N_*N_-6*N_*N_;
  }
  else{
    std::cout<<"Illegal number of dimension!"<<std::endl;
    assert(0);
  }

  //coefficients on the vericle edges and horizontal edges
  std::vector<double> vertCoef;
  std::vector<double> horiCoef;
  double interior=1e-5,exterior=1.;
  //1 for random and 2 for piecewise constant, other numbers for constant 1
  int coefficient_type = 2;
  int in_count=0,ex_count=0;
  std::srand(std::time(0));
  vertCoef.reserve(N_*(N_+1));
  horiCoef.reserve(N_*(N_+1));
  for(int j=0;j<N_+1;j++){
    for(int i=0;i< N_;i++){
      if(coefficient_type==1){
	vertCoef.push_back(1.*(std::rand())/(RAND_MAX));
      }
      else if (coefficient_type==2){
	double x = (i+1.)/(N_+1);
	double y = (j+.5)/(N_+1);
	if(x<.75 && x>.25 && y<.75 &&y >.25){
	  vertCoef.push_back(interior);
	  in_count++;
	  }
	else{
	  vertCoef.push_back(exterior);
	  ex_count++;
	}
    }
      else{
	vertCoef.push_back(1.);
      }
    //std::cout<<vertCoef.back()<<" ";
    }  
  }
  for(int j=0;j<N_;j++){
    for(int i=0;i< (N_+1);i++){
      if(coefficient_type==1){
	horiCoef.push_back(1.*(std::rand())/(RAND_MAX));
      }
      else if (coefficient_type==2){
	double x = (i+.5)/(N_+1);
	double y = (j+1.)/(N_+1);
	if(x<.75 && x>.25 && y<.75 &&y >.25){
	  horiCoef.push_back(interior);
	  in_count++;
	  }
	else{
	  horiCoef.push_back(exterior);
	  ex_count++;
	}

      }
      else{
	horiCoef.push_back(1.);
      }
    //std::cout<<horiCoef.back()<<" ";
    }
  }
    // std::cout<<std::endl;

    //std::cout<<"interior: "<<in_count<<", exterior: "<<ex_count<<std::endl;
  std::vector<TRIPLET> TripletList;
  std::vector<TRIPLETBOOL> TripletBoolList;
  
  TripletList.reserve(nnz_);
  TripletBoolList.reserve(nnz_);


  
  //mesh size
  double h = 1.0/N_;
  double diag= (dimension_==2 ? 4.0/h/h:6.0/h/h), off_diag = -1.0/h/h,row_ind,col_ind;
  double wCoef,eCoef,nCoef,sCoef;
  if(dimension_==2){
    for(int j=0;j<N_;j++){
      for(int i=0;i<N_;i++){
	row_ind = j*N_+i;
	col_ind = row_ind;
	wCoef = horiCoef[j*(N_+1)+i];
	eCoef = horiCoef[j*(N_+1)+i+1];
	nCoef = vertCoef[j*N_ + i];
	sCoef = vertCoef[(j+1)*N_+i];
	TripletList.push_back(TRIPLET(row_ind,col_ind,(wCoef+eCoef+nCoef+sCoef)*diag/4));
	//west
	if(i>0){
	  col_ind = j*N_+i-1;
	  TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag*wCoef));
	  TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	}
	//east
	if(i<N_-1){
	  col_ind = j*N_+i+1;
	  TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag*eCoef));
	  TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	}
	//north
	if(j<N_-1){
	  col_ind = (j+1)*N_+i;
	  TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag*sCoef));
	  TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	}
	//south
	if(j>0){
	  col_ind = (j-1)*N_+i;
	  TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag*nCoef));
	  TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	}
      }//end for j
    }//end for i

  }
  else{

    for(int i=0;i<N_;i++){
      for(int j=0;j<N_;j++){
	for(int k=0;k<N_;k++){
	  row_ind = (i*N_+j)*N_+k;
	  col_ind = row_ind;
	  TripletList.push_back(TRIPLET(row_ind,col_ind,diag));
	  //west
	  if(i>0){
	    col_ind = ((i-1)*N_+j)*N_+k;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }
	  //east
	  if(i<N_-1){
	    col_ind = ((i+1)*N_+j)*N_+k;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }
	  //north
	  if(j<N_-1){
	    col_ind = ((i)*N_+j+1)*N_+k;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }
	  //south
	  if(j>0){
	    col_ind = ((i)*N_+j-1)*N_+k;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }

	  //down
	  if(k>0){
	    col_ind = ((i)*N_+j)*N_+k-1;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }
	  //up
	  if(k<N_-1){
	    col_ind = ((i)*N_+j)*N_+k+1;
	    TripletList.push_back(TRIPLET(row_ind,col_ind,off_diag));
	    TripletBoolList.push_back(TRIPLETBOOL(row_ind,col_ind,true));
	  }
	}//end for k
      }//end for j
    }//end for i
  

  }
  //write matrix to file

  /*
  std::ofstream mfile;
  mfile.open("matrix.txt");
  mfile<<size_<<" "<<size_<<" "<<nnz_<<"\n";
  for(std::vector<TRIPLET>::iterator it=TripletList.begin(); it!=TripletList.end(); ++it ){
    mfile<<it->row()<<" "<<it->col()<<" "<<it->value()<<std::endl;
  }
  mfile.close();
  */
  matrix_ = new spMat(size_,size_);
  symb_matrix_ = new spMatBool(size_,size_);
  matrix_ ->setFromTriplets(TripletList.begin(),TripletList.end());
  symb_matrix_ ->setFromTriplets(TripletBoolList.begin(),TripletBoolList.end());
  matrix_->makeCompressed();
  symb_matrix_->makeCompressed();

  //  std::cout<<*symb_matrix_<<std::endl;
  //allocate space for eigenvector

  eigenVector_ = new VectorXd(size_);
  rhs_ = new VectorXd(size_);
  *rhs_ = VectorXd::Constant(size_,1.0);

  coordinates_ = new VectorXd(dimension_);
  
}

double Poisson::eigenValue(int k)
{
  if(k>size_-1){
    std::cout<<"The requested eigenvalue is out of range!"<<std::endl;
    assert(0);
  }
  int p, q, s;
  if(dimension_==2){
    p=k/N_+1;
    q=k%N_+1;
  }
  else{
    s = k%N_+1;
    q = k/N_;
    p = q/N_+1;
    q = q%N_+1;
  }

  double sin1,sin2,sin3,h=1.0/N_;
  sin1 = sin(p*M_PI/2/(N_+1));
  
  sin2 = sin(q*M_PI/2/(N_+1));

  sin3 = sin(s*M_PI/2/(N_+1));
  if(dimension_==2){
    return 4*(sin1*sin1+sin2*sin2)/h/h;
  }
  else{
    return 4*(sin1*sin1+sin2*sin2+sin3*sin3)/h/h;
  }
}


// return the k-th smallest eigenValue
double Poisson::min_eigenValue(int k)
{
  assert(dimension_==2);
  if(k>size_-1){
    std::cout<<"The requested eigenvalue is out of range!"<<std::endl;
    assert(0);
  }
  int sum = 1;
  k+=1;
  while(k>sum){
    k-=sum;
    sum++;
  }
  

  int p=k,q=sum-k+1;
  double sin1,sin2,h=1.0/N_;
  sin1 = sin(p*M_PI/2/(N_+1));

  sin2 = sin(q*M_PI/2/(N_+1));

  return 4*(sin1*sin1+sin2*sin2)/h/h;
}

VectorXd* Poisson::eigenVector(int k)
{
  if(k>eigenVector_->rows()-1){
    std::cout<<"The requested eigenvector is out of range!"<<std::endl;
    assert(0);
  }

  if(eigenVector_->rows()!=size_){
    std::cout<<"The eigenvector is not correctedly set!"<<std::endl;
    assert(0);
  }


  int p, q, s;
  if(dimension_==2){
    p=k/N_+1;
    q=k%N_+1;
  }
  else{
    s = k%N_+1;
    q = k/N_;
    p = q/N_+1;
    q = q%N_+1;
  }

  VectorXd sin_i(N_),sin_j(N_),sin_k(N_);
  for(int k=0;k<N_;k++){
    sin_i[k] = sin(p*(k+1)*M_PI/(N_+1));
    sin_j[k] = sin(q*(k+1)*M_PI/(N_+1));
    sin_k[k] = sin(s*(k+1)*M_PI/(N_+1));
  }

  if(dimension_==2){
    for(int i=0;i<N_;i++){
      for(int j=0;j<N_;j++){
	(*eigenVector_)[i*N_+j] = sin_i[i]*sin_j[j];
      }
    }
  }
  else{
    for(int i=0;i<N_;i++){
      for(int j=0;j<N_;j++){
	for(int k=0;k<N_;k++){
	  (*eigenVector_)[(i*N_+j)*N_+k] = sin_i[i]*sin_j[j]*sin_k[k];
	}
      }
    }
  }
  return eigenVector_;
}


VectorXd* Poisson::min_eigenVector(int k)
{
  assert(dimension_==2);
  if(k>eigenVector_->rows()-1){
    std::cout<<"The requested eigenvector is out of range!"<<std::endl;
    assert(0);
  }

  if(eigenVector_->rows()!=size_){
    std::cout<<"The eigenvector is not correctedly set!"<<std::endl;
    assert(0);
  }

  int sum = 1;
  k+=1;
  while(k>sum){
    k-=sum;
    sum++;
  }


  int p=k,q=sum-k+1;

  int sin1,sin2;
  VectorXd sin_i(N_),sin_j(N_);
  for(int k=0;k<N_;k++){
    sin_i[k] = sin(p*(k+1)*M_PI/(N_+1));
    sin_j[k] = sin(q*(k+1)*M_PI/(N_+1));
  }

  for(int i=0;i<N_;i++){
    for(int j=0;j<N_;j++){
      (*eigenVector_)[i*N_+j] = sin_i[i]*sin_j[j];
    }
  }
  return eigenVector_;
}




VectorXd* Poisson::linear_x()
{
  assert(dimension_==2);
  for(int i=0;i<N_;i++){
    for(int j=0;j<N_;j++){
      (*eigenVector_)[i*N_+j] = 1.*(i+1)/(N_+1);
    }
  }
  return eigenVector_;
}


VectorXd* Poisson::linear_y()
{
  assert(dimension_==2);
  for(int i=0;i<N_;i++){
    for(int j=0;j<N_;j++){
      (*eigenVector_)[i*N_+j] = 1.*(j+1)/(N_+1);
    }
  }
  return eigenVector_;
}


// Given the index of unknown, return the coordinates of the location in physical domain [0,1]^d
VectorXd* Poisson::coordinates(int k)
{

  int p, q, s;

  if(dimension_==2){
  
    p=k/N_+1;
    q=k%N_+1;
    (*coordinates_)[0] = 1.*p/(N_+1);
    (*coordinates_)[1] = 1.*q/(N_+1);
    
  }
  else{
    s = k%N_+1;
    q = k/N_;
    p = q/N_+1;
    q = q%N_+1;
    (*coordinates_)[0] = 1.*p/(N_+1);
    (*coordinates_)[1] = 1.*q/(N_+1);
    (*coordinates_)[2] = 1.*s/(N_+1);

  }
  return coordinates_;

}

// Given a list of unknown, partition them into two groups with similar sizes--bisect in the dimension with maximum span. e.g. [0,2]*[0,1]-> [0,1]*[0,1]+[1,2]*[0,1]
void Poisson::partition(VectorXi *group)
{
  //assert(dimension_==2);
  VectorXd span = VectorXd::Zero(dimension_);
  VectorXd max = VectorXd::Zero(dimension_);
  VectorXd min = VectorXd::Constant(dimension_,1.0);
  VectorXd *coord;
  for(int i=0;i<group->size();i++){
    coord = coordinates((*group)(i));
    for(int j = 0;j < dimension_;j++){
      if((*coord)(j) > max(j)){
	max(j) = (*coord)(j);
      }
      if((*coord)(j) < min(j)){
	min(j) = (*coord)(j);
      }
    }
  }
  double max_span = 0.0;
  int max_dim = -1;
  for(int j = 0; j < dimension_; j++){
    double temp  = (max(j)>=min(j) ? max(j)-min(j) : 0);
    if(temp > max_span){
      max_span = temp;
      max_dim = j;
    }    
  }
  double pivot = min(max_dim)+max_span/2;
  for(int i = 0; i < group->size(); i++){
    coord = coordinates((*group)(i));
    if((*coord)(max_dim) >pivot){
      (*group)(i) = 1;
    }
    else{
      (*group)(i) = 0;
    }
  }

}
