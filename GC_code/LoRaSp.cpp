#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "params.h"
#include "node.h"
#include "redNode.h"
#include "blackNode.h"
#include "superNode.h"
#include "tree.h"
#include "edge.h"
#include "time.h"
#include "gmres.h"
#include "Eigen/IterativeLinearSolvers"
#include "Eigen/Sparse"
#include "Eigen/Core"
#include "eyePC.h"
#include "diagPC.h"
#include "precondMat.h"
#include "Mat.h"
#include "Poisson.h"
#define HAS_ARPACK 1  //Specifying if ARPACK is avalable
#define USE_POISSON 1 //Specifying if we use stanford finite difference discretization of Poisson equation
#if HAS_ARPACK
#include "areig.h"
#include "nsymsol.h"
#include "arlspdef.h"
#include "arsnsym.h"
#endif

int main(int argc, char *argv[])
{

  params *PARAM;
  if ( (argc != 2) && (argc !=16 ) )
    {
      std::cout<<"Please provide input files. e.g, ./LoRaSp param.in"<<std::endl;
      std::cout<<"Or provide parameters directly. e.g, ./LoRaSp voro4k/A.mtx voro4k/b.mtx 8 1e-2 1e-2 0 0"<<std::endl;
      std::cout<<"parameters are: path-to-matrix-file path-to-RHS-file tree-depth lowRankMeth cutOffMeth epsilon aPrioriRank rankCapFactor deployFactor gmresMaxIters gmresEpsilon gmresPC ILUDropTol ILUFill normCols"<<std::endl;
      exit(1);
    }
  else if ( argc == 2)
    {
      //! Creating the param object from params.in
      PARAM = new params( argv[1] );
    }
  else
    {
      //! Creating the param object from provided parameters
      PARAM = new params( argv[1], atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atof(argv[6]), atoi(argv[7]), atof(argv[8]), atof(argv[9]), atoi(argv[10]), atof(argv[11]), atoi(argv[12]),atoi(argv[13]), atof(argv[14]), atoi(argv[15]), atoi(argv[16]) );
    }
  
  clock_t start, finish;
  start = clock();
  //! Creating the FMM tree with its root-node

  Poisson problem(PARAM->Poisson_Size(),2);
#if USE_POISSON
    tree TREE(PARAM,&problem);
#else
    tree TREE(PARAM);
#endif
  finish = clock();
  std::cout<<" Creating FMM tree time = "<<double(finish-start)/CLOCKS_PER_SEC<<std::endl;
  TREE.assembleTime = double(finish-start)/CLOCKS_PER_SEC;
 
  start = clock();
  //! Create the columns to leaves map
  TREE.createCol2LeafMap();
  finish = clock();
  std::cout<<" Creating columns to leaves Map time = "<<double(finish-start)/CLOCKS_PER_SEC<<std::endl;

  start = clock();
  //! Create the Adjacency list for redNodes
  TREE.createAdjList();
  finish = clock();
  std::cout<<" Creating adjacency lists time = "<<double(finish-start)/CLOCKS_PER_SEC<<std::endl;

  start = clock();
  //! Create the edges between leaves of the tree
  TREE.createLeafEdges();
  finish = clock();
  std::cout<<" Creating Leaf Edges time = "<<double(finish-start)/CLOCKS_PER_SEC<<std::endl;

  start = clock();
  //! Dedicate memory, and set the RHS (and VARs) for the leaf nodes
  TREE.setLeafRHSVAR();
  finish = clock();
  std::cout<<" Set VAR for leaves time = "<<double(finish-start)/CLOCKS_PER_SEC<<std::endl;

  // Create a random solution and RHS
  VectorXd x = VectorXd::Random( TREE.n() );
  VectorXd EigenVec = VectorXd::Zero(TREE.n());
  VectorXd AddVec = VectorXd::Zero(TREE.n());
  VectorXd b = ( *( TREE.Matrix() ) ) * x;
  // Initial guess vector
  VectorXd x0 = VectorXd::Zero( TREE.n() );
  VectorXd e = VectorXd::Zero( TREE.n() );
  VectorXd r = VectorXd::Zero( TREE.n() );
  std::cout<<" Artificial RHS and Solution created \n";

  switch ( PARAM->gmresPC() )
    {
    case 1: // i.e., Diag
      {
	start = clock();
	diagPC DIAGPC( TREE.Matrix() ) ;
	finish = clock();
	TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
	gmres<diagPC> GMRES( TREE.Matrix(), &DIAGPC, &b, &x0, PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
	GMRES.solve( );
	
	x0 = x - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/x.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );
	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	break;
      }
    case 2: // i.e., ILU
      {
	start = clock();
	Eigen::IncompleteLUT<double> ILU( *(TREE.Matrix() ), PARAM->ILUDropTol(), PARAM->ILUFill() );
	finish = clock();
	TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
	gmres<Eigen::IncompleteLUT<double> > GMRES( TREE.Matrix(), &ILU, &b, &x0, PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
	GMRES.solve( );
	
	x0 = x - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/x.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );
	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	break;
      }
    case 3: // i.e., H2
      {
	start = clock();
	TREE.factorize();
	finish = clock();
	TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
		
	////////  H2 iteration /////////
	/*
	while ( residu > 1e-14 )
	  {
	    std::cout<<"residual = "<<residu<<std::endl;
	    std::cout<<"accuracy = "<<acc<<"\n\n";
	    r = b - ( *( TREE.Matrix() ) ) * x0;
	    e = TREE.solve( r );
	    x0 += e;
	    residu = r.norm() / norm_b;
	    acc = (x - x0).norm() / norm_x;
	  }
	 std::cout<<"residual = "<<residu<<std::endl;
	 std::cout<<"accuracy = "<<acc<<"\n\n";
	 exit(0);
	*/	
	///////////No GMRES ////////////////
	/*
	b = VectorXd::Zero( TREE.n() );
	VectorXd xp = TREE.solve( b );
	std::cout<<" accuracy = "<<xp.norm()<<"\n";
	exit(0);
	*/
	b = *TREE.RHS();
	gmres<tree> GMRES( TREE.Matrix(), &TREE, &b, &x0, PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
	GMRES.solve( );
	
	x0 = x - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/x.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );
	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	x0 = *(GMRES.retVal());
	break;
      }
    case 4:{ // stationary iterations
      start = clock();
      TREE.factorize();
      finish = clock();
      TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
      int iter = PARAM->gmresMaxIters();
      b = *TREE.RHS();
      double res_pre=b.norm();
      double accum_ratio=1;
      double ratio = 1;
      for(int i=0;i<iter;i++){
        e = TREE.solve(b);
        x0 = x0 + e;
        b = (*TREE.RHS())-(*TREE.Matrix())*x0;
	
	ratio = b.norm()/res_pre;
	std::cout<<"residual at"<<i<<"th step: "<<b.norm()<<"ratio: "<<ratio<<std::endl;
	  accum_ratio*=ratio;
	res_pre = b.norm();
      }
      
      std::cout<<"average convergence rate: "<< std::pow(accum_ratio,1.0/(iter))<<std::endl;
      //      x0 = TREE.solve();                                     
      r = (*TREE.RHS())-(*TREE.Matrix())*x0;
      TREE.residual = r.norm()/(*TREE.RHS()).norm();
      //std::cout<< "r norm="<< r.norm()<<" rhs norm= "<<(*TREE.RHS()).norm()<<" relative= "<<TREE.residual<< "matrix frob= "<< (*TREE.Matrix()).norm()<<std::endl;
      break;
    }
    case 5:{ //apply preconditioner as a stationary solver to test the convergence on certain eigenvectors                  
      std::cout<<"Eigenvalue test\n";

      int iter = PARAM->gmresMaxIters();


	int ncol,num_v=1;
	int target_vec = 0;// vector of choice

	// read eigenvectors from file
	//      std::ifstream addvec_file("addvec_eigenvector.dat");  
      // 	addvec_file>> ncol>>num_v;
      //      for(int i=0;i<num_v;i++){
	//	  std::cout<<"size of it:" <<EigenVec.rows()<<" , "<<EigenVec.cols()<< "ncol is "<<ncol<<std::endl;
	  
	//	for(int j=0; j<ncol; j++){
	//    addvec_file>>AddVec(j);
	//}
	  //permute the eigenvector to the current ordering

	  //if(i==target_vec){
	    //  TREE.permuteSol(&AddVec);
	    // we force it to be constant
	    //AddVec = VectorXd::Constant(AddVec.size(),1.0);
	
	    AddVec = VectorXd::Random( AddVec.size() );
	    densMat AddBlock = densMat::Random(AddVec.size(),5);
	    densMat IterateBuffer = densMat::Zero(AddVec.size(),AddBlock.cols());
	    EigenVec = VectorXd::Random( EigenVec.size() );
	    for(int i=0;i<5;i++){
	      AddVec = AddBlock.col(i);
	      TREE.setLeafADDVEC(AddVec);
	    }
	    start = clock();
	    TREE.factorize();
	    finish = clock();
	    TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
	    
	    MatrixXd reduction = MatrixXd::Zero(iter,num_v);
	//choose which eigen vec to use

	    for(int i=0;i<num_v;i++){
	  //	  std::cout<<"for "<<i<< "th eigenvector"<<std::endl;
	  // repeat the solving process for each eigenvector
	  //std::cout<<"size of it:" <<EigenVec.rows()<<" , "<<EigenVec.cols()<< "ncol is "<<ncol<<std::endl;
	  
	  //for(int j=0; j<ncol; j++){
	  //      eigen_file>>EigenVec(j);
	  //   }
	  //permute the eigenvector to the current ordering
	  //TREE.permuteSol(&EigenVec);
	  EigenVec = AddVec; 
	  int k=0;
	  double err=1.0;

	  //energy norm
	  //b = (*TREE.Matrix())*EigenVec;
	  //double norm_ev=std::sqrt(b.adjoint()*EigenVec);
	  double norm_ev=AddBlock.norm();	        double norm_start = norm_ev;
	  double norm_ev_new;
	  while(k<iter&&err>1e-12*norm_start){
	    for(int i=0;i<AddBlock.cols();i++){
	      b = (*TREE.Matrix())*AddBlock.col(i);
	      IterateBuffer.col(i) = TREE.solve(b);
	    }
	    	  
	    IterateBuffer = IterateBuffer-AddBlock;
	    norm_ev_new = IterateBuffer.norm();
	    AddBlock = IterateBuffer;

	    if(norm_ev>1e-15){
	      reduction(k,i)=norm_ev_new/norm_ev;
	      norm_ev=norm_ev_new;
	    }
	    k++;
	  }
	  std::cout<<"num of iteration: "<<k<<std::endl;
	  
	}
	
	//eigen_file.close();
	std::cout<<reduction<<std::endl;
	b = *TREE.RHS();
	double res_pre=b.norm();
	double accum_ratio=1;
	double ratio = 1;
      /*
      for(int i=0;i<iter;i++){
        e = TREE.solve(b);
        x0 = x0 + e;
        b = (*TREE.RHS())-(*TREE.Matrix())*x0;
	
	ratio = b.norm()/res_pre;
	std::cout<<"residual at"<<i<<"th step: "<<b.norm()<<"ratio: "<<ratio<<std::endl;
	  accum_ratio*=ratio;
	res_pre = b.norm();
      }
      */
      std::cout<<"average convergence rate: "<< std::pow(accum_ratio,1.0/(iter))<<std::endl;
      //      x0 = TREE.solve();                                     
      r = (*TREE.RHS())-(*TREE.Matrix())*x0;
      TREE.residual = r.norm()/(*TREE.RHS()).norm();
      //std::cout<< "r norm="<< r.norm()<<" rhs norm= "<<(*TREE.RHS()).norm()<<" relative= "<<TREE.residual<< "matrix frob= "<< (*TREE.Matrix()).norm()<<std::endl;
      break;
    }

    case 6:{
      #if HAS_ARPACK
      // we use this case to test H-solver with added evectors from either arpack++ or files.

      Mat matA(&TREE);

      // Read eigenvectors from files:
      //std::ifstream matrix_file("../IFMM/ifem/struct2p4_2/evecs.txt");
      //std::ifstream matrix_file("/Volumes/Storage1/evecs.txt");

      
      int nconv=0,neigs= PARAM->neigs(), nsize,nev;
      VectorXd *residual = new VectorXd(TREE.n());
      start = clock();
      std::cout<<"Solving for a few eigenvectors using arpack..."<<std::endl;
      ARNonSymStdEig<double,Mat> Aprob(matA.n(),8,&matA, &Mat::MultMv,"SM",0,0.001,100000,NULL,true);
     
      nconv = Aprob.FindEigenvectors();
      finish = clock();
      std::cout<<"Time cost for find "<< nconv<<" eigenvector: "<< double(finish-start)/CLOCKS_PER_SEC<<std::endl;

      if(nconv==0){
	//	if(0){
	  std::cout<<"no eigenvector converged!"<<std::endl;
	  exit(0);
	}
	else{

	  for(int i=0;i<TREE.n();i++){
	    AddVec(i) = Aprob.EigenvectorImag(nconv-1,i);
	  }
	  if(AddVec.norm()>1e-14){
	    std::cout<<"Eigenvector is not real!"<<std::endl;
	    exit(0);
	  }
	  
	  // Read evectors from files
	  //matrix_file>> nsize>>nev;
	  //assert(nsize==TREE.n());
	  //assert(nev>neigs);

	  for(int j=0;j<neigs;j++){
	    for(int i=0;i<TREE.n();i++){
	      // matrix_file>>AddVec(i);// From file
	      AddVec(i) = Aprob.EigenvectorReal(nconv-1-j,i); //From arpack
	      //AddVec(i) = 1.0; //constant
	    }

	    TREE.permuteSol(&AddVec);
	    TREE.setLeafADDVEC(AddVec);
	  }

	}
      //      matrix_file.close();
      start = clock();
      TREE.factorize();
      finish = clock();
      TREE.dumpPermuteVector();
      TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
      //here we manufacture a solution
      VectorXd sol = VectorXd::Constant(TREE.n(), 1.0);
      sol += VectorXd::Random(TREE.n());
      b = (*TREE.Matrix())*sol;
      
      //b = *TREE.RHS();
      //      matrix_file.open("../IFMM/ifem/struct2p4_2/evecs.txt",std::ifstream::in);
      //matrix_file.open("/Volumes/Storage1/evecs.txt",std::ifstream::in);
      //matrix_file>>nsize>>nev;
      //assert(nsize==TREE.n());
      //assert(nev>neigs);
      //            nev = 20;


      // Stationary iteration
      int iter=0;
      double error = sol.norm(), last_error  = 0.0;
      start = clock();
      while(iter<500&&error>PARAM->gmresEpsilon()*sol.norm()){
	AddVec =  b-(*TREE.Matrix())*x;
	AddVec = TREE.solve(AddVec);
	x += AddVec;
	AddVec = x-sol;
	//b = AddVec - b;
	//(*residual)(j) = AddVec.norm()/last_error;
	error = AddVec.norm();
	std::cout<<iter<<"\t Relative Error:"<<error/sol.norm()<<", relative error reduction: "<<error/last_error<<std::endl;
	last_error = error;
	iter++;
       }
      finish = clock();
      //TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
  
      //Here we do a regular GMRES solve
      gmres<tree> GMRES( TREE.Matrix(), &TREE, &b, &x0,PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
//	GMRES.solve();
	
	x0 = sol - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/sol.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();


	x0 = *(GMRES.retVal())-sol;
	x0 = (*TREE.Matrix())*(*(GMRES.retVal())-sol);
	TREE.Accuracy = x0.dot((*TREE.Matrix())*x0);
	TREE.Accuracy /= sol.dot(b);
	TREE.Accuracy = std::sqrt(TREE.Accuracy);
	
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );
	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	x0 = *(GMRES.retVal());
	std::cout<<"done with the test"<<std::endl;
	break;  
	#else

	std::cout<<"Case 6 is not available if ARPACK is not supported!"<<std::endl;
	
        #endif

    }


    case 7: // i.e., H2+GS                                                    
      {
        start = clock();
	AddVec = VectorXd::Constant( AddVec.size(),1.0 );       
	TREE.setLeafADDVEC(AddVec);
	//AddVec = VectorXd::Random( AddVec.size());       
	//TREE.setLeafADDVEC(AddVec);
        TREE.factorize();
        finish = clock();
        TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;

	// a quick test if it preserves constant
	VectorXd cons = VectorXd::Constant(TREE.n(),1.0);
	double norm_cons = cons.norm();
	b = (*TREE.Matrix())*cons;
	x0 = TREE.solve(b);
	cons -= x0;
	std::cout<<"Residual norm in test: "<<cons.norm()/norm_cons<<std::endl;
	
	x0 = VectorXd::Zero(TREE.n());
	b = *TREE.RHS();
        gmres<tree> GMRES( TREE.Matrix(), &TREE, &b, &x0, PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
        GMRES.solve( );

        x0 = x - *(GMRES.retVal());
        TREE.accuracy = x0.norm()/x.norm();
        x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
        TREE.residual = x0.norm()/b.norm();
        TREE.residuals = new VectorXd( GMRES.totalIters() );
        (*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters\
							   () );
        TREE.gmresTotalTime = GMRES.totalTime();
        TREE.gmresTotalIters = GMRES.totalIters();
        x0 = *(GMRES.retVal());



	break;
      }
   case 8:{
     //we solve the 2D Poisson problem with generated eigenvalues and eigenvectors
     //we have different approaches to obtain eigenvectors/values:
     //1. read from file
     //2. solve using Arpack
     //3. explicit form (2D Poisson)
     //4. smoothed by Gauss-Seidel
     int nconv=0,neigs= PARAM->neigs(), nsize,nev=TREE.n();
     nev = neigs+1;
     VectorXd *residual = new VectorXd(TREE.n());
     double max_error=0.0;


     /*
     //We make 6 eigen low frequency vector by GS smoothing
     densMat vecs = densMat(TREE.n(),neigs), Q;
     vecs.setRandom();
     densMat bs = densMat(TREE.n(),neigs);

     std::cout<<"Gauss-Seidel smoothing..."<<std::endl;
     start = clock();
     for(int count = 0; count<500; count++){
       bs = (*TREE.Matrix())*vecs;
       bs = (*TREE.Matrix()).transpose().triangularView<Eigen::Upper>().solve(bs);                          
       vecs = vecs-bs;
       /*
       for(int i = 0;i < neigs; i++){
	 for(int j = 0;j<i;j++){
	   vecs.col(i) = vecs.col(i) - (vecs.col(i)).dot(vecs.col(j))*vecs.col(j);
	 }
	 vecs.col(i) = vecs.col(i)/(vecs.col(i)).norm();
       }
       
     }
     finish = clock();
     std::cout<<"Gauss-Seidel smoothing time:"<< double(finish-start)/CLOCKS_PER_SEC<<" seconds"<<std::endl;

     std::cout<<"QR orthogonalization..."<<std::endl;
     start = clock();
     //Eigen::HouseholderQR<densMat> qr((*TREE.Matrix()));
     //Q = qr.householderQ();
     //     vecs = Q.block(0,0,TREE.n(),neigs);     

     for(int i = 0;i < neigs; i++){
       for(int j = 0;j<i;j++){
	 vecs.col(i) = vecs.col(i) - (vecs.col(i)).dot(vecs.col(j))*vecs.col(j); 
       }
       vecs.col(i) = vecs.col(i)/(vecs.col(i)).norm();
     }


     finish = clock();
     std::cout<<"QR time:"<< double(finish-start)/CLOCKS_PER_SEC<<" seconds";

     
*/      

     /*
      //verify the eigenvalues and eigenvectors
      for(int i=0;i<TREE.n();i++){
	AddVec = *(problem.eigenVector(i));
	TREE.permuteSol(&AddVec);
	b = (*TREE.Matrix())*AddVec;
	b = b-problem.eigenValue(i)*AddVec;
	//	std::cout<<i<<": Relative error: "<<b.norm()/AddVec.norm()<<std::endl;
	if (b.norm()/AddVec.norm()/problem.eigenValue(i)>max_error){
	  max_error =b.norm()/AddVec.norm()/problem.eigenValue(i);
	}
      }
      std::cout<<"Max error: "<< max_error<<std::endl;
      //      */
      
     nev=0;
      //add vectors
     neigs = -1;
     
      for(int i=0;i<neigs;i++){
	
	AddVec = *(problem.min_eigenVector(i));///problem.min_eigenValue(i);
	//		AddVec = VectorXd::Constant(TREE.n(), 1.0);
	// change the flag in blackNode.cpp for piecewise constant on red nodes
		
	TREE.permuteSol(&AddVec);
	TREE.setLeafADDVEC(AddVec);	 
      }

      // add linear functions
      
           AddVec = VectorXd::Constant(TREE.n(), 1.0);
      TREE.permuteSol(&AddVec);
      TREE.setLeafADDVEC(AddVec);
      /*
      AddVec = *(problem.linear_x());///problem.min_eigenValue(i);
      TREE.permuteSol(&AddVec);
      TREE.setLeafADDVEC(AddVec);
      AddVec = *(problem.linear_y());///problem.min_eigenValue(i);
      TREE.permuteSol(&AddVec);
      TREE.setLeafADDVEC(AddVec);
      */

      start = clock();
      TREE.factorize();
      finish = clock();
      std::cout<<"Factorization finished"<<std::endl;
      //TREE.dumpPermuteVector();
      TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
      b = *TREE.RHS();
      

      
      int progress = 10;
      double max_residual=0.0,last_error=1.0,error;
      
      VectorXd sol = VectorXd::Constant(TREE.n(), 0.0);
      sol.setRandom();
      VectorXd x = VectorXd::Constant(TREE.n(),0.0);


      sol += VectorXd::Constant(TREE.n(),1.0);
      error = sol.norm();
      last_error = sol.norm();
      b = (*TREE.Matrix())*sol;
      double maximum=0.0;
      /*
      //test the error reduction on all eigenvectors
      
      nev = TREE.n();
      
      for(int j=0;j<nev;j++){
	
	if(j*100.0/nev>progress){
	  std::cout<<progress<<"% "<<std::endl;
	  progress+=10;
	}
	AddVec = *(problem.eigenVector(j));	  
	TREE.permuteSol(&AddVec);
	b = (*TREE.Matrix())*AddVec;
	x = TREE.solve(b);
	b = AddVec-x;
	(*residual)(j) = b.norm()/AddVec.norm();
	//std::cout<<"reduction: "<<(*residual)(j)<<", evalue: "<<problem.eigenValue(j*100)<<std::endl;
	//maximum = std::max(maximum, (*residual)(j));
	
      }


      std::ofstream evecs_file,eval_file;
	evecs_file.open("output.txt",std::ofstream::out);
	eval_file.open("evals.txt",std::ofstream::out);

	evecs_file<<nev<<std::endl;
	
	
	  for(int i=0;i<nev;i++){
	    evecs_file<<(*residual)(i)<<std::endl;
	    eval_file<<problem.eigenValue(i)<<std::endl;
	  }
	  evecs_file.close();
	std::cout<<std::endl;
      //*/
      //std::cout<<"finish computation, maximum is "<<maximum<<std::endl;


      // test the convergence on random vectors with stationary iterations
      /*
            int iter=0;
      start = clock();
      while(iter<500&&error>PARAM->gmresEpsilon()*sol.norm()){
	AddVec =  b-(*TREE.Matrix())*x;
	AddVec = TREE.solve(AddVec);
	x += AddVec;
	AddVec = x-sol;
	//b = AddVec - b;
	//(*residual)(j) = AddVec.norm()/last_error;
	error = AddVec.norm();
	std::cout<<iter<<"\t Relative Error:"<<error/sol.norm()<<", relative error reduction: "<<error/last_error<<std::endl;
	last_error = error;
	iter++;
       }
      finish = clock();
      
	 TREE.gmresTotalTime = double(finish-start)/CLOCKS_PER_SEC;
         TREE.gmresTotalIters = iter;
      //std::cout<<"Solve time for each iteration: "<<double(finish-start)/CLOCKS_PER_SEC/iter<<std::endl;
      //std::cout<<"Error:"<<error<<std::endl;
      //*/
	  //if((*residual)(j)>max_residual){
	  //max_residual = (*residual)(j);
	  
	  //} 
	//}
	//std::cout<<"Maximum residual: "<<max_residual<<std::endl;

	//	matrix_file.close();


      //Solve using GMRES

      gmres<tree> GMRES( TREE.Matrix(), &TREE, &b, &x0,PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
      		GMRES.solve( );
	
	x0 = sol - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/sol.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );

	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	
	x0 = *(GMRES.retVal());
	//break;
	//*/
      	


	break;  
    }

     
   case 9:{
     //Given 2D Poisson problem, evalues and evectors are all available
     //We need USE_POISSON=1 for this case
     //we added a few eigenvectors solve the 2D Poisson problem with generated eigenvalues and eigenvectors
     // then we test the error reduction rate of GC on all eigenvectors.
     // this is the first test in paper h-solver 2016
     std::cout<<"Case 9: first test in paper h-solver 2016, error reduction rate on all eigenvectors is tested"<<std::endl;
     int nconv=0,neigs= PARAM->neigs(), nsize,nev=TREE.n();
     nev = neigs+1;
     VectorXd *residual = new VectorXd(TREE.n());
      
      nev = TREE.n();
     //nev=20;
      //add eigenvectors

      for(int i=0;i<neigs;i++){
	AddVec = *(problem.min_eigenVector(i))/problem.min_eigenValue(i);
	TREE.permuteSol(&AddVec);
	TREE.setLeafADDVEC(AddVec);	 
      }

      start = clock();
      TREE.factorize();
      finish = clock();
      std::cout<<"Factorization finished"<<std::endl;
      
      TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
      b = *TREE.RHS();
      
	//here we just test the reduction of the first step
      
      int progress = 10;
      double max_residual=0.0,last_error=1.0,error;


      double maximum=0.0;

      for(int j=0;j<nev;j++){
	// we applied stationary solver on all eigenvectors
	if(j*100.0/nev>progress){
	  //  std::cout<<progress<<"% "<<std::endl;
	  progress+=10;
	}
	AddVec = *(problem.eigenVector(j));	  
	TREE.permuteSol(&AddVec);
	b = (*TREE.Matrix())*AddVec;
	x = TREE.solve(b);
	b = AddVec-x;
	(*residual)(j) = b.norm()/AddVec.norm();
      }

      std::ofstream evecs_file,eval_file;
      evecs_file.open("output.txt",std::ofstream::out);
      eval_file.open("evals.txt",std::ofstream::out);
      
      evecs_file<<nev<<std::endl;	
      for(int i=0;i<nev;i++){
	evecs_file<<(*residual)(i)<<std::endl;
	eval_file<<problem.eigenValue(i)<<std::endl;
      }
      evecs_file.close();
      std::cout<<std::endl;
      break;  
    }

    case 10:{
      start = clock();
      TREE.factorize();
      finish = clock();
      TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
      VectorXd basis,temp;
      std::ofstream precondMat("precondMat.dat",std::ofstream::out);
      precondMat << TREE.n()<<'\n'; 
      for(int i=0; i< TREE.n();i++){
	  basis = VectorXd::Zero(TREE.n());
	  basis(i) = 1.0;
	  temp = (*TREE.Matrix())*basis;
	  basis = TREE.solve(temp);
	  precondMat << basis<<'\n';
	}
	    precondMat.close();
      break;
    }
    default: // i.e., Identity
      {
	start = clock();
	eyePC EYEPC;
	finish = clock();
	TREE.precondFactTime = double(finish-start)/CLOCKS_PER_SEC;
	gmres<eyePC> GMRES( TREE.Matrix(), &EYEPC, &b, &x0, PARAM->gmresMaxIters(), PARAM->gmresEpsilon(), PARAM->gmresVerbose() );
	GMRES.solve( );
	
	x0 = x - *(GMRES.retVal());
	TREE.accuracy = x0.norm()/x.norm();
	x0 = b - (*TREE.Matrix()) * (*GMRES.retVal());
	TREE.residual = x0.norm()/b.norm();
	TREE.residuals = new VectorXd( GMRES.totalIters() );
	(*TREE.residuals) =  GMRES.residuals() -> segment( 0 , GMRES.totalIters() );
	TREE.gmresTotalTime = GMRES.totalTime();
	TREE.gmresTotalIters = GMRES.totalIters();
	break;
      }

    }

  TREE.permuteSolBack(&x0);
  TREE.permuteSolBack(TREE.RHS());
  std::ofstream output;
  output.precision(18);
  output.open( "sol.mtx", std::fstream::out);
  output<<TREE.n()<<"\n";
  for(int i=0;i<TREE.n();i++)
    output<<x0(i)<<"\n";
  output.close();

  //      std::ofstream output;                                              
  output.open( "rhs.mtx", std::fstream::out);
  output<<TREE.n()<<"\n";


  for(int i=0;i<TREE.n();i++)
    output<<(*TREE.RHS())(i)<<"\n";
 
  output.close();
  std::cout<<"\n*********GMRES CONVERGED***********\n\n";
  std::cout<<"Factorization TIME  = "<<TREE.precondFactTime<<std::endl;
  std::cout<<"GMRES TIME  = "<<TREE.gmresTotalTime<<std::endl;
  std::cout<<"GMRES NUM ITERS  = "<<TREE.gmresTotalIters<<std::endl;
  std::cout<<"GMRES RESIDUAL = "<<TREE.residual<<std::endl;
  std::cout<<"GMRES ACCURACY = "<<TREE.accuracy<<"\n";
  std::cout<<"GMRES ACCURACY in A norm = "<<TREE.Accuracy<<"\n\n\n";
 
  
  //TREE.log("log.txt");
}
