#include "redNode.h"
#include <iostream>
#include "blackNode.h"
#include "edge.h"
#include "hparams.h"
#include "htree.h"
// constructor for redNode
RedNode::RedNode(BlackNode* P, HTree* T, bool W, int first, int last)
    : Node((Node*)P, T, first, last) {
  // Set its parent
  parent_ = P;

  // Set if it is left or right child
  which_ = W;

  // Set its child to Null (later, an actual blacknode may be assigned)
  child_ = NULL;

  // Set its level
  if (P == NULL) {
    level_ = 0;
  } else {
    level_ = P->parent()->level() + 1;
  }

  // When a redNode born, it should be added to the redNode list of the tree
  T->addRedNode(level_, this);

  n(last - first + 1);

  // Initially not eliminated
  eliminated_ = false;

  // Access to HTree parameters
  parameters_ = T->parameters();
}

// Destructor for redNode: delete its blackChild (if any)
RedNode::~RedNode() {
  if (child_ != NULL) {
    delete child_;
  }

  // Delete interaction edges
  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (parameters_->symmetric()) {
      if (it->first != this) {
        it->first->outEdges.erase(this);
      }
    }
    delete it->second;
  }

  if (parameters_->wellSepDistance() != 1) {
    delete generalAdjSet_;
  }
}

// Continue creating the tree (Red -> Black)
void RedNode::createBlackNode() {
  child_ = new BlackNode(this, tree(), indexFirst(), indexLast());
}

void RedNode::formGeneralAdjSet() {
  if (parameters_->wellSepDistance() == 1) {
    generalAdjSet_ = &adjSet_;
    return;
  }

  generalAdjSet_ = new std::set<RedNode*>;
  generalAdjSet_->insert(this);

  if (parameters_->wellSepDistance() == 0) {
    return;
  }

  for (int i = 0; i < parameters_->wellSepDistance(); i++) {
    std::set<RedNode*> toBeAdded;
    for (auto it_j = generalAdjSet_->begin(); it_j != generalAdjSet_->end();
         ++it_j) {
      RedNode* neighb = (*it_j);
      for (auto it_k = neighb->adjSet()->begin();
           it_k != neighb->adjSet()->end(); ++it_k) {
        toBeAdded.insert(*it_k);
      }
    }
    for (auto it = toBeAdded.begin(); it != toBeAdded.end(); ++it) {
      generalAdjSet_->insert(*it);
    }
  }
}
