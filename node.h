#ifndef node_h
#define node_h

class HTree;
class RedNode;
class BlackNode;
class SuperNode;
class Edge;
class Node;
class Hparams;

#include <map>
#include <vector>
#include "Eigen/Dense"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a dynamic size dense matrix used to store the inverse of pivot */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! Define key value pair for edge map */
typedef std::pair<Node*, Edge*> EdgePair;

/**************************************************************************/
/*                           CLASS NODE (Abstract)                        */
/**************************************************************************/

//! Class node
/*!
  This (virtual) class is an interface for the three inherited classes
  blackNode, redNode, and superNode
*/

class Node {
  //! Pointer to the tree that this node belongs to
  HTree* tree_ptr_;

  //! Pointer to the parent node
  Node* parent_;

  //! Index of the first column in the matrix (inclusive) correspondig to this
  //! node
  int index_first_;

  //! Index of the last column in the matrix (inclusive) correspondig to this
  //! node
  int index_last_;

  //! Indicate number of variables in this node
  /*!
    Note that for a leaf redNode [n] is the number of columns/rows of the matrix
    corresponding to this node.
    However, for redNodes upper in the tree, [n] is determined by low rank
    approximation.
   */
  int n_;

  //! The right hand side vector
  VectorXd* RHS_;

  //! The vector of unknowns
  VectorXd* VAR_;

  //! Pointer to htree parameter class
  Hparams* hparams_;

 public:
  //! Default constructor
  Node(){};

  //! Constructor:
  /*!
    input arguments:
    pointer to parent, pointer to the tree, range of belonging rows/cols
  */
  //! Constructor by passing the ptr to the tree, and range of belonging columns
  Node(Node*, HTree*, int, int);

  //! Destructor
  virtual ~Node();

  //! Returns parent_
  Node* parent() const { return parent_; }

  //! Returns index_first_
  int indexFirst() const { return index_first_; }

  //! Returns index_last_
  int indexLast() const { return index_last_; }

  //! Returns tree_ptr_
  HTree* tree() const { return tree_ptr_; }

  //! True if this node contains no column of the matrix
  bool isEmpty() { return index_first_ > index_last_; }

  //! True if this node is eliminated
  bool isEliminated() const { return eliminated_; }

  //! Set the elimination flag to [true]
  void eliminate();

  //! Set the elimination flag to [false]
  void deEliminate();

  //! Returns node size
  int n() const { return n_; }

  //! Set node size
  void n(int val) { n_ = val; }

  //! Returns pointer to the redParent (pure virtual)
  /*!
    redNode -> returns itself
    blackNode -> returns parent()
    superNode -> returns parent() of paranet()
   */
  virtual RedNode* redParent() = 0;

  //! List of incoming edges (key: pointer to source node, value: edge pointer)
  std::map<Node*, Edge*> inEdges;

  //! List of outgoing edges (key: destination node ptr, value: edge ptr)
  /*!
  When symmetric option is activated, only outEdges map is used for all edges.
  In this case edges are not directed.
  We use Edge::isTransposed() function to identify if M or M.tranpose() should
  be used, where M is the matrix associated to an edge.
  */
  std::map<Node*, Edge*> outEdges;

  //! A boolean flag to keep track of elimination
  bool eliminated_;

  //! Erase removed edges from the list of incoming/outgoing edges
  void eraseCompressedEdges();

  //! Access to the pointer of the RHS vector
  VectorXd* RHS() { return RHS_; }

  //! Set the pointer of the RHS vector
  void RHS(VectorXd* in) { RHS_ = in; }

  //! Access to the pointer of the variables vector
  VectorXd* VAR() { return VAR_; }

  //! Set the pointer of the VAR vector
  void VAR(VectorXd* in) { VAR_ = in; }

  //! The inevrse of the selfEdge matrix (i.e., pivot)
  /*!
  In case of combined elimination invPivot contains the modified pivot (that
  corresponds to elimination of superNode and blackNode together)
  */
  densMat* invPivot = NULL;

  //! For combined elimination (only used by superNode)
  densMat* inInvPivot_ = NULL;
  densMat* outInvPivot_ = NULL;

  //! solve L z = b
  /*!
    This function updates RHS, which is solve for z in L z = b
    It uses the order of elimination.
   */
  void solveL();

  //! solve U VAR = RHS
  /*!
    Solve for unknowns of this cluster.
    It uses the order of elimination.
   */
  void solveU();

  //! A boolean flag to keep track of updated RHS
  bool rhsUpdated_;

  //! The order of elimination
  int order;

  //! Apply block Gauss elimination
  void gaussElimination();

  //! A helper function to create/update a new edge to the graph
  /*!
  source of the edge is the instance of node that this function is called from.
  first argument: pointer to edge's destination node
  second argument: reference to the matrix value of the edge

  If the edge already exist this function adds the matrix value to the existing
  matrix value.

  If calculations are symmetric, only outEdges are used.
  */
  void createUpdateEdge(Node* destination, densMat& mat);

  //! A helper function to create a new edge to the graph
  /*!
  source of the edge is the instance of node that this function is called from.
  first argument: pointer to edge's destination node
  second argument: reference to the matrix value of the edge

  If the edge already exist this function does nothing.

  If calculations are symmetric, only outEdges are used.
  */
  void createEdge(Node* destination, densMat& mat);
};

#endif
