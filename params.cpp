#include "params.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

Params::Params(int argc, char* argv[]) {
  if ((argc != 2) && (argc != 28)) {
    std::cout << "Incorrect input format." << std::endl;
    exit(1);
  } else if (argc == 2) {
    //! Creating the param object from params.in
    loadFromFile(argv[1]);
  } else {
    // Input data
    data_ = new DataLoader(argv[1], argv[2], argv[3]);

    // Other parameters
    solveMethod_ = (unsigned int)atoi(argv[4]);

    scaleMatrix_ = (unsigned int)atoi(argv[5]);
    preElimination_ = atoi(argv[6]);

    preconditioner_ = (unsigned int)atoi(argv[7]);
    maxIters_ = (unsigned int)atoi(argv[8]);
    iterationEpsilon_ = (double)atof(argv[9]);
    iterationVerbose_ = (bool)atoi(argv[10]);

    // H-solver paraeters
    hparams_ = new Hparams(
        (unsigned int)atoi(argv[11]), (unsigned int)atoi(argv[12]),
        (unsigned int)atoi(argv[13]), (unsigned int)atoi(argv[14]),
        (int)atoi(argv[15]), (bool)atoi(argv[16]), (bool)atoi(argv[17]),
        (unsigned int)atoi(argv[18]), (unsigned int)atoi(argv[19]),
        (double)atof(argv[20]), (unsigned int)atoi(argv[21]),
        (double)atof(argv[22]), (unsigned int)atoi(argv[23]),
        (double)atof(argv[24]), (int)atoi(argv[25]), (int)atoi(argv[26]));

    // ILU parameters
    ILUDropTol_ = (unsigned int)atoi(argv[27]);
    ILUFill_ = (unsigned int)atoi(argv[28]);
  }
}

void Params::loadFromFile(char* address) {
  std::ifstream param(address);
  std::ostringstream param_trimmed_out;
  std::string input_line;
  while (!param.eof()) {
    std::getline(param, input_line);
    if ((int(input_line[0]) != 0) && (int(input_line[0]) != 35))
      param_trimmed_out << input_line << std::endl;
  }
  param.close();

  std::istringstream param_trimmed_in(param_trimmed_out.str());

  // Input data
  std::string inputMatrixFile_;
  std::string inputRHSFile_;
  std::string inputSolutionFile_;

  param_trimmed_in >> inputMatrixFile_;
  param_trimmed_in >> inputRHSFile_;
  param_trimmed_in >> inputSolutionFile_;

  data_ = new DataLoader(inputMatrixFile_, inputRHSFile_, inputSolutionFile_);

  // Other parameters
  param_trimmed_in >> solveMethod_;

  param_trimmed_in >> scaleMatrix_;
  param_trimmed_in >> preElimination_;

  param_trimmed_in >> preconditioner_;
  param_trimmed_in >> maxIters_;
  param_trimmed_in >> iterationEpsilon_;
  param_trimmed_in >> iterationVerbose_;

  // H-solver parameters
  unsigned int treeDepth_;
  int wellSepDistance_;
  unsigned int solveLevel_;
  unsigned int symmetric_;
  int secondOrder_;
  bool combinedElimination_;
  bool unCompressedSolve_;
  unsigned int eliminationOrder_;
  unsigned int lowRankMethod_;
  double rSVDFactor_;
  int cutOffMethod_;
  double epsilon_;
  int aPrioriRank_;
  double rankCapFactor_;
  int smoothing_;
  int basisEnhancement_;

  param_trimmed_in >> treeDepth_;
  param_trimmed_in >> wellSepDistance_;
  param_trimmed_in >> solveLevel_;
  param_trimmed_in >> symmetric_;
  param_trimmed_in >> secondOrder_;
  param_trimmed_in >> combinedElimination_;
  param_trimmed_in >> unCompressedSolve_;
  param_trimmed_in >> eliminationOrder_;
  param_trimmed_in >> lowRankMethod_;
  param_trimmed_in >> rSVDFactor_;
  param_trimmed_in >> cutOffMethod_;
  param_trimmed_in >> epsilon_;
  param_trimmed_in >> aPrioriRank_;
  param_trimmed_in >> rankCapFactor_;
  param_trimmed_in >> smoothing_;
  param_trimmed_in >> basisEnhancement_;

  hparams_ = new Hparams(treeDepth_, wellSepDistance_, solveLevel_, symmetric_,
                         secondOrder_, combinedElimination_, unCompressedSolve_,
                         eliminationOrder_, lowRankMethod_, rSVDFactor_,
                         cutOffMethod_, epsilon_, aPrioriRank_, rankCapFactor_,
                         smoothing_, basisEnhancement_);

  // ILU parameters
  param_trimmed_in >> ILUDropTol_;
  param_trimmed_in >> ILUFill_;
}
