#ifndef redNode_h
#define redNode_h

#include <set>
#include <vector>
#include "node.h"

class BlackNode;
class Hparams;

/**************************************************************************/
/*                             CLASS Red NODE                             */
/**************************************************************************/

//! Class redNode
/*!
  Inherited from the general class node. It is a node correponding to the
  multipole variables,
  and local equations.
*/

class RedNode : public Node {
  //! Pointer to the parent (which is a blackNode/NULL)
  BlackNode* parent_;

  //! Pointer to the child (which is a blackNode)
  BlackNode* child_;

  //! The level of node ( only redNodes have level)
  unsigned int level_;

  //! Indicate if it is a left(0) or right(1) child;
  bool which_;

  //! The adjacency set (from original interaction of the symbolic matrix)
  std::set<RedNode*> adjSet_;

  //! generalized adjacent set with distance threshold d (as a parameter)
  /*!
  Note that the adjSet_ (defined above) is a special case of general adjacent
  set where d = 1 is used. In this definition two red-nodes are adjacent if
  their distance is smaller than d for a given d > 0.
  */
  std::set<RedNode*>* generalAdjSet_;

  //! Access to HTree parameters
  Hparams* parameters_;

 public:
  //! Default constructor
  RedNode(){};

  //! Constructor:
  /*!
    input arguments:
    pointer to the blackNode parent, pointer to the tree, which child, range of
    belonging rows/cols
  */
  RedNode(BlackNode*, HTree*, bool, int, int);

  //! Destructor
  ~RedNode();

  //! Returns child_
  BlackNode* child() const { return child_; }

  //! True if this is leaf node
  bool isLeaf() const { return (child_ == 0); }

  //! Return parent_
  BlackNode* parent() const { return parent_; }

  //! Returns level_
  unsigned int level() const { return level_; }

  //! Returns which_
  bool which() const { return which_; }

  //! Continue creating the tree
  void createBlackNode();

  //! Returns pointer to adjacent rednodes set
  std::set<RedNode*>* adjSet() { return &adjSet_; }

  //! Returns pointer to general adjacent rednodes set
  /*!
  Note that the adjSet() (defined above) is a special case of general adjacent
  set where d = 1 is used. In this definition two red-nodes are adjacent if
  their distance is smaller than d for a given d > 0.
  */
  std::set<RedNode*>* generalAdjSet() { return generalAdjSet_; }

  //! Returns pointer to it self
  RedNode* redParent() { return this; }

  //! Form general adjacent set for this red-node
  /*!
  Find red-nodes at distance less than d (well-separation parameter) and
  form the general adjacent set. Note that this function assumes the regular
  adjacent set for the red-node is already created. This function is called by
  HTree::createGeneralAdjSet() */
  void formGeneralAdjSet();
};

#endif
