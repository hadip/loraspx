from main import *

config = Config()


class Node:
    def __init__(self, Mat = None,  RHS = None, Var = None):
        if Mat is None or RHS is None:
            raise Exception('Mat or RHS of node %i is None'%config.node_id)
        if isinstance(Mat, float):
            self.mat = array([[Mat]], dtype=float64)
        else:
            self.mat = Mat

        if isinstance(RHS, float64):
            RHS = [RHS]

        self.rhs = array(RHS,dtype=float64)
        self.var = Var
        self.id = config.node_id
        self._size = self.mat.shape[0]
        self._supernode = None # for usage of super node

        config.node_id += 1
        add_nodes(self)        

    def eliminate(self):
        ''' ELIMINATE eliminates a node and takes care of the mat flow and b flow. ''' 

        # mat flow
        for in_edge in config.incoming_edges_ids[self.id]:
            for out_edge in config.outgoing_edges_ids[self.id]:
                j_id = config.id_to_edges[out_edge]._to
                k_id = config.id_to_edges[in_edge]._from
                if j_id not in config.active_nodes_id or k_id not in config.active_nodes_id:
                    continue

                Aii = self.mat
                Aji = config.id_to_edges[out_edge].mat
                Aik = config.id_to_edges[in_edge].mat

                if Aii.shape[0]==1:
                    temp = Aji/Aii[0]
                else:
                    temp = dot(Aji, linalg.inv(Aii))
                dA = - dot(temp, Aik)

                if j_id != k_id:
                    e_kj = config.incoming_edges_ids[j_id] & config.outgoing_edges_ids[k_id]
                    assert(len(e_kj)<=1)
                    if len(e_kj)==1:
                        # print('Edge already exist.')
                        E_kj = config.id_to_edges[e_kj.pop()]
                    else:
                        # print('Create a new edge')
                        E_kj = Edge(zeros_like(dA), k_id, j_id)
                        add_to_incoming_edges_ids(j_id, E_kj.id)
                        add_to_outgoing_edges_ids(k_id, E_kj.id)
                    E_kj.mat += dA
                else:
                    config.id_to_nodes[j_id].mat += dA



        j_ids = []
        # b flow
        for out_edge in config.outgoing_edges_ids[self.id]:
            j_id = config.id_to_edges[out_edge]._to
            if j_id not in config.active_nodes_id:
                continue
            Aii = self.mat
            Aji = config.id_to_edges[out_edge].mat
            if Aii.shape[0]==1:
                temp = Aji/Aii[0]
            else:
                temp = dot(Aji, linalg.inv(Aii))
            config.id_to_nodes[j_id].rhs -= dot(temp, self.rhs).squeeze()
            j_ids.append(j_id)

        for j_id in j_ids:
            remove_edge(self.id, j_id)

                
        config.active_nodes_id.remove(self.id)

    def solve(self):
        if self.var is not None:
            return
        # propagate 
        if self.id not in config.active_nodes_id:
            raise Exception('Node %i is not active, cannot be solved'%self.id)
        self.var = self.rhs.copy() # so that the solve process is not destructive
        if self.id in config.incoming_edges_ids:
            for in_edge in config.incoming_edges_ids[self.id]:
                e = config.id_to_edges[in_edge]
                node = config.id_to_nodes[e._from]
                if node.var is not None:
                    self.var -= dot(e.mat, node.var).squeeze()

        if self.mat.shape[0]==1:
            if isclose(self.mat, 0.0):
                raise Exception('Singular matrice encountered during solving process,\
                    consider pivoting.')
            self.var = self.var / self.mat
        else:
            if linalg.cond(self.mat)>1e5:
                raise Exception('Singular matrice encountered during solving process,consider pivoting.')
            self.var = linalg.solve(self.mat, self.var)
        config.active_nodes_id.remove(self.id)


        

def e_from_nid(i,j):
    if i not in config.outgoing_edges_ids or j not in config.incoming_edges_ids:
        return None
    s = config.outgoing_edges_ids[i] & config.incoming_edges_ids[j]
    assert(len(s)<=1)
    if len(s)==0:
        return None
    else:
        return config.id_to_edges[s.pop()]

def add_edge_from_to(e, _from, _to):
    add_to_incoming_edges_ids(_to, e.id)
    add_to_outgoing_edges_ids(_from, e.id)

def remove_edge(e, s = None):
    if s is not None:
        e = e_from_nid(e, s)
        if e is None:
            return
    elif isinstance(e, int):
        e = config.id_to_edges[e]

    config.outgoing_edges_ids[e._from].remove(e.id)
    config.incoming_edges_ids[e._to].remove(e.id)
    del config.id_to_edges[e.id]



def merge(nodei, nodej):
    if isinstance(nodei, int):
        nodei = config.id_to_nodes[nodei]
    if isinstance(nodej, int):
        nodej = config.id_to_nodes[nodej]
    si = nodei._size
    sj = nodej._size
    Mat = zeros((si+sj, si+sj))
    Mat[0:si, 0:si] = nodei.mat
    Mat[si:,si:] = nodej.mat
    e_ji_id = config.outgoing_edges_ids[nodej.id] & config.incoming_edges_ids[nodei.id]
    if len(e_ji_id) != 0:
        Mat[:si,si:] = config.id_to_edges[e_ji_id.pop()].mat
    e_ij_id = config.outgoing_edges_ids[nodei.id] & config.incoming_edges_ids[nodej.id]
    if len(e_ij_id) != 0:
        Mat[si:,:si] = config.id_to_edges[e_ij_id.pop()].mat
    b = concatenate((nodei.rhs, nodej.rhs))
    node = Node(Mat, b)
    node._supernode = [nodei.id, nodej.id]

    # incoming edges
    inode = set([ config.id_to_edges[eid]._from for eid in config.incoming_edges_ids[nodei.id] ]).union(  \
        set([ config.id_to_edges[eid]._from for eid in config.incoming_edges_ids[nodej.id] ]))
    inode = inode.difference(set([nodei.id, nodej.id]))

    for nid in inode:
        Ai = e_from_nid(nid, nodei.id).mat if e_from_nid(nid, nodei.id) is not None \
            else zeros((si, config.id_to_nodes[nid]._size))
        Aj = e_from_nid(nid, nodej.id).mat if e_from_nid(nid, nodej.id) is not None \
            else zeros((sj, config.id_to_nodes[nid]._size))
        remove_edge(nid, nodei.id)
        remove_edge(nid, nodej.id)

        A = concatenate((Ai, Aj), axis=0)
        edge = Edge(A, nid,node.id)
        add_edge_from_to(edge, nid, node.id)

    # outgoing edges
    onode = set([ config.id_to_edges[eid]._to for eid in config.outgoing_edges_ids[nodei.id] ]).union( \
        set([ config.id_to_edges[eid]._to for eid in config.outgoing_edges_ids[nodej.id] ]))
    onode = onode.difference(set([nodei.id, nodej.id]))

    for nid in onode:
        Ai = e_from_nid(nodei.id, nid).mat if e_from_nid(nodei.id, nid) is not None \
            else zeros((config.id_to_nodes[nid]._size, si))
        Aj = e_from_nid(nodej.id, nid).mat if e_from_nid(nodej.id, nid) is not None \
            else zeros((config.id_to_nodes[nid]._size,sj))
        remove_edge(nodei.id, nid)
        remove_edge(nodej.id, nid)

        A = concatenate((Ai, Aj), axis=1)
        edge = Edge(A,node.id,nid)
        add_edge_from_to(edge, node.id, nid)

    config.active_nodes_id.remove(nodei.id)
    config.active_nodes_id.remove(nodej.id)
    config.active_nodes_id.add(node.id)
    return node.id

def splitVar(node):
    ''' split the node into two'''
    if isinstance(node, int):
        node = config.id_to_nodes[node]
    if node.var is None:
        raise Exception('SplitVar requires Var attributes to be not None')
    if node._supernode is None:
        raise Exception('Cannot split a non-supernode.')
    
    ni_id, nj_id = node._supernode
    ni = config.id_to_nodes[ni_id]
    nj = config.id_to_nodes[nj_id]

    ni.rhs = node.rhs[:ni._size]
    nj.rhs = node.rhs[ni._size:]

    ni.var = node.var[:ni._size]
    nj.var = node.var[nj._size]

    if node.id in config.active_nodes_id:
        config.active_nodes_id.remove(node.id)

    return ni_id, nj_id




    

class Edge:
    
    def __init__(self, Mat = None, from_id = None,  to_id = None):
        if from_id == to_id:
            raise Exception('No self loop edge is allowed')
        if Mat is None:
            raise Exception('Mat is None')
        if isinstance(Mat, float):
            self.mat = array([[Mat]], dtype=float64)
        else:
            self.mat = Mat
        self._from = from_id
        self._to = to_id
        self.id = config.edge_id
        config.edge_id += 1
        add_edges(self)
        



def add_to_incoming_edges_ids(n_id, e_id):
    if n_id not in config.incoming_edges_ids:
        config.incoming_edges_ids[n_id] = set([])
    config.incoming_edges_ids[n_id].add(e_id)
    
def add_to_outgoing_edges_ids(n_id, e_id):
    if n_id not in config.outgoing_edges_ids:
        config.outgoing_edges_ids[n_id] = set([])
    config.outgoing_edges_ids[n_id].add(e_id)
    
def add_nodes(n):
    if n.id not in config.id_to_nodes:
        config.id_to_nodes[n.id] = n
    else:
        raise Exception('Node %i already exists.'%n.id)

def add_edges(e):
    config.id_to_edges[e.id] = e

def plot_graph():

    if config.NOSHOW:
        return
    try:
        import networkx as nx
    except:
        raise Exception("Install networkx to enable plotting feature.")
    
    import os
    if 'figures' not in os.listdir('.'):
        os.system('mkdir figures')

    close('all')
    figure(figsize=(7,7))
    node_color = []
    node_size = []
    edge_labels = {}


    for nid in config.id_to_nodes:
        if nid not in config.outgoing_edges_ids:
            continue
        for eid in config.outgoing_edges_ids[nid]:
            pid = config.id_to_edges[eid]._to
            edge_labels[(nid,pid)] = str(config.id_to_edges[eid].mat)

    G=nx.DiGraph()
    G.add_node('Solved')
    G.add_node('Active')
    G.add_node('Inactive')
    node_color = [1.5,2.0,0.0]
    node_size = [100,100,100]
    for n_id in config.id_to_nodes:
        G.add_node(n_id)

        if config.id_to_nodes[n_id]._supernode is not None:
            node_size.append(500)
        else:
            node_size.append(100)

        if config.id_to_nodes[n_id].var is not None:
            node_color.append(1.5)
        elif n_id in config.active_nodes_id:
            node_color.append(2.0)
        else:
            node_color.append(0.0)
    for e_id in config.id_to_edges:
        edge = config.id_to_edges[e_id]
        if edge._from in config.active_nodes_id or edge._to in config.active_nodes_id:
            G.add_edge(edge._from, edge._to)

    graph_pos = nx.shell_layout(G)

    if len(config.active_nodes_id)==len(config.id_to_nodes):
        node_color = 'yellow'

    nx.draw_networkx_nodes(G,graph_pos, node_color=node_color,cmap='summer',node_size=node_size)
    nx.draw_networkx_edges(G,graph_pos,arrows=False,edge_color='r')
    nx.draw_networkx_labels(G, graph_pos, font_size=7)

    if config.verbose:
        nx.draw_networkx_edge_labels(G, graph_pos, edge_labels = edge_labels,label_pos=0.7,alpha=0.1,font_size=7)

    
    for i in range(len(config.id_to_nodes)):
        s = ''
        node = config.id_to_nodes[i]
        if node.var is not None: s += 'var=' + str(node.var) + '\n'
        if config.verbose:
            s += 'rhs=' + str(node.rhs) + '\n'
            s += 'mat=' + str(node.mat) + '\n'
        text(graph_pos[i][0], graph_pos[i][1], s,color='c',fontsize=7)

    # # # inactive
    for e_id in config.id_to_edges:
        edge = config.id_to_edges[e_id]
        if edge._from in config.active_nodes_id or edge._to in config.active_nodes_id:
            G.remove_edge(edge._from, edge._to)
        if edge._from not in config.active_nodes_id and edge._to not in config.active_nodes_id:
            G.add_edge(edge._from, edge._to)

    nx.draw_networkx_edges(G,graph_pos,arrows=False,edge_color='gray')
    savefig('figures/figure_%i.jpg'%config._counter)
    config._counter += 1
    
    


def construct_graph_dense(A, b):
    A = array(A, dtype=float64)
    b = array(b, dtype=float64)
    n = b.shape[0]
    for i in range(n):
        node = Node(A[i,i], [b[i]])
        config.leaf_nodes_id.append(node.id)
        config.active_nodes_id.add(node.id)
        
    for i in range(n):
        for j in range(n):
            if i==j:
                continue
            if not isclose(A[i,j],0):
                edge = Edge(A[i,j], config.leaf_nodes_id[j], config.leaf_nodes_id[i])
                add_to_incoming_edges_ids(config.leaf_nodes_id[i], edge.id)
                add_to_outgoing_edges_ids(config.leaf_nodes_id[j], edge.id)

def printinfo():
    print('config.leaf_nodes_id',config.leaf_nodes_id)
    print('config.active_nodes_id',config.active_nodes_id)
    print('config.incoming_edges_ids',config.incoming_edges_ids)
    print('config.outgoing_edges_ids', config.outgoing_edges_ids)
    print('config.id_to_edges', config.id_to_edges.keys())
    print('config.id_to_nodes',config.id_to_nodes.keys())

def node_info():
    for idn in config.id_to_nodes:
        print(idn)
        node = config.id_to_nodes[idn]
        print(node.mat)
        print(node.rhs)
        print(node.var)

def extractX():
    X = zeros(len(config.leaf_nodes_id))
    for i in range(len(config.leaf_nodes_id)):
        X[i] = config.id_to_nodes[config.leaf_nodes_id[i]].var
    return X

def lu(A,b):
    config.clear()
    construct_graph_dense(A, b)
    order = []
    for i in range(len(config.leaf_nodes_id)-1):   
        config.id_to_nodes[config.leaf_nodes_id[i]].eliminate()
        order.append(config.leaf_nodes_id[i])
        plot_graph()
        show()

    order.append(config.leaf_nodes_id[-1])
    order = order[::-1]
    for i in range(len(order)):
        config.id_to_nodes[order[i]].solve()
        if i < len(order)-1:
            config.active_nodes_id.add(order[i+1])
        plot_graph()
        show()

    assert(allclose(linalg.solve(A,b), extractX()))



def test_solve_Ax_b():

    A = random.random((10,10))
    b = random.random(10)

    construct_graph_dense(A, b)
    order = []
    for i in range(len(config.leaf_nodes_id)-1):   
        config.id_to_nodes[config.leaf_nodes_id[i]].eliminate()
        order.append(config.leaf_nodes_id[i])
        plot_graph()
        show()

    order.append(config.leaf_nodes_id[-1])
    order = order[::-1]
    for i in range(len(order)):
        config.id_to_nodes[order[i]].solve()
        if i < len(order)-1:
            config.active_nodes_id.add(order[i+1])
        plot_graph()
        show()

    assert(allclose(linalg.solve(A,b), extractX()))

def test_solve_Ax_b_merge():
    config.clear()
    A = random.random((10,10))
    b = random.random(10)
    construct_graph_dense(A, b)


    nid = merge(config.leaf_nodes_id[0], config.leaf_nodes_id[1])
    order = [nid]
    for i in range(2, len(config.leaf_nodes_id)):
        order.append(config.leaf_nodes_id[i])


    plot_graph()
    show()

    for i in range(len(order)-1):
        config.id_to_nodes[order[i]].eliminate()
        plot_graph()
        show()

    order = order[::-1]
    for i in range(len(order)):
        config.id_to_nodes[order[i]].solve()
        if i < len(order)-1:
            config.active_nodes_id.add(order[i+1])
        plot_graph()
        show()

    splitVar(nid)
    assert(allclose(linalg.solve(A,b), extractX()))

if __name__=="__main__":
    test_solve_Ax_b()
    test_solve_Ax_b_merge()



