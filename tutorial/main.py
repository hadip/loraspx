from numpy import *
from matplotlib.pyplot import *
from copy import deepcopy
# from matplotlib import pyplot as plt

def pmatrix(a):
	''' printing a latex matrix from numpy array a (2-D required)'''
	m,n = a.shape
	T = r'''\begin{equation}
  		\begin{pmatrix}
  		%s
  		\end{pmatrix}
		\end{equation}'''
	s = ''
	for i in range(m):
		for j in range(n):
			s += ' %0.4g '%a[i,j]
			s += '&' if j!=n-1 else '\\\\'
	print(T%s)

'''global configuration. All the global variables should be placed here'''
class Config():
	def __init__(self):
		self.node_id = None
		self.edge_id = None
		self.NOSHOW  = None
		self.incoming_edges_ids = None
		self.outgoing_edges_ids = None
		self.active_nodes_id = None
		self.leaf_nodes_id  = None
		self.id_to_nodes = None
		self.id_to_edges = None
		self.clear()


	def clear(self):
		self.node_id = 0
		self.edge_id = 0
		self.NOSHOW = True # toggle to draw graphs in `figures` directory
		self.verbose = False # verbose printing, including Mat, rhs.
		self.incoming_edges_ids = {}
		self.outgoing_edges_ids = {}
		self.active_nodes_id = set([])
		self.leaf_nodes_id = []
		self.id_to_nodes = {}
		self.id_to_edges = {}
		self._counter = 0
		self._n0 = None
		self._n1 = None
		self._n2 = None # for general usage



	def info(self):
		print('node_id', self.node_id)
		print('edge_id',self.edge_id)
		print('NOSHOW', self.NOSHOW)
		print('incoming_edges_ids',self.incoming_edges_ids)
		print('outgoing_edges_ids',self.outgoing_edges_ids)
		print('active_nodes_id',self.active_nodes_id )
		print('leaf_nodes_id',self.leaf_nodes_id )
		print('id_to_nodes',self.id_to_nodes.keys() )
		print('id_to_edges', self.id_to_edges.keys() )
		s = ''
		i = 0
		for e_id in self.id_to_edges:
			s += '%i-->%i\t'%(self.id_to_edges[e_id]._from, self.id_to_edges[e_id]._to)
			i += 1
			if i==5:
				i = 0
				s+='\n'
		print(s)

	def curEqn(self, node_list = None):
		s = 0
		starts = {}
		ends = {}
		if node_list is None:
			node_list = self.active_nodes_id

		for nid in node_list:
			starts[nid] = s
			s += self.id_to_nodes[nid]._size
			ends[nid] = s
		A = zeros((s,s))
		b = zeros(s)
		for nid in node_list:
			idx = starts[nid]
			A[idx:idx+self.id_to_nodes[nid]._size,idx:idx+self.id_to_nodes[nid]._size] = \
				self.id_to_nodes[nid].mat
			b[idx:idx+self.id_to_nodes[nid]._size] = self.id_to_nodes[nid].rhs
			for o_e in self.outgoing_edges_ids[nid]:
				_to = self.id_to_edges[o_e]._to
				if _to not in node_list:
					continue
				A[starts[_to]:ends[_to] , idx:idx+self.id_to_nodes[nid]._size] = \
					self.id_to_edges[o_e].mat

		# pring current Ax=b. This is very useful for debugging as we assumed that the system should 
		# recover the original solution any time if relevant(to be more precise, coupled nodes)
		#  nodes are picked
		print('A=',A)
		print('b=',b)
		print('x=',linalg.solve(A,b))





