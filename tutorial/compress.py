from graph import *

def is_compress_valid(nid,plist):
    if nid not in config.active_nodes_id:
        raise Exception('Node %i not in active set'%nid)
    for p in plist:
        for q in plist:
            if p==q:
                continue
            if e_from_nid(p, q) is not None:
                raise Exception('There is an edge %i --> %i'%(p,q))

def _getmat(nid0, nid1):
    # get edge matrix from two node ids, nid0, nid1
    edge = e_from_nid(nid0, nid1)
    if edge is None:
        return zeros((config.id_to_nodes[nid0]._size, config.id_to_nodes[nid1]._size))
    return edge.mat.reshape((config.id_to_nodes[nid0]._size, config.id_to_nodes[nid1]._size))

def _getr(s):
    # get the number of singular values we should use.
    S = s**2
    T = (s**2).sum()
    t = 0.0
    # # if we want to do exact decomposition, use the following line
    # return len(s)
    for r in range(len(s)):
      t += S[r]
      if t / T > 0.9:
          return r+1

def _dedge(i,j):
    # delete edge between i,j. It automatically takes care of other topology information.
    e = e_from_nid(i, j)
    if e is None:
        return
    del config.id_to_edges[e.id]
    config.incoming_edges_ids[j].remove(e.id)
    config.outgoing_edges_ids[i].remove(e.id)


def compress(nid, plist):
    ''' COMPRESS compress a node nid, given the corresponding compressing list.
    It will DELETE physically the original edges. The newly created nodes are activated.'''
    if len(plist)==0:
        print("Warning: plist is empty, nothing to be done.")
        return None, nid
    is_compress_valid(nid,plist)

    t = len(plist)
    As = [None]*t ; Bs = [None]*t
    for i in range(t):
        As[i] = _getmat(nid, plist[i])
        Bs[i] = _getmat(plist[i],nid).T
    S = concatenate(As+Bs, axis=0)
    u, s, v = linalg.svd(S)
    r = _getr(s)
    u = u[:,:r]
    s = s[:r]
    v = v[:r, :]
    V = dot(diag(s),v).T

    Rs = [None]*t ;  Qs = [None]*t
    idx = 0
    Hf = int(S.shape[0]/2)
    for i in range(t):
        Rs[i] = u[idx:idx+As[i].shape[0],:]
        Qs[i] = u[Hf+idx: Hf + idx+As[i].shape[0],:]
        idx += As[i].shape[0]

    # create new nodes and edges
    bnode = Node(zeros((r,r)),zeros(r))
    bsEdge = Edge(V, bnode.id, nid)
    add_edge_from_to(bsEdge, bnode.id, nid)
    sbEdge = Edge(V.T, nid, bnode.id)
    add_edge_from_to(sbEdge, nid, bnode.id)

    Pbnode = Node(zeros((r,r)), zeros(r))
    pbEdge = Edge(-eye(r), bnode.id, Pbnode.id)
    add_edge_from_to(pbEdge, bnode.id, Pbnode.id)
    bpEdge = Edge(-eye(r), Pbnode.id, bnode.id )
    add_edge_from_to(bpEdge, Pbnode.id, bnode.id)

    for i in range(t):
        edge = Edge(Qs[i].T, plist[i], Pbnode.id)
        add_edge_from_to(edge, plist[i], Pbnode.id)
        edge = Edge(Rs[i], Pbnode.id, plist[i])
        add_edge_from_to(edge, Pbnode.id, plist[i])

    # delete legacy edges
    for i in range(t):
        _dedge(plist[i], nid)
        _dedge(nid, plist[i])

    config.active_nodes_id.add(bnode.id)
    config.active_nodes_id.add(Pbnode.id)
    return bnode.id, Pbnode.id

def test_t_matrix():
    config.clear()
    N = 10
    A = random.random((N,N))
    b = ones(N)
    for i in range(1,N):
        for j in range(1,N):
            if i!=j:
                A[i,j] = 0.0

    construct_graph_dense(A, b)
    bid, pbid = compress(0, list(range(1,N)))
    order = [0,bid,pbid] + list(range(1,N))

    plot_graph();show() 
    for o in range(len(order)-1):
        config.id_to_nodes[order[o]].eliminate()
        plot_graph();show() 

    order = order[::-1]
    for o in range(len(order)):
        config.id_to_nodes[order[o]].solve()
        if o!=len(order)-1:
            config.active_nodes_id.add(order[o+1])
        plot_graph();show()

    assert(allclose(linalg.solve(A,b), extractX()))


if __name__=="__main__":
    test_t_matrix()
