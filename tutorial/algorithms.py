from compress import *

def tridiag(Bs,bs):
    ''' TRIDIAG solves a blockwise tridiagonal linear system, with subdiagonal entries -I. 
    LORASP is not used. Direct elimination is applied here as we will 
    preserve the sparsity pattern during the elimination process. 
    Bs -- a list of diagonal blocks
    bs -- corresponding right hand side
    '''
    blocks = len(Bs)
    if isinstance(Bs[0], float):
        blocksize = 1
    else:
        blocksize = Bs[0].shape[0]
    config.clear()
    for i in range(blocks):
        node = Node(Bs[i], bs[i])
        config.active_nodes_id.add(node.id)
        if i-1>=0:
            edge = Edge(-eye(blocksize), i-1, i)
            add_edge_from_to(edge, i-1, i)
        if i+1<=blocks-1:
            edge = Edge(-eye(blocksize), i+1, i)
            add_edge_from_to(edge, i+1, i)
    plot_graph()
    order = list(range(blocks))
    for o in range(len(order)-1):
        config.id_to_nodes[order[o]].eliminate()
        plot_graph()

    order = order[::-1]
    for o in range(len(order)):
        config.id_to_nodes[order[o]].solve()
        if o!=len(order)-1:
            config.active_nodes_id.add(order[o+1])
        plot_graph()

    X = [config.id_to_nodes[idv].var for idv in config.id_to_nodes]
    return concatenate(X).squeeze()


def test_tridiag():
    blocksize = 1
    blocks = 10
    Bs = []
    bs = []
    for i in range(blocks):
        B = random.random((blocksize,blocksize))
        b = random.random(blocksize)
        Bs.append(B)
        bs.append(b)

    B = zeros((blocksize*blocks,blocksize*blocks))
    b = zeros(blocksize*blocks)
    for i in range(blocks):
        B[i*blocksize:(i+1)*blocksize, i*blocksize:(i+1)*blocksize] = Bs[i]
        if i-1>=0:
            B[i*blocksize:(i+1)*blocksize, (i-1)*blocksize:i*blocksize] = -eye(blocksize)
        if i+1<=blocks-1:
            B[i*blocksize:(i+1)*blocksize, (i+1)*blocksize:(i+2)*blocksize] = -eye(blocksize)
        b[i*blocksize:(i+1)*blocksize] = bs[i]


    X = tridiag(Bs, bs)
    Y = linalg.solve(B,b)


    assert(allclose(X,Y))


def _tridiag(order):
    ''' _TRIDIAG is used for solving general tridiagonal matrix, 

    A <--> B <--> ... <--> Z

    it will eliminate the matrix in order of ORDER'''

    for o in range(len(order)-1):
        config.id_to_nodes[order[o]].eliminate()

        plot_graph()

    order = order[::-1]
    for o in range(len(order)):
        # config.curEqn([10,12,14])
        config.id_to_nodes[order[o]].solve()
        # print(config.id_to_nodes[order[o]].var)
        if o!=len(order)-1:
            config.active_nodes_id.add(order[o+1])
        plot_graph()

def _bmerge(curr):
    ''' _BMERGE merges pairwise CURR nodes'''

    store = []
    assert(len(curr)%2==0)
    for i in range(int(len(curr)/2)):
        bid = merge(curr[2*i], curr[2*i+1])
        plot_graph()
        store.append(bid)
    return store

def _solve(order):
    ''' _SOLVE solves in order of ORDER and takes care of supernode'''

    order0 = order[::-1]
    for o in range(len(order0)):
        if config.id_to_nodes[order0[o]].var is None:
            config.active_nodes_id.add(order0[o])
            plot_graph()
            config.id_to_nodes[order0[o]].solve()
            if config.id_to_nodes[order0[o]]._supernode is not None:
                splitVar(order0[o]) 
            plot_graph()




def _find_plist(nid):
    # write your own _find_plist topological utility functions
    res = []
    for s in config.active_nodes_id:
        if e_from_nid(nid, s) or e_from_nid(s, nid):
            res.append(s)
    return res

def _biProblem(n, blocks):
    curr = list(config.id_to_nodes.keys())
    
    order0 = []
    plot_graph()
    for i in range(n):
        order0 += curr
        curr = _bmerge(curr)

        store = []
        for nid in curr:
            bid , pbid = compress(nid, _find_plist(nid))
            config.id_to_nodes[nid].eliminate()
            config.id_to_nodes[bid].eliminate()
            order0 += [nid, bid]
            
            plot_graph()
            store.append(pbid)
        
        curr = store

    
    plot_graph()
    order = deepcopy(curr) # cannot solve directly. There are stability issues
    #TODO separate pivoting issue
    if len(order)>2:
        order[1], order[2] = order[2], order[1] # pivot. VERY important
    _tridiag(order)

    order0 = order0[::-1]
    for o in range(len(order0)):
        if config.id_to_nodes[order0[o]].var is None:
            config.active_nodes_id.add(order0[o])
            plot_graph()
            config.id_to_nodes[order0[o]].solve()
            if config.id_to_nodes[order0[o]]._supernode is not None:
                splitVar(order0[o])
    plot_graph()

    X = extractX()
    return X

    

def biProblem(n,blocks,A,b):
    if linalg.cond(A)>1e5:
        raise Exception('Singular Matrix')
    config.clear()
    construct_graph_dense(A, b)
    return _biProblem(n, blocks)


def test_biProblem():
    A = array([[2,1,-1,0,0,0],
        [1,2,0,-1,0,0],
        [-1,0,2,1,-1,0],
        [0,-1,1,2,0,-1],
        [0,0,-1,0,2,1],
        [0,0,0,-1,1,2]])
    b = ones(6)
    X = biProblem(1, 3, A, b)
    assert(allclose(X,linalg.solve(A,b)))

def test_biProblem_randomized():
    # be sure to use tridiagonal matrix plus  -I blocks
    n = 2
    m = 5
    N = 2**n
    A = zeros((N*m,N*m))
    b = random.random(N*m)


    idx = 0
    for i in range(m):
        B = random.random((N,N))
        for p in range(N):
            for q in range(N):
                if q-p>1 or p-q > 1:
                    B[p,q] = 0

        A[idx:idx+N,idx:idx+N] = B
        if idx-N>=0:
            A[idx:idx+N,idx-N:idx] = -eye(N)
        if idx+N<=N*m-N:
            A[idx:idx+N,idx+N:idx+2*N] = -eye(N)
        idx += N
    
    X = biProblem(n, m, A, b)
    assert(allclose(X,linalg.solve(A,b)))
    



def test_t_matrix2():
    config.clear()
    A = array([[2.23,1.12,-1.1,0],
                [1,2,0,-1],
                [-1,0,2,1],
                [0,-1,1,3]])
    b = ones(4)
    construct_graph_dense(A, b)
    import os; os.system('rm -rf figures/ && mkdir figures')
    plot_graph()
    _bmerge(config.leaf_nodes_id)

    bid, pbid = compress(4, _find_plist(4))
    config.id_to_nodes[4].eliminate()
    config.id_to_nodes[bid].eliminate()
    plot_graph()

    bid, pbid = compress(5, _find_plist(5))
    config.id_to_nodes[5].eliminate()
    config.id_to_nodes[bid].eliminate()
    plot_graph()
    
    config.id_to_nodes[7].eliminate()

    plot_graph()
    _solve([4,6,7,5,8,7,9])
    plot_graph()
    assert(allclose(linalg.solve(A,b),extractX()))

            


        
if __name__=="__main__":
    test_tridiag()
    test_biProblem()
    test_t_matrix2()
    test_biProblem_randomized()
            
                

