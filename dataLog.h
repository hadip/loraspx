#ifndef datalog_h
#define datalog_h

#include <fstream>

class DataLoader;
class DataProcessor;
class Preconditioner;
class OuterSolver;

//! Parameters class
/*!
  This class stores the performance timings, errors, etc.
 */

class DataLog {
 private:
  //! Pointer to the output file
  std::ofstream out_;

 public:
  //! Constructot (open the log file)
  DataLog();

  //! Destructor
  ~DataLog();

  //! Log data loader information
  void logDataLoader(DataLoader*);

  //! Log data Processor information
  void logDataProcessor(DataProcessor*);

  //! Log preconditioner information
  void logPreconditioner(Preconditioner*);

  //! Log outer solver information
  void logOuterSolver(OuterSolver*);
};
#endif
