#include "superNode.h"
#include <cassert>
#include <iostream>
#include "Eigen/Dense"
#include "Eigen/LU"
#include "edge.h"
#include "hparams.h"
#include "htree.h"
#include "redNode.h"
#include "rsvd.h"
#include "time.h"

// Constructor for superNode
SuperNode::SuperNode(BlackNode* P, HTree* T, int first, int last)
    : Node((Node*)P, T, first, last) {
  // set the machine precision zero
  MACHINE_PRECISION = 1e-14;

  // Set its parent
  parent_ = P;

  // Set its level
  level_ = P->parent()->level() + 1;

  // When a superNode born, it should be added to the superNode list of the tree
  T->addSuperNode(level_, this);

  // We set m, and n based on number of columns/rows corresponding to this
  // redNode.
  // However, for non-leaf redNodes, n will be changed after low-rank
  // approximation
  n(last - first + 1);

  // Initially not eliminated
  eliminated_ = false;

  // Pointer to the frobNorm list:
  frobNorms_ = &(T->frobNorms[0]);

  // Pointer to the globFrobNorm of the tree;
  globFrobNorm_ = &(T->globFrobNorm);

  // Pointer to the globSize of the tree;
  globSize_ = &(T->globSize);

  // Pointer to the totalSizes list:
  totalSizes_ = &(T->totalSizes[0]);

  // maximum singularvalue of the matrix
  maxSig_ = T->maxSig();

  // Access to hparams
  hparams_ = T->parameters();
}

// Destructor
SuperNode::~SuperNode() {
  // Delete interaction edges
  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (hparams_->symmetric()) {
      if (it->first != this) {
        it->first->outEdges.erase(this);
      }
    }
    delete it->second;
  }

  // Delete the inverse of the pivot matrix
  if (invPivot != NULL) delete invPivot;
  if (inInvPivot_ != NULL) delete inInvPivot_;
  if (outInvPivot_ != NULL) delete outInvPivot_;
}

// Compress all well-separated interactions
timeTuple2 SuperNode::compress() {
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  start = clock();
  // Check if superNode has any variable
  if (n() == 0 || level() <= hparams_->solveLevel()) {
    parent()->n(0);
    redParent()->n(0);
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // First erase all previously compressed edges from in/out edge lists
  eraseCompressedEdges();

  // Calculate the total number of columns (by incoming edges)
  int nCols = 0;
  if (!(hparams_->symmetric())) {
    // Go over all incoming edges, and compress those are well-separated
    for (auto it = inEdges.begin(); it != inEdges.end(); ++it) {
      if (it->second->isWellSeparated()) {
        nCols += it->second->matrix().cols();
      }
    }
  }

  // Add outgoing edges
  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (it->second->isWellSeparated()) {
      if (it->second->isTransposed(this)) {
        nCols += it->second->matrix().cols();
      } else {
        nCols += it->second->matrix().rows();
      }
    }
  }

  if (nCols == 0) {
    parent()->n(0);
    redParent()->n(0);
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // Find the pivot edge (selfedge)
  Edge* selfEdge = outEdges[this];

  if (hparams_->secondOrder() == 1) {  // Use SVD
    Eigen::JacobiSVD<densMat> svdS(selfEdge->matrix(),
                                   Eigen::ComputeThinU | Eigen::ComputeThinV);
    VectorXd sigma1_2(n());
    VectorXd invSigma1_2(n());
    for (int i = 0; i < n(); i++) {
      sigma1_2(i) = std::sqrt((svdS.singularValues())(i));
      invSigma1_2(i) = 1.0 / sigma1_2(i);
    }
    invVo_ = new densMat(invSigma1_2.asDiagonal() * svdS.matrixV().transpose());
    Vo_ = new densMat(svdS.matrixV() * sigma1_2.asDiagonal());
    if (!(hparams_->symmetric())) {
      Vi_ = new densMat(svdS.matrixU() * sigma1_2.asDiagonal());
      invVi_ =
          new densMat(invSigma1_2.asDiagonal() * svdS.matrixU().transpose());
    }

  } else if (hparams_->secondOrder() == 2) {  // Use Cholesky
    Eigen::LLT<densMat> lu(selfEdge->matrix());
    Vo_ = new densMat(lu.matrixU());
    invVo_ = new densMat(Vo_->inverse());
  }

  densMat bigUV(n(), nCols);
  // Fill the bigUV matrix from entries of the well-separated interactions
  int nColsFilled = 0;
  if (!(hparams_->symmetric())) {
    for (auto it = inEdges.begin(); it != inEdges.end(); ++it) {
      if (it->second->isWellSeparated()) {
        if (hparams_->secondOrder() > 0) {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().cols())
              << (*invVi_) * it->second->matrix();
        } else {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().cols())
              << it->second->matrix();
        }
        nColsFilled += it->second->matrix().cols();
      }
    }
  }

  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (it->second->isWellSeparated()) {
      if (it->second->isTransposed(this)) {
        if (hparams_->secondOrder() > 0) {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().cols())
              << (*invVo_) * it->second->matrix();
        } else {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().cols())
              << it->second->matrix();
        }
        nColsFilled += it->second->matrix().cols();
      } else {
        if (hparams_->secondOrder() > 0) {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().rows())
              << (*invVo_) * it->second->matrix().transpose();
        } else {
          bigUV.block(0, nColsFilled, n(), it->second->matrix().rows())
              << it->second->matrix().transpose();
        }
        nColsFilled += it->second->matrix().rows();
      }
    }
  }

  // Compress bigU = Unew * R
  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();
  Eigen::JacobiSVD<densMat> svdUV(bigUV,
                                  Eigen::ComputeThinU | Eigen::ComputeThinV);
  finish = clock();
  times[0] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();
  // Find the rank given the full SVD
  int k = cutOff(svdUV, hparams_->cutOffMethod(), hparams_->epsilon(),
                 hparams_->aPrioriRank(), hparams_->rankCapFactor());

  // Set the size of parent nodes
  parent()->n(k);
  redParent()->n(k);

  if (k == 0) {
    if (!(hparams_->symmetric())) {
      for (auto it = inEdges.begin(); it != inEdges.end(); ++it) {
        if (it->second->isWellSeparated()) {
          it->second->compress();
        }
      }
    }
    for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
      if (it->second->isWellSeparated()) {
        it->second->compress();
      }
    }
    eraseCompressedEdges();
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // Now that we have the exact size of parents, we can set RHS and VAR
  if (!hparams_->combinedElimination()) {
    parent()->RHS(new VectorXd(parent()->n()));
    parent()->VAR(new VectorXd(parent()->n()));
  }

  redParent()->RHS(new VectorXd(redParent()->n()));
  redParent()->VAR(new VectorXd(redParent()->n()));

  // Store the interpolation matrix
  interpolationU = new densMat(svdUV.matrixU().leftCols(k));

  int firstRow = 0;

  if (!(hparams_->symmetric())) {
    for (auto it = inEdges.begin(); it != inEdges.end(); ++it) {
      if (it->second->isWellSeparated()) {
        // Say the matrix on the edge is K [ kold * nCols ]
        // Update K = R [k * kold] * K, which will be [k * nCols]
        int nColsi = it->second->matrix().cols();

        densMat val =
            (svdUV.singularValues().head(k).asDiagonal() *
             (svdUV.matrixV().block(firstRow, 0, nColsi, k)).transpose());

        it->first->createEdge(redParent(), val);

        it->second->compress();

        firstRow += nColsi;
      }
    }
  }

  // Repeat all in the above for outEdges

  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (it->second->isWellSeparated()) {
      int nRowsi;
      if (it->second->isTransposed(this)) {
        nRowsi = it->second->matrix().cols();
      } else {
        nRowsi = it->second->matrix().rows();
      }
      densMat val = (svdUV.matrixV().block(firstRow, 0, nRowsi, k) *
                     svdUV.singularValues().head(k).asDiagonal());
      redParent()->createEdge(it->first, val);

      it->second->compress();

      firstRow += nRowsi;
    }
  }

  eraseCompressedEdges();

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;
  return times;
}

// Calculate k, the number of important singular values (used for compression)
int SuperNode::cutOff(Eigen::JacobiSVD<densMat>& svd, int method,
                      double epsilon, int apRank, double rankCapFactor) {
  int return_value = 0;
  // number of singular values
  int p = svd.singularValues().size();

  if (p == 0) {
    return 0;
  }

  if (svd.singularValues()(0) < MACHINE_PRECISION) {
    return 0;
  }

  // method0 : cutofff based on the absolute value
  if (method == 0) {
    int k_return = p;
    for (int i = 0; i < p; i++) {
      // Find the first singular value that is smaller than epsilon
      if ((svd.singularValues())(i) < epsilon) {
        k_return = i;
        break;
      }
    }

    return_value = k_return;
  }
  // method1: cutoff based on relative values
  else if (method == 1) {
    double sigma1 = svd.singularValues()(0);

    int k_return = p;
    for (int i = 0; i < p; i++) {
      // Find the first singular value that is smaller than epsilon
      if (svd.singularValues()(i) < epsilon * sigma1) {
        k_return = i;
        break;
      }
    }

    return_value = k_return;
  } else if (method == 2) {
    int k_return = p;
    for (int i = 0; i < p; i++) {
      // Find the first singular value that is smaller than epsilon
      if ((svd.singularValues())(i) < epsilon * frobNorms_[level_]) {
        k_return = i;
        break;
      }
    }

    return_value = k_return;
  } else if (method == 3) {
    int k_return = p;

    for (int i = 0; i < p; i++) {
      // Find the first singular value that is smaller than epsilon
      if ((svd.singularValues())(i) < epsilon * (*globFrobNorm_)) {
        k_return = i;
        break;
      }
    }

    return_value = k_return;
  }

  else if (method == 4) {
    int k_return = p;
    double frobNorm2 = 0;
    for (int i = p - 1; i >= 0; i--) {
      frobNorm2 += std::pow(svd.singularValues()(i), 2);
      // Find the first place that the frobNorm is greater than epsilon
      if (std::sqrt(frobNorm2) > frobNorms_[level_] * epsilon) {
        k_return = i;
        break;
      }
    }
    if (std::sqrt(frobNorm2) <= frobNorms_[level_] * epsilon) {
      return 0;
    } else {
      return_value = k_return + 1;
    }
  } else if (method == 5) {
    int k_return = p;
    double frobNorm = 0;
    double blockSize = (svd.matrixU().rows() * svd.matrixV().rows());
    double epsilon2 = blockSize * std::pow(frobNorms_[level_], 2) * epsilon *
                      epsilon / totalSizes_[level_];
    // double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
    for (int i = p - 1; i >= 0; i--) {
      frobNorm += std::pow(svd.singularValues()(i), 2);
      // Find the first place that the frobNorm is greater than epsilon
      if (frobNorm > epsilon2) {
        k_return = i;
        break;
      }
    }
    if (frobNorm <= epsilon2) {
      return 0;
    } else {
      return_value = k_return + 1;
    }
  } else if (method == 6) {
    int k_return = p;
    double frobNorm2 = 0;
    for (int i = p - 1; i >= 0; i--) {
      frobNorm2 += std::pow(svd.singularValues()(i), 2);
      // Find the first place that the frobNorm is greater than epsilon
      if (std::sqrt(frobNorm2) > (*globFrobNorm_) * epsilon) {
        k_return = i;
        break;
      }
    }
    if (std::sqrt(frobNorm2) <= (*globFrobNorm_) * epsilon) {
      return 0;
    } else {
      return_value = k_return + 1;
    }
  } else if (method == 7) {
    int k_return = p;
    double frobNorm = 0;
    double blockSize = (svd.matrixU().rows() * svd.matrixV().rows());
    double epsilon2 = blockSize * std::pow((*globFrobNorm_), 2) * epsilon *
                      epsilon / (*globSize_);
    // double epsilon2 = std::pow(frobNorms_[ level_ ],2) * epsilon;
    for (int i = p - 1; i >= 0; i--) {
      frobNorm += std::pow(svd.singularValues()(i), 2);
      // Find the first place that the frobNorm is greater than epsilon
      if (frobNorm > epsilon2) {
        k_return = i;
        break;
      }
    }
    if (frobNorm <= epsilon2) {
      return 0;
    } else {
      return_value = k_return + 1;
    }
  } else if (method == 8) {
    return_value = std::min(p, apRank);
  }
  // method 9: cutoff based on norm2 matrix
  else if (method == 9) {
    int k_return = p;
    for (int i = 0; i < p; i++) {
      // Find the first singular value that is smaller than epsilon
      if (svd.singularValues()(i) < epsilon * maxSig_) {
        k_return = i;
        break;
      }
    }

    return_value = k_return;
  }
  // method 10: cutoff based on Frob norm of ignored part.
  else if (method == 10) {
    int k_return = p;
    double frobNorm2 = 0;
    for (int i = p - 1; i >= 0; i--) {
      frobNorm2 += std::pow(svd.singularValues()(i), 2);
      // Find the first place that the frobNorm is greater than epsilon
      if (std::sqrt(frobNorm2) > epsilon) {
        k_return = i;
        break;
      }
    }
    if (std::sqrt(frobNorm2) <= epsilon) {
      return 0;
    } else {
      return_value = k_return + 1;
    }
  }

  if (rankCapFactor > MACHINE_PRECISION) {
    int rankCap =
        (int)(apRank * std::pow(rankCapFactor, hparams_->treeDepth() - level_));
    return std::min(return_value, rankCap);
  } else {
    return return_value;
  }
}

bool SuperNode::criterionCheck(int meth, double eps,
                               RedSVD::RedSVD<densMat>& RSVD, densMat& A,
                               int r) {
  if (r == 0 || (meth == 8)) {
    return true;
  }

  else if (meth == 0) {
    return (RSVD.singularValues()(r - 1) <= eps);
  } else if (meth == 1) {
    if (RSVD.singularValues()(0) < MACHINE_PRECISION) {
      return true;
    } else if (r == 1) {
      return false;
    } else {
      return (RSVD.singularValues()(r - 1) / RSVD.singularValues()(0) <= eps);
    }
  } else if (meth == 2) {
    return (RSVD.singularValues()(r - 1) <= eps * frobNorms_[level_]);
  } else if (meth == 3) {
    return (RSVD.singularValues()(r - 1) <= eps * (*globFrobNorm_));
  } else {
    densMat residu = A -
                     RSVD.matrixU().leftCols(r) *
                         RSVD.singularValues().head(r).asDiagonal() *
                         RSVD.matrixV().leftCols(r).transpose();
    double normResidu = residu.norm();
    double blockSize = (RSVD.matrixU().rows() * RSVD.matrixV().rows());
    if (meth == 4) {
      return (normResidu <= eps * frobNorms_[level_]);
    } else if (meth == 5) {
      double epsilon2 = blockSize * std::pow(frobNorms_[level_], 2) * eps *
                        eps / totalSizes_[level_];
      return (normResidu * normResidu <= epsilon2);
    } else if (meth == 6) {
      return (normResidu <= eps * (*globFrobNorm_));
    } else if (meth == 7) {
      double epsilon2 =
          blockSize * std::pow((*globFrobNorm_), 2) * eps * eps / (*globSize_);
      return (normResidu * normResidu <= epsilon2);
    }
  }
  return false;
}

// Block gauss elimination
timeTuple2 SuperNode::schurComp() {
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  start = clock();
  // Step 0: Check if this node has any variable
  if (n() == 0) {
    eliminate();
    parent()->leftChild()->eliminate();
    parent()->rightChild()->eliminate();
    parent()->eliminate();
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // First we detect the selfEdge (i.e., the edge from supernode to itself)
  Edge* selfEdge = outEdges[this];

  // Now compute the inverse of the selfEdge matrix (i.e., inverse of the pivot)
  invPivot = new densMat(n(), n());

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();

  *invPivot = selfEdge->matrix().inverse();

  finish = clock();
  times[0] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();

  if (parent()->n() > 0) {
    if (!(hparams_->symmetric())) {
      // Create incoming edge from the balck parent
      densMat val_i(n(), parent()->n());
      if (hparams_->secondOrder() > 0) {
        val_i = (*Vi_) * (*interpolationU);
      } else {
        val_i = *interpolationU;
      }
      parent()->createEdge(this, val_i);
    }

    // Create outgoing edge to the balck parent
    densMat val_o(parent()->n(), n());
    if (hparams_->secondOrder() > 0) {
      val_o = interpolationU->transpose() * Vo_->transpose();
    } else {
      val_o = interpolationU->transpose();
    }
    createEdge(parent(), val_o);
    delete interpolationU;
  }

  gaussElimination();

  // Now, mark the node as eliminated
  eliminate();

  // eliminate left and right redChildren
  parent()->leftChild()->eliminate();
  parent()->rightChild()->eliminate();

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;

  timeTuple2 times_black;
  // After eliminating a superNode, we can immediately eliminate its
  // black-parent
  times_black = parent()->schurComp();
  times[0] += times_black[0];
  times[1] += times_black[1];
  return times;
}

// Block Gauss elimination (combined to super and black nodes)
timeTuple2 SuperNode::combinedSchurComp() {
  // When no black/red parent exist
  if (parent()->n() == 0) {
    return schurComp();
  }

  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  start = clock();
  // Step 0: Check if this node has any variable
  if (n() == 0) {
    eliminate();
    parent()->leftChild()->eliminate();
    parent()->rightChild()->eliminate();
    parent()->eliminate();
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // First we detect the selfEdge (i.e., the edge from supernode to itself)
  Edge* selfEdge = outEdges[this];

  // This time we compute modified inverse of pivot
  invPivot = new densMat(n(), n());

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;

  start = clock();

  // This is the regular inverse of the pivot
  densMat invS = selfEdge->matrix().inverse();
  outInvPivot_ = new densMat(n(), parent()->n());
  if (!(hparams_->symmetric())) {
    inInvPivot_ = new densMat(parent()->n(), n());
  }

  // Required matrices for combined elimination
  if (hparams_->secondOrder() > 0) {
    *outInvPivot_ = invVo_->transpose() * (*interpolationU);
    densMat value = densMat::Identity(redParent()->n(), redParent()->n());

    if (!(hparams_->symmetric())) {
      *inInvPivot_ = interpolationU->transpose() * (*invVi_);
      *invPivot = invS - (*outInvPivot_) * (*inInvPivot_);
      delete Vi_;
      delete invVi_;
    } else {
      *invPivot = invS - (*outInvPivot_) * outInvPivot_->transpose();
    }
    // Add red Parent self edge
    redParent()->createEdge(redParent(), value);

    delete Vo_;
    delete invVo_;
  } else {
    *outInvPivot_ = invS * (*interpolationU);
    densMat lowRankS = interpolationU->transpose() * (*outInvPivot_);
    densMat lowRankSInv = lowRankS.inverse();
    if (!(hparams_->symmetric())) {
      *inInvPivot_ = (lowRankSInv * interpolationU->transpose()) * invS;
      *invPivot = invS - (*outInvPivot_) * (*inInvPivot_);
      *outInvPivot_ = (*outInvPivot_) * lowRankSInv;
    } else {
      *invPivot =
          invS - (*outInvPivot_) * lowRankSInv * outInvPivot_->transpose();
      *outInvPivot_ = (*outInvPivot_) * lowRankSInv;
    }
    // Add red Parent self edge
    redParent()->createEdge(redParent(), lowRankSInv);
  }

  delete interpolationU;

  finish = clock();
  times[0] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();

  // Gauss elimination (super and black node combined)

  // Loop over all incoming edges to supernode and create the corresponding edge
  // to redParent

  if (!(hparams_->symmetric())) {
    for (auto it_i = inEdges.begin(); it_i != inEdges.end(); ++it_i) {
      if ((it_i->first != this) && !(it_i->second->isEliminated())) {
        densMat val = (*inInvPivot_) * it_i->second->matrix();
        it_i->first->createEdge(redParent(), val);
      }
    }
  }

  // Loop over all outGoing edges to supernode and create the corresponding edge
  // to redParent
  for (auto it_j = outEdges.begin(); it_j != outEdges.end(); ++it_j) {
    if ((it_j->first != this) && !(it_j->second->isEliminated())) {
      densMat val;
      if (it_j->second->isTransposed(this)) {
        val = it_j->second->matrix().transpose() * (*outInvPivot_);
      } else {
        val = it_j->second->matrix() * (*outInvPivot_);
      }
      redParent()->createEdge(it_j->first, val);
    }
  }

  gaussElimination();

  // Now, mark the node as eliminated
  eliminate();

  // Mark left and right redChildren as eliminated
  parent()->leftChild()->eliminate();
  parent()->rightChild()->eliminate();

  // Mark black parent as eliminated
  parent()->eliminate();

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;

  return times;
}

// Split the vector VAR to the left and right redNodes
void SuperNode::splitVAR() {
  if (parent()->leftChild()->n() >
      0)  // i.e., if the left redNode has any unknowns
  {
    *(parent()->leftChild()->VAR()) =
        VAR()->segment(0, parent()->leftChild()->n());
  }

  if (parent()->rightChild()->n() >
      0)  // i.e., if the right redNode has any unknowns
  {
    *(parent()->rightChild()->VAR()) =
        VAR()->segment(parent()->leftChild()->n(), parent()->rightChild()->n());
  }
}