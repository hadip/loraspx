THIS CODE IS WRITTEN BY HADI POURANSARI

CONTACT: HADIP [AT] STANFORD [D.O.T.] EDU

-----------------------------------------

Please read **refman.pdf** for a brief description of the methods and classes implemented in the code.

You need to install:

1. The [Eigen](http://eigen.tuxfamily.org/dox/GettingStarted.html) library.

2. The [SCOTCH](https://www.labri.fr/perso/pelegrin/scotch/) library.

Make sure to change the PATH to these libraries in the makefile.

-----------------------------------------

License (see also the file LICENSE in the repository)

LORASPX, a direct solver for sparse linear systems
Developed by Hadi Pouransari and Eric Darve

Copyright (C) 2015-2017 Mohammad Hadi Pouransari, Eric Felix Darve

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


CONTACT: 

- HADIP [AT] STANFORD [D.O.T.] EDU
- DARVE [AT] STANFORD [D.O.T.] EDU
