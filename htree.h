#ifndef htree_h
#define htree_h
#include <array>
#include <map>
#include <string>
#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "preconditioner.h"

class Hparams;
class Node;
class RedNode;
class SuperNode;

/**
 * { Define a triplet type to store symbolic matrix }
 */
typedef Eigen::Triplet<bool> TRIPLETBOOL;

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! Define a sparse matrix (column major) with boolean entries */
typedef Eigen::SparseMatrix<bool> spMatBool;

/*! Define a redNode* list */
typedef std::vector<RedNode*> redNodeStrList;

/*! Define a superNode* list */
typedef std::vector<SuperNode*> superNodeStrList;

/*! A dense vector class used for permutation */
typedef Eigen::Matrix<int, Eigen::Dynamic, 1> VectorXi;

/*! The permutation matrix type */
typedef Eigen::PermutationMatrix<Eigen::Dynamic, Eigen::Dynamic, int> permMat;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Tuple of times for (time1, time2) */
typedef std::array<double, 2> timeTuple2;

/*! Tuple of times for (compression (svd), compression (rest), sChurComp
 * (inverse), sChurComp (rest)) */
typedef std::array<double, 4> timeTuple4;

//! Class htree
/*! This class represents a tree of nodes, and provide necessary
   information/routines for tree. */
class HTree : public Preconditioner {
  static constexpr double machinePrecision = 1e-16;
  /***************************************************/
  /*     HTREE RELATED VARIABLES/FUNCTION (Private)   */
  /***************************************************/

  //! Pointer to the parameter class
  Hparams* param_ = NULL;

  //! Pointer to the root node
  Node* root_ = NULL;

  //! Pointer to the matrix
  spMat* matrix_ = NULL;

  //! Pointer to solution (considered dense)
  VectorXd* VAR_ = NULL;

  //! Pointer to RHS (considered dense)
  VectorXd* RHS_ = NULL;

  //! Approximation of maximum singularvalue of the matrix.
  double maxSig_;

  //! Pointer to the symbolic matrix (and a temporary for permutation)
  spMatBool* symb_matrix_ = NULL;
  spMatBool* permuted_symb_matrix_ = NULL;

  //! A permutation matrix
  /*! Used once at each level to re order DOF's based on
   their assigned node in partitioning */
  permMat* permutationMatrix_ = NULL;

  //! Full permutation matrix
  permMat* fullPermutationMatrix_ = NULL;

  //! Size of the matrix
  int n_;

  //! The list of pointers of red nodes at each level
  std::vector<redNodeStrList> redNodeList_;

  //! The list of pointers of superNodes at each level
  std::vector<superNodeStrList> superNodeList_;

  //! The list containing mean rank at each level
  std::vector<double> meanRanks_;

  //! The list containing minimum rank at each level
  std::vector<int> minRanks_;

  //! The list containing maximum rank at each level
  std::vector<int> maxRanks_;

  //! Use permutationVector and permute the full matrix
  void permuteMatrix();

  //! A map from the permuted matrix columns to the leaves of the tree
  std::map<int, RedNode*> col2Leaf_;

  //! Total times
  double total_SVD;
  double total_GEMM;
  double total_INV;
  double total_REST;

  //! Track if new instance of Hparams class is created in construction
  bool default_param_created_ = false;

  //! From symbolic matrix of the given numerical matrix
  void formSymbolicMatrix();

  //! Form the adjacency set for all redNodes
  /*!
    Note that this is the set of original interactions induced by children.
    Later on during the elimination, nodes may have new interactions, and will
    be stored through edges.
    Only symbolic matrix will be used here.
  */
  void createAdjSet();

  //! Form the set of adjacent red-nodes with general distance d
  /*!
  This is a generalization to createAdjSet in which if ditance is smaller than 1
  two red-nodes are adjacent. Here, for each red-node we form a set of all other
  red-nodes at the same level with distance smaller than parameter d.
  */
  void createGeneralAdjSet();

  //! Using a Map we create key-value pairs for all leaf nodes
  /*!
    For each non-empty leaf the key is the index of the first columns it posses,
    and the value is the pointer to the leaf.
   */
  void createCol2LeafMap();

  //! Load the data to edges between tree leaves
  /*!
    After the tree, and adjacency lists are created,
    load the sub block of matrix to the edges between leaves.
  */
  void createLeafEdges();

  //! Dedicate memory, and set the VAR for leaf nodes
  void setLeafRHSVAR();

  //! Set tree depth and a-priori rank automatically
  void autoParameterSet();

  //! Apply full permutation (combined of all levels) to the numerical matrix
  void applyFullPermutation();

  //! Apply inverse of full permutation (combined of all levels) to the
  //! numerical matrix
  void applyInverseFullPermutation();

  //! Eliminate variables at level [l] in the tree
  /*!
    Output is the time elapsed.
   */
  timeTuple4 eliminate(unsigned int);

  //! Set the RHS for all nodes
  void setRHS(VectorXd&);

  //! Dedicate memory, and set the RHS for leaf nodes
  void setLeafRHS(VectorXd&);

  //! SolveL for black, super, and red Nodes in a level
  /*!
    This functions work from bottom to top
   */
  void solveL(unsigned int);

  //! SolveU for black, super, and red Nodes in a level
  /*!
    This functions work from top to the bottom
   */
  void solveU(unsigned int);

  //! Bottom to Top traverse to decompose A = L U
  void factorize();

  //! Compute solution
  /*!
    i.e., collect the solution of all leaves
  */
  VectorXd& computeSolution();

  //! Creating superNodes
  /*!
    This function combine sibling redNodes at level [l] of the tree, and create
    a new superNode.
    By construction, the parent of the resulted superNode is a blackNode.
    Note that in order to acess pairs of sibling redNodes at level [l] of the
    tree,
    we go through the redNodes at level [l-1] in the tree, and look at their
    grand red children.
    Output is the time elapsed to merge.
   */
  double createSuperNodes(unsigned int);

  //! Compute the Frob. norm at level l
  void computeFrobNorm(unsigned int);

  //! Approximate maximum singular value of the matrix
  void computeMaxSig();

  //! Tree assembling time
  double setupTime_;

  //! Time for preconditioner factorization time
  double factorizationTime_;

 public:
  //! Integer vector to form the permutation matrix at each level
  VectorXi* permutationVector_ = NULL;

  //! Store Forb. of each level
  /*!
    The frob. norm of level l is defined as:
    sqrt( sum_{e is edge between two superNodes} e.matrix.frob^2)
   */
  std::vector<double> frobNorms;

  //! Totoal size of each level
  /*!
    For each level, l, it computes:
    sum of sizes (= m*n for a m-by-n matrix) of all blocks in that level
   */
  std::vector<double> totalSizes;

  //! global frob norm = sum(frobNorms)
  double globFrobNorm;

  //! global size = sum(totalSizes)
  double globSize;

  //! Count the number of eliminated nodes (so far)
  int count;

  //! Default constructor
  HTree(){};

  //! Constructor
  /* Create the H-tree, and perform factorization.
  If parameters are not provided default values will be used.
  */
  HTree(spMat*, Hparams* = NULL);

  //! Destructor
  ~HTree();

  //! Returns param_
  Hparams* parameters() const { return param_; }

  //! Returns symb_matrix_
  spMatBool* symbMatrix() const { return symb_matrix_; }

  // Returns size of the matrix n_
  int n() const { return n_; }

  //! Returns pointer to the list of redNodes
  std::vector<redNodeStrList>* redNodeList() { return &redNodeList_; }

  //! Returns pointer to the list of superNodes
  std::vector<superNodeStrList>* superNodeList() { return &superNodeList_; }

  //! Returns pointer to mean rank list
  std::vector<double>* meanRanks() { return &meanRanks_; }

  //! Returns pointer to min rank list
  std::vector<int>* minRanks() { return &minRanks_; }

  //! Returns pointer to max rank list
  std::vector<int>* maxRanks() { return &maxRanks_; }

  //! Add a new redNode to the list
  /*!
    The blackNode will use this function to create its red children.
    The first argument is the level of the new red node and the second is the
    pointer to it
  */
  void addRedNode(unsigned int, RedNode*);

  //! Add a new superNode to the list
  /*!
    The blackNode will use this function to create its superNode child.
    The first argument is the level of the new superNode and the second is the
    pointer to it
  */
  void addSuperNode(unsigned int, SuperNode*);

  //! Top to bottom traverse to solve for a given RHS
  /*!
    If no RHS is provided it uses the RHS from input params, and automatically
    permute it.
    For the overloaded  version, it takes a permuted RHS as an input.
   */
  VectorXd& solve(VectorXd&);

  //! Access to the pointer of the variables vector
  VectorXd* VAR() { return VAR_; }

  //! set ranks
  /*!
    At each level compute mean, min, and max size of the redNodes
   */
  void setRanks();

  //! Log all information related to HTree (in human readable format)
  void log(std::ofstream&);

  //! Returns a pointer to the solution
  VectorXd* retVal();

  //! Returns matrix maximum singular value
  double maxSig() const { return maxSig_; }
};
#endif
