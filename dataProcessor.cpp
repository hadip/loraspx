#include "dataProcessor.h"
#include <stdlib.h>
#include <iostream>
#include <set>
#include "dataLoader.h"
#include "time.h"

DataProcessor::DataProcessor(DataLoader* dl, unsigned int s_param,
                             unsigned int pe_param) {
  clock_t start = clock();
  dl_ = dl;
  matrix_ = dl->matrix();
  numRHS_ = dl->numRHS();
  scaling_param_ = s_param;
  elim_param_ = pe_param;

  d1_ = new VectorXd(matrix_->rows());
  d2_ = new VectorXd(matrix_->cols());
  invD2_ = new VectorXd(matrix_->cols());
  xUnScaled_ = new VectorXd(matrix_->cols());
  matrixScaled_ = new spMat(matrix_->rows(), matrix_->cols());
  solScaled_ = new VectorXd(matrix_->cols());
  rhsScaled_ = new VectorXd(matrix_->rows());

  xUnPreEliminate_ = new VectorXd(matrix_->cols());

  applyScaling();
  applyPreElimination();
  time_ = double(clock() - start) / CLOCKS_PER_SEC;
}

DataProcessor::~DataProcessor() {
  delete d1_;
  delete d2_;
  delete invD2_;
  delete xUnScaled_;
  delete matrixScaled_;
  delete xUnPreEliminate_;
  delete rhsScaled_;
  delete solScaled_;
  if (elim_param_ != 0) {
    delete selected_;
    delete permutationVector_;
    delete permutationMatrix_;
    delete B12_;
    delete B21_;
    delete solPreEliminated_;
    delete rhsPreEliminated_;
    delete matrixPreEliminated_;
  }
  if (elim_param_ < 0) {
    delete invB11_;
  }
}

void DataProcessor::applyScaling() {
  for (int i = 0; i < matrix_->rows(); i++) {
    (*d1_)(i) = 1;
  }
  for (int j = 0; j < matrix_->cols(); j++) {
    (*d2_)(j) = 1;
  }

  switch (scaling_param_) {
    case 0: {
      break;
    }
    case 1: {
      for (int i = 0; i < matrix_->rows(); i++) {
        (*d1_)(i) = 0;
      }

      for (int i = 0; i < matrix_->outerSize(); i++) {
        for (int j = matrix_->outerIndexPtr()[i];
             j < matrix_->outerIndexPtr()[i + 1]; j++) {
          int rowIndex = matrix_->innerIndexPtr()[j];
          (*d1_)(rowIndex) += matrix_->valuePtr()[j] * matrix_->valuePtr()[j];
        }
      }

      for (int i = 0; i < matrix_->rows(); i++) {
        (*d1_)(i) = 1. / std::sqrt((*d1_)(i));
      }

      break;
    }
    case 2: {
      for (int j = 0; j < matrix_->cols(); j++) {
        (*d2_)(j) = 1. / matrix_->col(j).norm();
      }
      break;
    }
    case 3: {
      for (int i = 0; i < matrix_->rows(); i++) {
        (*d1_)(i) = 1. / matrix_->diagonal()(i);
      }
      break;
    }
    case 4: {
      for (int j = 0; j < matrix_->cols(); j++) {
        (*d2_)(j) = 1. / matrix_->diagonal()(j);
      }
      break;
    }
  }

  for (int j = 0; j < matrix_->cols(); j++) {
    (*invD2_)(j) = 1. / (*d2_)(j);
  }

  *matrixScaled_ = d1_->asDiagonal() * (*matrix_) * d2_->asDiagonal();
}

VectorXd* DataProcessor::undoScaling(VectorXd* z) {
  *xUnScaled_ = d2_->asDiagonal() * (*z);
  return xUnScaled_;
}

void DataProcessor::applyPreElimination() {
  // In current implementation we pre-eliminate 1/k of degree of freedom where k
  // is read from input parameter file
  if (elim_param_ == 0) {
    matrixPreEliminated_ = matrixScaled_;
    size_ = matrixPreEliminated_->rows();
  } else {
    // Select rows/cols to be pre-eliminated
    select();

    // Form permutation matrix
    int numSelceted = matrixScaled_->rows() - size_;
    permutationVector_ = new VectorXi(matrixScaled_->rows());
    int i1 = 0;
    int i2 = numSelceted;
    for (int i = 0; i < matrixScaled_->rows(); i++) {
      if ((*selected_)[i]) {
        (*permutationVector_)[i] = i1;
        i1++;
      } else {
        (*permutationVector_)[i] = i2;
        i2++;
      }
    }

    permutationMatrix_ = new permMat(*permutationVector_);

    // Permute matrix
    spMat tempMatrix = matrixScaled_->transpose();
    spMat permutedMatrix = (*permutationMatrix_) * tempMatrix;
    tempMatrix = permutedMatrix.transpose();
    permutedMatrix = (*permutationMatrix_) * tempMatrix;

    spMat B11 = permutedMatrix.block(0, 0, numSelceted, numSelceted);
    B12_ = new spMat(permutedMatrix.block(0, numSelceted, numSelceted, size_));
    B21_ = new spMat(permutedMatrix.block(numSelceted, 0, size_, numSelceted));
    spMat B22 = permutedMatrix.block(numSelceted, numSelceted, size_, size_);

    // Compute pre-eliminated matrix
    if (elim_param_ > 0) {
      solver_.analyzePattern(B11);
      solver_.factorize(B11);
      spMat invB11_times_B12 = solver_.solve(*B12_);
      matrixPreEliminated_ = new spMat(B22 - (*B21_) * invB11_times_B12);
    } else {
      invB11_ = new VectorXd(numSelceted);
      for (int i = 0; i < numSelceted; i++) {
        (*invB11_)(i) = 1.0 / B11.diagonal()(i);
      }
      matrixPreEliminated_ =
          new spMat(B22 - (*B21_) * (invB11_->asDiagonal() * (*B12_)));
    }

    // Dedicate memory for rhs and solution vectors
    rhsPreEliminated_ = new VectorXd(size_);
    solPreEliminated_ = new VectorXd(size_);
  }
}

VectorXd* DataProcessor::undoPreElimination(VectorXd* y2) {
  if (elim_param_ == 0) {
    *xUnPreEliminate_ = *y2;
  } else {
    VectorXd c = (*permutationMatrix_) * (*rhsScaled_);
    int numSelceted = matrixScaled_->rows() - size_;

    VectorXd* y1 = new VectorXd(numSelceted);
    if (elim_param_ > 0) {
      *y1 = solver_.solve(c.segment(0, numSelceted) - (*B12_) * (*y2));
    } else {
      *y1 =
          invB11_->asDiagonal() * (c.segment(0, numSelceted) - (*B12_) * (*y2));
    }

    xUnPreEliminate_->segment(0, numSelceted) = *y1;
    delete y1;
    xUnPreEliminate_->segment(numSelceted, size_) = *y2;
    *xUnPreEliminate_ = permutationMatrix_->transpose() * (*xUnPreEliminate_);
  }
  return xUnPreEliminate_;
}

VectorXd* DataProcessor::rhs(int j) {
  *rhsScaled_ = d1_->asDiagonal() * (*dl_->rhs(j));

  if (elim_param_ == 0) {
    rhsPreEliminated_ = rhsScaled_;
  } else {
    VectorXd c = (*permutationMatrix_) * (*rhsScaled_);
    int numSelceted = matrixScaled_->rows() - size_;
    if (elim_param_ > 0) {
      *rhsPreEliminated_ = c.segment(numSelceted, size_) -
                           (*B21_) * solver_.solve(c.segment(0, numSelceted));
    } else {
      VectorXd B21c = ((invB11_->asDiagonal()) * (c.segment(0, numSelceted)));
      *rhsPreEliminated_ = (*B21_) * B21c;
      *rhsPreEliminated_ = c.segment(numSelceted, size_) - *rhsPreEliminated_;
    }
  }
  return rhsPreEliminated_;
}

VectorXd* DataProcessor::sol(int j) {
  *solScaled_ = invD2_->asDiagonal() * (*dl_->sol(j));
  if (elim_param_ == 0) {
    solPreEliminated_ = solScaled_;
  } else {
    VectorXd y = (*permutationMatrix_) * (*solScaled_);
    int numSelceted = matrixScaled_->rows() - size_;
    *solPreEliminated_ = y.segment(numSelceted, size_);
  }
  return solPreEliminated_;
}

VectorXd* DataProcessor::retrieve(VectorXd* x_t) {
  return undoScaling(undoPreElimination(x_t));
}

void DataProcessor::analyze(VectorXd& x, int j) {
  VectorXd* x_computed = retrieve(&x);
  VectorXd delta = (*matrix_) * (*x_computed) - (*(dl_->rhs(j)));
  sum_residuals_ += delta.norm() / dl_->rhs(j)->norm();
  if (solutionAvailable()) {
    delta = (*x_computed) - (*(dl_->sol(j)));
    sum_errors_ += delta.norm() / dl_->sol(j)->norm();
  }
  num_analysis_++;
}

void DataProcessor::log(std::ofstream& out) {
  out << "Matrix size after pre-processing = " << size_ << std::endl;
  out << "Matrix norm after pre-processing = " << matrix()->norm() << std::endl;
  out << "Pre-processing time = " << time_ << " seconds" << std::endl;
  out << "Average solution relative residual = "
      << sum_residuals_ / num_analysis_ << std::endl;
  if (solutionAvailable()) {
    out << "Average solution relative error = " << sum_errors_ / num_analysis_
        << std::endl;
  }
}

void DataProcessor::select() {
  selected_ = new VectorXb(matrixScaled_->rows());
  if (elim_param_ > 0) {
    /* initialize random seed: */
    srand(time(NULL));
    size_ = 0;
    for (int i = 0; i < matrixScaled_->rows(); i++) {
      // False means not selected, true means selcetd
      (*selected_)[i] = (rand() % (elim_param_ + 1) == 0);
      if (!(*selected_)[i]) size_++;
    }
  } else {
    oneSelection();
  }
}

void DataProcessor::oneSelection() {
  std::set<int> avail;
  // Initially all DOF's are potentially available for selection
  for (int i = 0; i < matrixScaled_->rows(); i++) {
    avail.insert(i);
    (*selected_)[i] = false;
  }
  int num_selected = 0;
  while (!avail.empty()) {
    int i = *avail.begin();  // select a DOF
    (*selected_)[i] = true;
    avail.erase(i);
    num_selected++;
    // neighbors of i from the available set
    for (int j = matrixScaled_->outerIndexPtr()[i];
         j < matrixScaled_->outerIndexPtr()[i + 1]; j++) {
      int k = matrixScaled_->innerIndexPtr()[j];
      avail.erase(k);
    }
  }
  size_ = matrixScaled_->rows() - num_selected;
}