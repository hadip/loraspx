#ifndef ILU_h
#define ILU_h

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "preconditioner.h"
#include "time.h"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/***************************************************************/
/*                       ILU Preconditioner                    */
/***************************************************************/
//! Class ILU
/*!
This is just a wrapper around Eigen ILU inherited from preconditioner class.
 */
class ILU : public Preconditioner {
  //! Eigen ILU
  Eigen::IncompleteLUT<double>* eigenILU;

  //! Pointer to the solution vector
  VectorXd* x_;

  //! Time for preconditioner factorization time
  double setupAndFactorizationTime_;

 public:
  //! Default constructor
  ILU(){};

  //! constructor
  /*!
    Takes pointer to the sparse matrix, and ILU params
   */
  ILU(spMat* A, double dropTol, unsigned int fill) {
    clock_t start = clock();
    eigenILU = new Eigen::IncompleteLUT<double>(*A, dropTol, fill);
    setupAndFactorizationTime_ = double(clock() - start) / CLOCKS_PER_SEC;
    x_ = new VectorXd(A->rows());
  }

  //! Destructor
  ~ILU() {
    delete x_;
    delete eigenILU;
  }

  //! Solve function
  VectorXd& solve(VectorXd& b) {
    *x_ = eigenILU->solve(b);
    return *x_;
  }

  //! Log info
  void log(std::ofstream& out) {
    out << "ILU setup and factorization time = " << setupAndFactorizationTime_
        << " seconds\n";
  }
};

#endif
