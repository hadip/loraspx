#ifndef superNode_h
#define superNode_h

#include <array>
#include <vector>
#include "Eigen/Dense"
#include "blackNode.h"
#include "node.h"
#include "rsvd.h"

class HTree;
class RedNode;
class BlackNode;
class SuperNode;
class Hparams;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! Define a type pointer to dynamic size dense matrix stored by edge */
typedef densMat* densMatStr;

/*! A dense vector class used for permutation */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/*! Tuple of times for (time1, time2) */
typedef std::array<double, 2> timeTuple2;

/**************************************************************************/
/*                           CLASS Super NODE                             */
/**************************************************************************/

//! Class superNode
/*!
  Inherited from the general class node. It is a node correponding to particles
*/

class SuperNode : public Node {
  //! zero
  double MACHINE_PRECISION;

  //! Pointer to the parent (which is a blackNode)
  BlackNode* parent_;

  //! The level of node ( only redNodes have level)
  unsigned int level_;

  //! cut-off criterion for compression
  /*!
    It is used when deciding what singular values are important.
    It gets the vector of singular values, and returns k, the number of
    important singular values.
    Second input argument is the method.
    Third argument is the epsilon for cutoff
    Fourth argument is a given constant rank (for method 8)
    Last argument is the growth factor for rank cap (geometric series). Zero
    means no cap.
  */
  int cutOff(Eigen::JacobiSVD<densMat>&, int, double, int, double);

  //! Pointer to the frob norm list of the tree
  double* frobNorms_;

  //! Pointer to the frob norm of the full matrix
  double* globFrobNorm_;

  //! Pointer to the size of the full matrix
  double* globSize_;

  //! Pointer to the size list of the tree
  double* totalSizes_;

  //! maximum singularvalue of the matrix
  double maxSig_;

  //! Acess to HTree paramaeters
  Hparams* hparams_;

  //! For second-order low-rank factorization
  densMat* Vi_ = NULL;
  densMat* invVi_ = NULL;
  densMat* Vo_ = NULL;
  densMat* invVo_ = NULL;

  //! Store the inperpolation operator from black parent
  densMat* interpolationU = NULL;

 public:
  //! Default constructor
  SuperNode() {}

  //! Constructor:
  /*!
    input arguments:
    pointer to the blackNode parent, pointer to the tree, range of belonging
    rows/cols
  */
  SuperNode(BlackNode*, HTree*, int, int);

  //! Destructor
  ~SuperNode();

  //! Returns parent_
  BlackNode* parent() const { return parent_; }

  //! Returns pointer to its parent
  RedNode* redParent() { return parent_->parent(); }

  //! Returns level_
  unsigned int level() const { return level_; }

  //! Compress all well separated interactions
  /*!
    Exit is a timeTuple: (lowRank time, everything else time)
   */
  timeTuple2 compress();

  //! Eliminate the node, and its parent
  /*!
    Eliminating a node invloves going through all edges, and create new edges
    based on schur complement. Returns tuple of times: time spent to compute
    inverse of pivot, all other calculations
   */
  timeTuple2 schurComp();

  //! Eliminate super-node and black parent to-gether.
  /*!
  Returns tuple of times: time spent to compute
    inverse of pivot, all other calculations
  */
  timeTuple2 combinedSchurComp();

  //! Split the solution to left/right redNodes
  void splitVAR();

  //! check if the approximation is fine
  bool criterionCheck(int, double, RedSVD::RedSVD<densMat>&, densMat&, int);
};

#endif
