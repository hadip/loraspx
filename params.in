#DO NOT CHANGE THE ORDER OF PARAMETERS BUT YOU CAN ADD COMMENT USING HASH TAG => EACH LINE BEGINS WITH: HASH OR DATA OR NULL

#####################################################################
########################### INPUT FILES  ############################
#####################################################################

# [ Sparse matrix file address ]
# Hint: It can be either a relative path or an absolute path
A.mtx

# [ Right hand side(s) file address ]
# Hint 1: enter "NA" to generate RHS = A * SOLUTION.
# Hint 2: enter "USE_RANDOM" to generate a random RHS vector. In this case solution vector will not be loaded.
NA

# [ Solution(s) file address ]
# Hint 1: Enter "USE_RANDOM" to generate a random solution vector (in this case RHS is formed automatically as well)
# Hint 2: ENTER "NA" if solution is not available
# Hint 3: Right hand side and solution matrices should be of the same size and consistent with the sparse matrix size. We assume rhs is given in a dense format: one number per row. In the beginning of the file the size of the matrix should be given
USE_RANDOM

#####################################################################
########################### SOLVE METHOD  ###########################
#####################################################################

# [ Solve method ]
# Hint: Choose from the options below:
# "0" : Direct solve
# "1" : GMRES Iterations
# "2" : Fixed-Point Iteration
1

#####################################################################
###################### PRE-PROCESS PARAMETERS  ######################
#####################################################################
# [ scale the matrix ]
# Hint: with this option you can re-scale rows/columns of the matrix. Choose an option from the list below:
# "0": No re-scaling
# "1": Make all rows norm one
# "2": Make all columns norm one
# "3": Make all diagonal entries 1 (row manipulation)
# "4": Make all diagonal entries 1 (column manipulation)
# NOTE: Activating scaling most likely results in a non-symmetric matrix
0

# [ pre-elimination ]
# Hint choose from the options below for pre-elimination
# "0": No pre-elimination
# "d>0": Select each DOF with probability 1.0/(d+1) and pre-eliminate all selected DOFs.
# "-1": Select DOFs such that in the induced graph all connected components have only 1 node
# NOTE: "-1" option works only for matrices whose symbolic matrix is symmetric 
0

#####################################################################
######################## ITERATION PARAMETERS #######################
#####################################################################

# [ Pre-conditioner ]
# Hint 1: This is only used if an iterative solver is deployed, i.e., [ Solve method ] > 0.
# Hint 2: Determine a preconditioner to be used in conjunction with the iterative method from the list below:
# "0" : No pre-conditioner
# "1" : Diagonal pre-conditioner
# "2" : ILU
# "3" : H-solver
3

# [ max num iterations ]
# Hint: The maximum number of iterations, if an iterative method is used.
500

# [ pre-conditioned residual criterion ]
# Hint: Iteration continues until the pre-conditioned residual is smaller than this number
1e-10

# [ Verbose iterations ]
# Hint: Enter "1" to print per iteration info to the consule, otherwise, enter "0".
1

#####################################################################
######################## H-SOLVER PARAMETERS ########################
#####################################################################

# [ H-Tree depth ]
# Hint: Enter "0" to use the default depth = max( (log2 n) - 4, 1 )
0

# [ Well-separation distance ]
# Hint 1: This is the threshold distance to recognize an interaction as well-separated
# Hint 2: The default value is 1
1

# [ H-Tree direct-solve level ]
# Hint 1: This is the level at which we solve the system directly. 1 <= [ H-Tree direct-solve level ] <= [ H-Tree depth ]
# Hint 2: Default value is 1
1

# [ Symmetric ]
# Hint: choose between symmetric/non-symmetric solver. Choose from options below:
# "0": Non-symmetric solver
# "1": Symmetric solver
1

# [ 2nd order low-rank ]
# "0": Regular compression
# "1": Use SVD of the pivot to scale the well-separated blocks
# "2": Use LL^T (Cholesky) of the pivot to scale the well-separated blocks (Only for SPD matrices)
# Note 1: This option is expected to improve accuracy (while increasing factorization cost)
# Note 2: If your matrix is symmetric but not positive definite, you CANNOT activate both [Symmetric] and [2nd order low-rank]. For non-positive definitie matrices pivot decomposition S = V_i * V_o.tranpose() may not be symmetric.
0

# [ Combined elimination ]
# Hint: enter "0" for regular algorithm, and "1" to combine the elimination of red and black nodes (about 2x faster elimination)
# Hint: Activating this option in general improves the solver efficiency.
1

# [ un-compressed solve ] (ONLY "0" IMPLEMENTED)
# Hint: enter "0" to use compressed edges during solve, "1" to use un-compressed edges during the solve time
0

# [ Order of elimination ] (ONLY "0" IMPLEMENTED)
# Choose the order of elimination of super-nodes at each level from the options below:
# "0": no-order (generic)
# "1": min degree first
# "2": min norm( inv( pivot ) ) first
0

# [ Low-Rank-Approximation method ] (ONLY "0" IMPLEMENTED)
# Hint: "0": SVD, "1" : randomized SVD, "2" : rank-revealing QR
0

# [ rSVD safety factor ]
# Hint 1: This is only used if [ Low-Rank-Approximation method ] = "1"
# Hint 2: We do a larger rSVD by this factor, but use just first singular vectors.
1.5

# [ Compression cut-off method ]
# Hint: Choose from the list below:
# "0" : sigma(k) < epsilon
# "1" : sigma(k) / sigma(0) < epsilon
# "2" : sigma(k) / ||level||_F < epsilon
# "3" : sigma(k) / ||full matrix so far||_F < epsilon
# "4" : ||A - USV||_F / ||level||_F < epsilon
# "5" : ||A - USV||_F^2 / ||level||_F^2 < epsilon^2 * size(A) / size(level)
# "6" : ||A - USV||_F / ||full matrix so far||_F < epsilon
# "7" : ||A - USV||_F^2 / ||full matrix so far||_F^2 < epsilon^2 * size(A) / size(full matrix so far)
# "8" : constant rank = a priori rank
# "9" : sigma(k) / sigma0(A) < epsilon   sigma0(A) is the largest singular vector of the full matrix
# "10": ||A - USV||_F < epsilon
1

# [ Compression epsilon ]
# Hint: 0 <= epsilon <= 1. Smaller epsilons results in more accurate factorization.
1e-1

# [ A priori rank ]: 
# Hint 1: This is an estimation of the rank of well-separated interactions.
# Hint 2: Enter "0" to use the default value = mean( size( leaf red nodes ) )
# Hint 3: If method 8 is used for cut-off, this is the maximum rank at all levels.
# Hint 4: If [ enforced rank cap ] is active, this is the rank at the leaf level. The maximum rank at other levels may be higher (determined by a geometric series).
0

# [ Enforced rank cap ]
# Hint 1: Enter "0" to "de-activate" the rank cap. Generally, [ enforced rank cap ] decreases run time and accuracy.
# Hint 2: Any number greater than 0 "activates" the [ enforced rank cap ]. The maximum rank at level is [i] then determined by a geometric series: [ A priori rank ] * [ Enforced rank cap ] ^ ( [ H-Tree depth ] - [i] )
# Hint 3: Any number smaller than 2^(1/3) guarantees linear complexity
0

# [ Apply smoothing ] (ONLY "0" IMPLEMENTED)
# Hint: With this option you can determine how many block ILU smoothing to be applied at every level. "0" means no smoothing.
0

# [ Enhanced basis ] (ONLY "0" IMPLEMENTED)
# Hint: With this option you can enhance the intra level basis to enhance the accuracy and conserve smooth vectors. 0 means no enhancement. The larger number more enhancement is applied.
0

#####################################################################
########################## ILU PARAMETERS ###########################
#####################################################################

# ILU Droptol
1e-14

# ILU fill
1
