#ifndef h_params_h
#define h_params_h

//! H-tree solver parameter class
/*!
This class encapsulates all parameters related to the htree solver.
*/

class Hparams {
 private:
  //! Depth of the tree
  unsigned int treeDepth_;

  //! The level at which we perform direct solve
  unsigned int solveLevel_;

  //! Use symmetric algorithm or non-symmetric
  unsigned int symmetric_;

  //! Use combined red-black eliminatio
  bool combinedElimination_;

  //! Use un-compressed well-sep edges for solve time
  bool unCompressedSolve_;

  //! use second order low-rank method
  int secondOrder_;

  //! Order of elimination
  unsigned int eliminationOrder_;

  //! Low-rank approximation method
  unsigned int lowRankMethod_;

  //! Randomized SVD safety factor
  double rSVDFactor_;

  //! Method for compression cut-off
  int cutOffMethod_;

  //! Threshold for compression
  double epsilon_;

  //! A priori rank of compression
  int aPrioriRank_;

  //! Rank cap geometric series factor ( 0 == No cap )
  double rankCapFactor_;

  //! Number of ILU smoothing
  int smoothing_;

  //! Number of extra vectors to enhance the basis
  int basisEnhancement_;

  //! Verbose factorization
  bool verbose_;

  //! Distance threshold for well-separation
  int wellSepDistance_;

 public:
  //! Default constructor (uses default values for the solver)
  Hparams();

  //! Constructor with explicit values
  Hparams(unsigned int, int, unsigned int, unsigned int, int, bool, bool,
          unsigned int, unsigned int, double, unsigned int, double,
          unsigned int, double, int, int);

  //! Destrcutor
  ~Hparams() {}

  //! Access function to tree depth
  unsigned int treeDepth() const { return treeDepth_; }

  //! Set tree depth (used for automatic depth determination)
  void setTreeDepth(unsigned int d) { treeDepth_ = d; }

  //! Access function level to apply direct solve
  unsigned int solveLevel() const { return solveLevel_; }

  //! Access function: if the calculation is symmetric
  unsigned int symmetric() const { return symmetric_; }

  //! Access function: if to use black-red combined elimination
  bool combinedElimination() const { return combinedElimination_; }

  //! Access function: use un-compressed well-sep edges during solve
  bool unCompressedSolve() const { return unCompressedSolve_; }

  //! Access function: what decomposition to be used in 2nd-order low-rank
  int secondOrder() const { return secondOrder_; }

  //! Access function to order of elimination
  unsigned int eliminationOrder() const { return eliminationOrder_; }

  //! Access function to low-rank method
  unsigned int lowRankMethod() const { return lowRankMethod_; }

  //! Access function to randomized SVD safety factor
  double rSVDFactor() const { return rSVDFactor_; }

  //! Acess function to singular-values cut-off method
  unsigned int cutOffMethod() const { return cutOffMethod_; }

  //! Access function to compression epsilon
  double epsilon() const { return epsilon_; }

  //! Access function to a priori rank
  unsigned int aPrioriRank() const { return aPrioriRank_; }

  //! Set a priori rank (for automatically setup)
  void setAPrioriRank(unsigned int r) { aPrioriRank_ = r; }

  //! Access function to rank cap factor
  double rankCapFactor() const { return rankCapFactor_; }

  //! Access function to number of ILU smoothings to be applied at each level
  int smoothing() const { return smoothing_; }

  //! Access function to number vectors to be added to basis for enhancement
  int basisEnhancement() const { return basisEnhancement_; }

  //! Access function to verbosity
  bool verbose() const { return verbose_; }

  //! Access to distance threshold for well-separation
  int wellSepDistance() const { return wellSepDistance_; }
};

#endif