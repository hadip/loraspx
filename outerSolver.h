#ifndef outer_solver_h
#define outer_solver_h

#include <fstream>
#include <iostream>
#include "Eigen/Dense"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

//! Outer solver class
/*!
This is a pure abstract class that works as a wrapper around different outer
iterations implemented in this code (e.g., GMRES, Fixed point iteration, direct
solver).
*/
class OuterSolver {
 public:
  //! Solve linear system given the right-hand side and initial guess vectors
  virtual VectorXd& solve(VectorXd&, VectorXd&) = 0;

  virtual ~OuterSolver(){};

  //! Log outer solver information
  /*!
  Gets reference to an output file stream and put all information
  */
  virtual void log(std::ofstream&){};

  //! Returns pointer to the solution vector
  virtual VectorXd* retVal() = 0;
};

#endif
