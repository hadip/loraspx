#ifndef fixedPoint_h
#define fixedPoint_h

#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "outerSolver.h"
#include "preconditioner.h"
#include "time.h"

//! Sparse matrix type
/*! Define a sparse matrix (column major) */
typedef Eigen::SparseMatrix<double> spMat;

/*! Define a dynamic size dense matrix stored by edge */
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> densMat;

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

//! Class FixPoint
/*!
This class implements the fixed point iteration method.
 */
class FixedPoint : public OuterSolver {
  //! Maximum number of iterations
  int m_;

  //! Pointer to the preconditioner
  /*!
    Preconditioner supposed to have a function:
    x* solve( *rhs ): This function solve A x = rhs (approximately)
    and returns pointer to solution.
   */
  Preconditioner* precond_;

  //! size of the matrix
  int n_;

  //! total iterations applied
  int totalIters_;

  //! total time
  double totalTime_;

  //! preconditioner application time
  double pcTime_;

  //! required variables for the method:
  int k_;

  //! print per-iteration info
  bool verbose_;

  double epsilon_;
  double norm_b_;

  VectorXd* x_;
  VectorXd* dx_;
  VectorXd* r_;
  VectorXd* beta_;

  spMat* A_;

 public:
  //! Default constructor
  FixedPoint(){};

  //! Destructor
  ~FixedPoint();

  //! Constructor with input parameters
  /*!
    Pointer to the sparse matrix.
    Pointer to the preconditioner.
    Maximum number of iterations.
    Accuracy threshold.
    Verbose flag.
   */
  FixedPoint(spMat*, Preconditioner*, int, double, bool);

  //! solve: start iteration until:
  /*!
    1) Reach maximum number of iteration
    or
    2) Reach desired accuracy
    Input parameters:
    1) Reference to right hand side vector
    2) Reference to the initial guess vector
   */
  VectorXd& solve(VectorXd&, VectorXd&);

  //! Returns vector of residuals
  VectorXd* residuals();

  //! Returns solution
  VectorXd* retVal();

  //! Returns total number of itertions
  int totalIters() const { return totalIters_; }

  //! Returns total time to solve
  double totalTime() const { return totalTime_; }

  //! Log Fixed Point results in the given file
  void log(std::ofstream&);
};

FixedPoint::FixedPoint(spMat* A, Preconditioner* P, int m, double eps,
                       bool verb) {
  A_ = A;
  precond_ = P;
  n_ = A->cols();  // = A->rows()
  m_ = m;
  epsilon_ = eps;
  verbose_ = verb;

  //! Allocate memory to matrices and vectors
  x_ = new VectorXd(n_);
  r_ = new VectorXd(n_);
  dx_ = new VectorXd(n_);
  beta_ = new VectorXd(m_ + 1);
}

FixedPoint::~FixedPoint() {
  delete x_;
  delete r_;
  delete dx_;
  delete beta_;
}

VectorXd& FixedPoint::solve(VectorXd& b_, VectorXd& x0_) {
  clock_t start, start_pc;
  pcTime_ = 0;
  start = clock();

  (*x_) = (x0_);
  k_ = -1;
  double res = 1e10;
  (*r_) = b_ - (*A_) * (*x_);

  norm_b_ = b_.norm();

  while ((res > epsilon_) && (k_ < m_)) {
    k_++;

    start_pc = clock();
    (*dx_) = precond_->solve(*r_);
    pcTime_ += double(clock() - start_pc) / CLOCKS_PER_SEC;

    (*x_) = (*x_) + (*dx_);
    (*r_) = b_ - (*A_) * (*x_);
    res = r_->norm() / norm_b_;
    (*beta_)(k_) = res;
    if (verbose_) {
      std::cout << "   Residual at iteration " << k_ << " = " << res
                << std::endl;
    }
  }

  totalTime_ = double(clock() - start) / CLOCKS_PER_SEC;
  totalIters_ = k_ + 1;
  return *x_;
}

VectorXd* FixedPoint::retVal() { return x_; }

VectorXd* FixedPoint::residuals() { return beta_; }

void FixedPoint::log(std::ofstream& out) {
  out << "FixedPoint number of iterations = " << totalIters_ << std::endl;
  out << "FixedPoint total time = " << totalTime_ << " seconds\n";
  out << "Average preconditioner solve time = " << pcTime_ / totalIters_
      << " seconds\n";
}

#endif
