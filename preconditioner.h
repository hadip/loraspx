#ifndef precond_h
#define precond_h

#include <fstream>
#include <iostream>
#include "Eigen/Dense"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

//! Preconditioner class
/*!
This is a purely abstract class that works as a wrapper around different (inner)
solvers implemented in this code (e.g., HTree, ILU, etc.).
To add another solver to this code you inherit from this class (see for example,
ILU.h).
*/
class Preconditioner {
 public:
  //! Solve linear system for the given right-hand side vector
  virtual VectorXd& solve(VectorXd&) = 0;

  //! Destructor
  virtual ~Preconditioner(){};

  //! Log preconditioner information
  /*!
  Gets reference to an output file stream and put all information
  */
  virtual void log(std::ofstream&){};
};

#endif
