#include "blackNode.h"
#include <iostream>
#include "Eigen/Sparse"
#include "edge.h"
#include "hparams.h"
#include "htree.h"
#include "redNode.h"
#include "scotch.h"
#include "superNode.h"
#include "time.h"

// constructor for blackNode
BlackNode::BlackNode(RedNode* P, HTree* T, int first, int last)
    : Node((Node*)P, T, first, last) {
  // Set its parent
  parent_ = P;

  // Not elminated initially
  eliminated_ = false;

  //! Access to Htree parameters
  hparams_ = T->parameters();

  // We set m, and n based on number of columns/rows corresponding to this
  // redNode.
  // However, for non-leaf redNodes, m and n will be changed after low-rank
  // approximation
  //  n( last - first + 1 );
  n(0);

  // Block width
  int BlockWidth = last - first + 1;

  // If this is an epmty node, we just create two empty children, and a super
  // child
  if (isEmpty()) {
    leftChild_ = new RedNode(this, tree(), 0, first, last);
    rightChild_ = new RedNode(this, tree(), 1, first, last);
    superChild_ = new SuperNode(this, tree(), first, last);
    return;
  }

  // Get the Inner/Outer list for the corresponding block in symbolic matrix
  // Note that the symbolic matrix has zero diagonal (i.e., no self edge!)
  spMatBool SubMatrix =
      T->symbMatrix()->block(first, first, BlockWidth, BlockWidth);

  /**************************************************************************/
  /*                            SCOTCH PARTITIONING                         */
  /**************************************************************************/

  /********** Initializing SCOTCH VARIABLES **********/

  // The first index in arrays is 0
  SCOTCH_Num baseval = 0;

  // Number of vertices = number of rows
  SCOTCH_Num vertnbr = SubMatrix.rows();

  // Note that non-self edges count twice
  SCOTCH_Num edgenbr = SubMatrix.nonZeros();

  // No
  bool memAlloc = false;

  //! Array of start indices in edgetab
  SCOTCH_Num* verttab;

  //! Adjacency array of every vertex
  SCOTCH_Num* edgetab;

  // See SCOTCH user_guid to read more about SCOTCH_Num
  if (sizeof(SCOTCH_Num) == sizeof(int)) {
    verttab = SubMatrix.outerIndexPtr();
    edgetab = SubMatrix.innerIndexPtr();
  } else {
    std::cout << "This happened!" << std::endl;
    verttab = new SCOTCH_Num[vertnbr + 1];
    edgetab = new SCOTCH_Num[edgenbr];
    for (int i = 0; i <= vertnbr; i++) {
      verttab[i] = SubMatrix.outerIndexPtr()[i];
    }
    for (int i = 0; i < edgenbr; i++) {
      edgetab[i] = SubMatrix.innerIndexPtr()[i];
    }
    memAlloc = true;
  }

  /********** Initializing SCOTCH PARTITIONER **********/

  // Pointer to SCOTCH GRAPJ
  SCOTCH_Graph* graphPtr;

  // Define SCOTCH graph with error handling
  graphPtr = SCOTCH_graphAlloc();
  if (graphPtr == NULL) {
    std::cout << "Error! Could not allocate graph." << std::endl;
    exit(EXIT_FAILURE);
  }

  // Initiate SCOTCH graph with error handling
  if (SCOTCH_graphInit(graphPtr) != 0) {
    std::cout << "Error! Could not initialize graph." << std::endl;
    exit(EXIT_FAILURE);
  }

  // Define SCOTCH graph with error handling
  if (SCOTCH_graphBuild(graphPtr, baseval, vertnbr, verttab, verttab + 1, NULL,
                        NULL, edgenbr, edgetab, NULL) != 0) {
    std::cout << "Error! Failed to build graph." << std::endl;
    exit(EXIT_FAILURE);
  }

  if (SCOTCH_graphCheck(graphPtr) != 0) {
    std::cout << "Error! Graph inconsistent." << std::endl;
    exit(EXIT_FAILURE);
  }

  // Initialize partitioning strategy
  SCOTCH_Strat* partStratPtr = SCOTCH_stratAlloc();
  if (SCOTCH_stratInit(partStratPtr) != 0) {
    std::cout << "Error! Could not initialize partitioning strategy."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // Partition graph
  SCOTCH_Num* parttab = new SCOTCH_Num[BlockWidth];
  if (SCOTCH_graphPart(graphPtr, 2, partStratPtr, parttab) != 0) {
    std::cout << "Error! Partitioning Failed." << std::endl;
    exit(EXIT_FAILURE);
  }

  /*********** Modify the permutation vector of this level ************/
  // Find size of the first partion
  int size1 = 0;
  for (int i = 0; i < BlockWidth; i++) {
    if (parttab[i] == 0) size1++;
  }

  // now, modify the global permutation vecotr
  int i1 = 0;  // counter over the first partion
  int i2 = 0;  // counter over the second partion
  for (int i = 0; i < BlockWidth; i++) {
    if (parttab[i] == 0) {
      (*(T->permutationVector_))(first + i) = first + i1;
      i1++;
    } else {
      (*(T->permutationVector_))(first + i) = first + size1 + i2;
      i2++;
    }
  }

  /******** Now creating redNode children **********/
  // Note that at this point the global matrix has not permuted yet
  // However, the range of cols/row of children is known :)
  leftChild_ = new RedNode(this, tree(), 0, first, first + size1 - 1);
  rightChild_ = new RedNode(this, tree(), 1, first + size1, last);

  // Here we create the super child (i.e., the combination of the left and right
  // red children)
  // Later, we will actually create the corresponding edges of the super child.
  superChild_ = new SuperNode(this, tree(), first, last);

  // If used any extra memoty here we clean it heare
  SCOTCH_graphExit(graphPtr);
  delete[] parttab;
  if (memAlloc) {
    delete verttab;
    delete edgetab;
  }
}

// Destructor for blackNode: delete its Children
BlackNode::~BlackNode() {
  if (leftChild_ != NULL) delete leftChild_;
  if (rightChild_ != NULL) delete rightChild_;
  if (superChild_ != NULL) delete superChild_;

  // Delete interaction edges
  for (auto it = outEdges.begin(); it != outEdges.end(); ++it) {
    if (hparams_->symmetric()) {
      if (it->first != this) {
        it->first->outEdges.erase(this);
      }
    }
    delete it->second;
  }

  // Delete the inverse of the pivot matrix
  if (invPivot != NULL) delete invPivot;
}

// Merge left and right redNodes to a superNode
// Note that here we assume all redChildren have edges only within the same
// level
double BlackNode::mergeChildren() {
  clock_t start, finish;
  start = clock();
  // Set the number of variables/equations for the superChild
  superChild_->n(leftChild_->n() + rightChild_->n());
  // Check if any of the children has any variable
  if (superChild_->n() == 0) {
    finish = clock();
    return double(finish - start) / CLOCKS_PER_SEC;
  }

  // Define a Hash table:
  // key: parent(blackNode) value: pointer to the interaction matrices
  // (LL,RL,LR,RR)
  std::map<BlackNode*, densMatStr4> interMat;

  // Similar map to keep track of if we need to transpose matrices
  std::map<BlackNode*, Bool4> interBool;

  // Go over outgoing edges of left and right children, and add their parents to
  // the interactSet

  // Pointer to a redNode that has interaction with one of the children
  RedNode* interactant;

  // Parent of a redNode that has interaction with one of the children
  BlackNode* blackParent;

  // A temporary variable to create the 2x2 block of matrices(pointers)
  densMatStr4 blocks;

  // Temp variable (2x2 transpose flag)
  Bool4 blocksFlag;

  // Left child
  // First remove previosuly compressed edges from the list
  leftChild_->eraseCompressedEdges();

  for (auto it = leftChild_->outEdges.begin(); it != leftChild_->outEdges.end();
       ++it) {
    // Check if the edge is not eliminated
    if (it->second->isEliminated() == false) {
      // The redNode child I am interacting with
      interactant = (RedNode*)(it->first);

      // Parent of the interactant
      blackParent = interactant->parent();

      if (interMat.count(blackParent) ==
          0)  // i.e., has not been observed so far
      {
        // Initialyy all blocks are NULL
        blocks.fill(NULL);
        blocksFlag.fill(false);
      }

      else  // i.e., if we have already observed an interaction with a child of
            // this blackParent
      {
        // get the previous block
        blocks = interMat[blackParent];
        blocksFlag = interBool[blackParent];
      }

      // Depending on which(destination) fill either second or fourth block
      blocks[2 * interactant->which()] = &(it->second->matrix());
      blocksFlag[2 * interactant->which()] =
          it->second->isTransposed(leftChild_);

      // Update/Add the block in the Hash table
      interMat[blackParent] = blocks;
      interBool[blackParent] = blocksFlag;
    }
  }

  // Right child
  // First remove previosuly compressed edges from the list
  rightChild_->eraseCompressedEdges();

  for (auto it = rightChild_->outEdges.begin();
       it != rightChild_->outEdges.end(); ++it) {
    // Check if the edge is not eliminated
    if (it->second->isEliminated() == false) {
      // The redNode child I am interacting with
      interactant = (RedNode*)(it->first);

      // Parent of the interactant
      blackParent = interactant->parent();

      if (interMat.count(blackParent) ==
          0)  // i.e., has not been observed so far
      {
        // Initialyy all blocks are NULL
        blocks.fill(NULL);
        blocksFlag.fill(false);
      }

      else  // i.e., if we have already observed an interaction with a child of
            // this blackParent
      {
        // get the previous block
        blocks = interMat[blackParent];
        blocksFlag = interBool[blackParent];
      }

      // Depending on which(destination) fill either first or third block
      blocks[2 * interactant->which() + 1] = &(it->second->matrix());
      blocksFlag[2 * interactant->which() + 1] =
          it->second->isTransposed(rightChild_);

      // Update/Add the block in the Hash table
      interMat[blackParent] = blocks;
      interBool[blackParent] = blocksFlag;
    }
  }

  // Now the Hash table is complete, we need to create combined matrices
  // Go through all interactant, and create the corresponding edge

  for (auto it = interMat.begin(); it != interMat.end(); ++it) {
    // Define the block matrices:
    densMat topLeft(it->first->leftChild()->n(), leftChild_->n());
    densMat topRight(it->first->leftChild()->n(), rightChild_->n());
    densMat botLeft(it->first->rightChild()->n(), leftChild_->n());
    densMat botRight(it->first->rightChild()->n(), rightChild_->n());

    if ((it->second)[0] == NULL) {
      topLeft = densMat::Zero(topLeft.rows(), topLeft.cols());
    } else {
      if (interBool[it->first][0]) {
        topLeft = (it->second)[0]->transpose();
      } else {
        topLeft = *((it->second)[0]);
      }
    }

    if ((it->second)[1] == NULL) {
      topRight = densMat::Zero(topRight.rows(), topRight.cols());
    } else {
      if (interBool[it->first][1]) {
        topRight = (it->second)[1]->transpose();
      } else {
        topRight = *((it->second)[1]);
      }
    }

    if ((it->second)[2] == NULL) {
      botLeft = densMat::Zero(botLeft.rows(), botLeft.cols());
    } else {
      if (interBool[it->first][2]) {
        botLeft = (it->second)[2]->transpose();
      } else {
        botLeft = *((it->second)[2]);
      }
    }

    if ((it->second)[3] == NULL) {
      botRight = densMat::Zero(botRight.rows(), botRight.cols());
    } else {
      if (interBool[it->first][3]) {
        botRight = (it->second)[3]->transpose();
      } else {
        botRight = *((it->second)[3]);
      }
    }

    // Number of equations in destination
    int mDestination =
        it->first->leftChild()->n() + it->first->rightChild()->n();

    // Creating the combined interaction matrix
    densMat newMatrix(mDestination, superChild_->n());

    // Fill the combined matrix with blocks using Eigen comma initializer
    // *(superChild_ -> outEdges.back() -> matrix) << topLeft, topRight,
    // botLeft, botRight;
    if (topLeft.rows() * topLeft.cols() >
        0)  // i.e., if this is actually a finite block
    {
      newMatrix.block(0, 0, topLeft.rows(), topLeft.cols()) << topLeft;
    }
    if (topRight.rows() * topRight.cols() >
        0)  // i.e., if this is actually a finite block
    {
      newMatrix.block(0, topLeft.cols(), topRight.rows(), topRight.cols())
          << topRight;
    }
    if (botLeft.rows() * botLeft.cols() >
        0)  // i.e., if this is actually a finite block
    {
      newMatrix.block(topLeft.rows(), 0, botLeft.rows(), botLeft.cols())
          << botLeft;
    }
    if (botRight.rows() * botRight.cols() >
        0)  // i.e., if this is actually a finite block
    {
      newMatrix.block(topLeft.rows(), topLeft.cols(), botRight.rows(),
                      botRight.cols())
          << botRight;
    }
    // Creating the edge and add it to in/out edges list of two superChildren
    superChild_->createEdge(it->first->superChild(), newMatrix);
  }

  // Delete the matrix from non-eliminated outgoing edges from red-children:

  // Left child:
  for (auto it = leftChild_->outEdges.begin(); it != leftChild_->outEdges.end();
       ++it) {
    // Check if the edge is not eliminated
    if (it->second->isEliminated() == false) {
      it->second->deleteMatrix();
    }
  }

  // Rightt child:
  for (auto it = rightChild_->outEdges.begin();
       it != rightChild_->outEdges.end(); ++it) {
    // Check if the edge is not eliminated
    if (it->second->isEliminated() == false) {
      it->second->deleteMatrix();
    }
  }

  // Allocate memory for RHS and VAR of the superChild
  superChild_->RHS(new VectorXd(superChild_->n()));
  superChild_->VAR(new VectorXd(superChild_->n()));

  mergeRHS();

  finish = clock();
  return double(finish - start) / CLOCKS_PER_SEC;
}

void BlackNode::mergeRHS() {
  // Set the RHS
  if (leftChild()->n() >
      0)  // i.e., if the left child corresponds to any equation
  {
    superChild_->RHS()->segment(0, leftChild()->n()) = *(leftChild()->RHS());
  }
  if (rightChild()->n() >
      0)  // i.e., if the right child corresponds to any equation
  {
    superChild_->RHS()->segment(leftChild()->n(), rightChild()->n()) =
        *(rightChild()->RHS());
  }
}

// Apply the schur-complement
timeTuple2 BlackNode::schurComp() {
  timeTuple2 times;
  times[0] = 0;
  times[1] = 0;
  clock_t start, finish;
  start = clock();

  // Check if this node has any variable
  if (n() == 0) {
    eliminate();
    finish = clock();
    times[1] += double(finish - start) / CLOCKS_PER_SEC;
    return times;
  }

  // Create the edges to the redParent
  // Note that based on the algorithm the associated matrices are just -Identity
  densMat mId = -(densMat::Identity(n(), n()));

  createUpdateEdge(parent(), mId);

  if (!(hparams_->symmetric())) {
    parent()->createUpdateEdge(this, mId);
  }

  // First we detect the selfEdge (i.e., the edge from blacknode to itself)
  Edge* selfEdge = outEdges[this];

  invPivot = new densMat(n(), n());

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();

  // When second-order low-ran factorization is active,
  // black-node pivot is -Identity
  if (hparams_->secondOrder() > 0) {
    selfEdge->setMatrix(mId);
    *invPivot = mId;
  } else {
    *invPivot = selfEdge->matrix().inverse();
  }

  finish = clock();
  times[0] += double(finish - start) / CLOCKS_PER_SEC;
  start = clock();

  gaussElimination();

  // Now, mark the node as eliminated
  eliminate();

  finish = clock();
  times[1] += double(finish - start) / CLOCKS_PER_SEC;
  return times;
}
