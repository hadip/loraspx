#ifndef eyePC_h
#define eyePC_h

#include "Eigen/Dense"
#include "preconditioner.h"
#include "time.h"

/*! A dense vector class used for RHS */
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> VectorXd;

/***************************************************************/
/*                     Identity Preconditioner                 */
/***************************************************************/
//! Identity preconditioner.
/*!
  Essentialy, this identity precondiotner = no preconditioner!
 */
class EyePC : public Preconditioner {
 public:
  //! Default constructor
  EyePC(){};

  //! Destructor
  ~EyePC(){};

  //! Solve function
  VectorXd& solve(VectorXd& b) { return b; }

  //! Log info
  void log(std::ofstream& out) {
    out << "Identity preconditioner setup and factorization time = 0.0\n";
  }
};

#endif
