#include <stdlib.h>
#include "ILU.h"
#include "dataLog.h"
#include "dataProcessor.h"
#include "diagPC.h"
#include "directSolver.h"
#include "eyePC.h"
#include "fixedPoint.h"
#include "gmres.h"
#include "hparams.h"
#include "htree.h"
#include "outerSolver.h"
#include "params.h"
#include "preconditioner.h"
#include "time.h"

int main(int argc, char *argv[]) {
  //! Load data and parameters
  Params params(argc, argv);

  //! Apply pre-processing
  DataProcessor dataProcessor(params.data(), params.scaleMatrix(),
                              params.preElimination());

  Preconditioner *pc;
  switch (params.preconditioner()) {  // pick preconditioner
    case 1: {                         // i.e., Diag
      pc = new DiagPC(dataProcessor.matrix());
      break;
    }
    case 2: {  // i.e., ILU
      pc = new ILU(dataProcessor.matrix(), params.ILUDropTol(),
                   params.ILUFill());
      break;
    }
    case 3: {  // i.e., H2
      pc = new HTree(dataProcessor.matrix(), params.hparams());
      break;
    }
    default: {  // i.e., Identity
      pc = new EyePC();
    }
  }

  OuterSolver *outerSolver;
  switch (params.solveMethod()) {  // pick outer solve method
    case 0: {                      // direct solve
      outerSolver = new DirectSolver(dataProcessor.matrix(), pc);
      break;
    }
    case 1: {  // iteration with GMRES
      outerSolver =
          new Gmres(dataProcessor.matrix(), pc, params.maxIters(),
                    params.iterationEpsilon(), params.iterationVerbose());
      break;
    }
    default: {  // iteration with fixed point method
      outerSolver =
          new FixedPoint(dataProcessor.matrix(), pc, params.maxIters(),
                         params.iterationEpsilon(), params.iterationVerbose());
    }
  }

  for (int j = 0; j < dataProcessor.numRHS(); j++) {  // go over different RHSs
    // Initial guess vector
    VectorXd x0 = VectorXd::Zero(dataProcessor.size());

    // This right hand side
    VectorXd *b = dataProcessor.rhs(j);

    // Solve and analyze
    dataProcessor.analyze(outerSolver->solve(*b, x0), j);
  }

  //! Data Log
  DataLog dataLog;
  dataLog.logDataLoader(params.data());
  dataLog.logDataProcessor(&dataProcessor);
  dataLog.logPreconditioner(pc);
  dataLog.logOuterSolver(outerSolver);

  delete outerSolver;
  delete pc;
}
